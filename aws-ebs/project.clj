(defproject xeric/aws-ebs "0.2.2-SNAPSHOT"
  :description "Clojure library for Elastic Block Storage"
  :dependencies [[org.clojure/clojure "_"]
                 [com.amazonaws/aws-java-sdk "1.9.14"]]

  :plugins 
  [[lein-modules "0.3.9"]]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
