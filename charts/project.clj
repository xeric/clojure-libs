(defproject xeric/charts "0.2.2-SNAPSHOT"
  :description "C3 based charts for clojurescript"
  :dependencies 
  [[org.clojure/clojurescript "_"]
   [xeric/i18n :version]
   [xeric/dsler :version]
   [com.cemerick/clojurescript.test "_" :scope "test"]]
  :plugins [[lein-modules "0.3.9"]]
  :source-paths ["src"]
  :test-paths ["test"]
  :cljsbuild 
  {:builds [{:id "dev"
             :source-paths ["src" "test"]
             :compiler {
                        :output-to "target/charts.js"
                        :output-dir "out"
                        :optimizations :none
                        :source-map true}}
            {:id "test"
             :source-paths ["src" "test"]
             :notify-command 
             ["phantomjs" :cljs.test/runner
              "--web-security=false"
              "this.literal_js_was_evaluated=true"
              "--local-to-remote-url-access=true"
              "test-resources/polyfill.js"
              "test-resources/d3.v3.min.js"
              "target/cljs/testable.js"]
             :compiler {
                        :output-dir "target/cljs/out"
                        :output-to "target/cljs/testable.js"
                        :optimizations :simple
                        :pretty-print true}}]})
