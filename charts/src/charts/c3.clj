(ns charts.c3)

(defmacro add-configurer 
  [label fn]
  `(~'charts.c3/add-configurer* ~label ~fn))
