(ns charts.c3
  (:require [clojure.walk]))

(defn nil-or-empty?
  [x]
  (or (nil? x)
      (and (coll? x) (empty? x))))

(defn- remove-nils-or-empty
  "Removes keys will values nil or empty"
  [data]
  (clojure.walk/postwalk 
   (fn [x]
     (if (map? x) (into {} (remove (fn [[k v]] (nil-or-empty? v)) x)) x))
   data))

(declare transform-series transform-data-keys)

(defn- transform-data [{:keys [labels stacked-order series] :as chart}]
  (loop [series-list series data {:labels (or labels false)
                                  :order (or stacked-order "asc")}]
    (if-let [s (first series-list)]
      (recur (rest series-list) (transform-series data s))
        (-> chart (dissoc :series :labels) (assoc :data data)))))

(defn- transform-series
  [data {:keys [name text color series-type chart-type regions] :as series}]
  (-> data
      (assoc-in [:names name] text)
      (assoc-in [:colors name] color)
      (assoc-in [:regions name] regions)
      (assoc-in [:types name] series-type)
      (transform-data-keys series)))

(defn- transform-data-keys
  [data {:keys [name axis] :as series}]
  (if axis
    (assoc-in data [:keys :x] name)
    (update-in data [:keys :value] (fnil conj []) name))) 

(defn- transform-grid
  [{:keys [x-axis y-axis y2-axis] :as chart}]
  (-> chart
      (assoc-in [:grid :x] (:grid x-axis))
      (assoc-in [:x-axis :grid] nil)
      (assoc-in [:grid :y] (:grid y-axis))
      (assoc-in [:y-axis :grid] nil)
      (assoc-in [:grid :y2] (:grid y2-axis))
      (assoc-in [:y2-axis :grid] nil)))

(defn- to-regions [axis {:keys [regions]}]
  (map #(assoc % :axis axis) regions))

(defn- transform-region
  [{:keys [x-axis y-axis y2-axis] :as chart}]
  (let [regions (concat (to-regions :x x-axis)
                        (to-regions :y y-axis)
                        (to-regions :y2 y2-axis))]
    (-> chart
        (assoc :regions regions)
        (assoc-in [:x-axis :regions] nil)
        (assoc-in [:y-axis :regions] nil)
        (assoc-in [:y2-axis :regions] nil))))

(defn- transform-axis [chart]
  (let [{:keys [x-axis y-axis y2-axis] :as chart'} 
        (-> (transform-grid chart) transform-region)]
    (-> chart'
        (assoc-in [:axis :x] x-axis)
        (assoc-in [:axis :y] y-axis)
        (assoc-in [:axis :y2] y2-axis)
        (dissoc :x-axis :y-axis :y2-axis))))

(defn- transform-chart-options
  [{:keys [chart-options chart-type groups rotated] :as chart}]
  (-> (merge chart chart-options)
      (assoc-in [:data :type] chart-type)
      (assoc-in [:data :groups] groups)
      (assoc-in [:axis :rotated] rotated)
      (dissoc :chart-options :chart-type :groups :rotated)))

(defn transform-chart
  [chart values]
  (-> chart
      transform-data
      transform-axis
      transform-chart-options
      remove-nils-or-empty
      (assoc-in [:data :json] values)))

(defn format [s]
  (.. js/d3 (format s)))

(defn update-names! [chart & {:as args}]
  (.. chart -data (names (clj->js args))))

(defn update-colors! [chart & {:as args}]
  (.. chart -data (colors (clj->js args))))

(defn update-axis-labels! [chart & {:as args}]
  (.. chart -axis (labels (clj->js args))))

(defn resize! [chart & {:as args}]
  (.. chart (resize (clj->js args))))

(defn transform! 
  ([chart type] 
     (.. chart (transform (name type))))
  ([chart type series-name]
     (.. chart (transform (name type) (name series-name)))))

(defn update-data! [chart chart-data data]
  (let [keys (-> chart-data
                 transform-data
                 (get-in [:data :keys]))]
    (.. chart (load (clj->js {:json data :keys keys})))))
