(ns charts.core
  (:require-macros [dsler.core 
                    :refer [deflist defscalar defmap defsingularp defmultiplep
                            defattr required-fields]])
  (:require [dsler.core :as dsler :refer [apply-changers]]))

(defscalar chart-type)
(defscalar color)
(defscalar labels)
(defscalar stacked-order)
(defscalar format)
(defscalar text)
(defscalar data-name)
(defscalar axis)
(defscalar series-type)
(deflist groups)
(deflist regions)

(defmultiplep series [color data-name axis regions series-type])

(defmap x-axis)
(defmap y-axis)
(defmap y2-axis)
(defscalar rotated)
(defmap tooltip)
(defmap legend)
(defmap subchart)
(defmap zoom)
(defmap point)
(defmap line)
(defmap bar)
(defmap pie)
(defmap donut)
(defmap chart-options)

(defn chart
  [& args]
  (apply-changers {:type :chart} 
                  args 
                  [series x-axis y-axis y2-axis tooltip legend groups
                   subchart zoom point line bar pie donut chart-options
                   chart-type rotated labels stacked-order]))
