(ns charts.demo
  (:require [charts.core :as charts]
            [charts.c3 :as c3]))

(defn chart-maker
  [selector chart data]
  (.generate js/c3
             (clj->js (c3/transform-chart (assoc chart :bindto selector) data))))

(enable-console-print!)

(def line-chart
  (charts/chart (charts/series :data1) (charts/series :data2)))

(def sample-data [{:data1 300 :data2 50 :x "2014-01-01"}
                  {:data1 200 :data2 20 :x "2014-01-02"}
                  {:data1 100 :data2 10 :x "2014-01-03"}
                  {:data1 400 :data2 40 :x "2014-01-04"}
                  {:data1 150 :data2 15 :x "2014-01-05"}
                  {:data1 250 :data2 25 :x "2014-01-06"}])

(chart-maker "#line-chart" line-chart sample-data)

(def demo1
  (charts/chart
   (charts/series :data1)
   (charts/series :data2)
   (charts/rotated true)
   (charts/groups [:data1 :data2])
   (charts/x-axis :show false)
   (charts/y-axis :show false)
   (charts/chart-type :bar)))

(chart-maker "#line-chart" demo1 sample-data)

(def timeseries
  (charts/chart (charts/series :data1) (charts/series :data2)
                (charts/series :x (charts/axis :x))
                (charts/x-axis :type "timeseries"
                               :tick {:format "%Y-%m-%d"})))

(chart-maker "#timeseries" timeseries sample-data)

(def spline
  (charts/chart (charts/series :data1 "Data #1") (charts/series :data2 "Data #2")
                (charts/chart-type :spline)))
(chart-maker "#spline" spline sample-data)

(def regions
  (charts/chart (charts/series :data1 "Data #1") 
                (charts/series :data2 "Data #2"
                               (charts/regions {:start 1 :end 2}))
                (charts/y-axis :regions [{:end 20 :class "r2"}
                                        {:start 30 :class "r3"}])))

(chart-maker "#regions" regions sample-data)

(def steps
  (charts/chart (charts/series :data1 "Data #1" 
                               (charts/series-type :area-step)) 
                (charts/series :data2 "Data #2"
                               (charts/series-type :step))))

(chart-maker "#steps" steps sample-data)


(def areas
  (charts/chart (charts/series :data1 "Data #1" 
                               (charts/series-type :area)) 
                (charts/series :data2 "Data #2"
                               (charts/series-type :area-spline))))

(chart-maker "#areas" areas sample-data)

(def stacked
  (charts/chart
   (charts/stacked-order "desc")
   (charts/groups [:data1 :data2])
   (charts/series :data1 "Data #1" 
                  (charts/series-type :area)) 
   (charts/series :data2 "Data #2"
                  (charts/series-type :area-spline))))

(chart-maker "#stacked" stacked sample-data)


(def bar
  (charts/chart (charts/chart-type :bar)
                (charts/series :data1 "Data #1") 
                (charts/series :data2 "Data #2")
                (charts/series :x "Data" (charts/axis :x))
                (charts/y-axis :show true)
                (charts/x-axis :type :categories)))

(chart-maker "#bar" bar sample-data)

(def stacked-bar
  (charts/chart (charts/groups [:data1 :data2])
                (charts/chart-type :bar)
                (charts/stacked-order "desc")
                (charts/series :data1 "Data #1") 
                (charts/series :data2 "Data #2")
                (charts/series :x "Data" (charts/axis :x))
                (charts/x-axis :type :categories)))

(chart-maker "#stacked-bar" stacked-bar sample-data)

(def pie
  (charts/chart (charts/chart-type :pie)
                (charts/series :data1 "Data #1") 
                (charts/series :data2 "Data #2")
                (charts/series :x "Data" (charts/axis :x))
                (charts/x-axis :type :categories)))

(chart-maker "#pie" pie sample-data)

(def donut
  (charts/chart (charts/chart-type :donut)
                (charts/donut :title "Activity Donut")
                (charts/series :data1 "Data #1") 
                (charts/series :data2 "Data #2")
                (charts/series :x "Data" (charts/axis :x))
                (charts/x-axis :type :categories)))

(chart-maker "#donut" donut sample-data)

(def rotated-bar
  (charts/chart (charts/chart-type :bar)
                (charts/rotated true)
                (charts/series :data1 "Data #1") 
                (charts/series :data2 "Data #2")
                (charts/series :x "Data" (charts/axis :x))
                (charts/x-axis :type :categories)))

(chart-maker "#rotated-bar" rotated-bar sample-data)

(def tick-format
  (charts/chart (charts/series :data1 "Data #1") 
                (charts/series :data2 "Data #2")
                (charts/series :x "Data" (charts/axis :x))
                (charts/y-axis :tick {:format (c3/format "#,")}
                               :max 1000
                               :min -200
                               :label "Values Label")
                (charts/x-axis :type :timeseries 
                               :label {:text "Day Label"
                                       :position :outer-center}
                               :tick {:format (fn [x] (str "Day " (.getDay x)))
                                      :fit true
                                      :rotate 75
                                      :culling {:max 4}})))

(chart-maker "#tick-format" tick-format sample-data)

(def grid
  (charts/chart (charts/series :data1 "Data #1") 
                (charts/series :data2 "Data #2")
                (charts/series :x "Data" (charts/axis :x))
                (charts/y-axis :grid {:show true
                                      :lines [{:value 50 
                                               :text "half-century"}]})
                (charts/x-axis :type :categories :grid {:show true})))

(chart-maker "#grid" grid sample-data)

(def update
  (charts/chart (charts/series :data1 "Data #1")
                (charts/series :data2 "Data #2")))

(def update-chart
  (chart-maker "#update" update sample-data))

(def new-data
  (map (fn [row]
         (-> row 
             (update-in [:data1] (fn [data] (* data data)))
             (update-in [:data2] (fn [data] (+ data data)))))
       sample-data))

(js/setTimeout (fn [] 
                 (c3/update-axis-labels! update-chart 
                                         :x "X changed label"
                                         :y "Y Changed")
                 (c3/transform! update-chart :bar :data2)
                 (c3/update-data! update-chart update new-data)
                 (c3/resize! update-chart :width 500)
                 (c3/update-names! update-chart :data1 "New Name")
                 (c3/update-colors! update-chart :data1 "red"))
               2000)
