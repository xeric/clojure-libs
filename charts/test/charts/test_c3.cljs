(ns charts.test-c3
  (:require-macros [cemerick.cljs.test :refer (is are deftest done testing)])
  (:require [cemerick.cljs.test :as t]
            [charts.core :as charts]
            [charts.c3 :as c3]))

(deftest series-gets-mapped-to-data
  (is (= (c3/transform-chart
          (charts/chart
           (charts/labels {:format {:x "abc"}})
           (charts/series :provider "Provider"
                          (charts/color "blue"))
           (charts/series :active "Active" (charts/axis :x)))
          [{:provider :a :active 10}])
         {:type :chart
          :data {:labels {:format {:x "abc"}}
                 :order "asc"
                 :json [{:provider :a :active 10}]
                 :names {:provider "Provider" :active "Active"}
                 :colors {:provider "blue"}
                 :keys {:value [:provider] :x :active}}})))

(deftest chart-options-are-transformed
  (is (= (c3/transform-chart
          (charts/chart (charts/chart-options :padding {:top 10}
                                              :size {:width 500 :height 400}))
          [])
         {:type :chart :data {:order "asc" :labels false :json []} :padding {:top 10}
          :size {:width 500 :height 400}})))

(deftest groups-are-transformed
  (is (= (c3/transform-chart (charts/chart (charts/groups [:a :b])) [])
         {:type :chart :data {:order "asc" :labels false :json [] :groups '([:a :b])}})))

(deftest rotated-is-transformed
  (is (= (c3/transform-chart (charts/chart (charts/rotated true)) [])
         {:type :chart :data {:order "asc" :labels false :json []} :axis {:rotated true}})))

(deftest grid-is-transformed
  (is (= (c3/transform-chart 
          (charts/chart 
           (charts/x-axis :grid true)) [])
         {:type :chart :data {:order "asc" :labels false :json []} :grid {:x true}})))

(deftest regions-are-transformed
  (is (= (c3/transform-chart
          (charts/chart 
           (charts/x-axis
            :regions [{:start 1 :end 20 :style :dashed} {:start 22}])) [])
          {:type :chart :data {:order "asc" :labels false :json []}
                               :regions [{:start 1 :end 20 :style :dashed :axis :x}
                                         {:start 22 :axis :x}]})))

(deftest series-type-are-transformed
  (is (= (c3/transform-chart
          (charts/chart
           (charts/labels true)
           (charts/series :foo (charts/series-type :step))) [])
         {:type :chart :data {:order "asc" :labels true :types {:foo :step}, 
                              :keys {:value [:foo]}, :json []}})))
