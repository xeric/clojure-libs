(ns charts.test-core
  (:require-macros [cemerick.cljs.test :refer (is are deftest done testing)])
  (:require [cemerick.cljs.test :as t]
            [charts.core :as charts]))

(deftest test-charts
  (is (= (charts/chart 
          (charts/labels true)
          (charts/series :provider (charts/color "red") (charts/axis :x))
          (charts/series :activity)
          (charts/x-axis :show true :rotated false)
          (charts/y-axis :padding 20 :label "Activities"))
         {:type :chart
          :labels true
          :x-axis {:show true :rotated false}
          :y-axis {:padding 20 :label "Activities"}
          :series [{:index 0 :type :series :name :provider :color "red" :axis :x}
                   {:index 1 :type :series :name :activity}]})))

(deftest test-tooltip
  (is (= (charts/chart (charts/tooltip :show true))
         {:type :chart :tooltip {:show true}})))

(deftest test-legend
  (is (= (charts/chart (charts/legend :position :center :show true))
         {:type :chart :legend {:position :center :show true}})))

(deftest test-groups
  (is (= (charts/chart (charts/groups [:active :inactive]
                                      [:completed :quota-full]))
         {:type :chart :groups [[:active :inactive] [:completed :quota-full]]})))
