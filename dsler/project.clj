(defproject xeric/dsler "0.2.2-SNAPSHOT"
  :description "Tool to build your own dsl"
  :dependencies 
  [[org.clojure/clojurescript "_"]
   [inflections "0.9.9"]
   [xeric/i18n :version]
   [com.cemerick/clojurescript.test "_" :scope "test"]]

  :plugins [[lein-modules "0.3.9"]]
  :cljsbuild 
  {:builds [{:id "test"
             :source-paths ["src" "test"]
             :notify-command 
             ["phantomjs" :cljs.test/runner
              "--web-security=false"
              "this.literal_js_was_evaluated=true"
              "--local-to-remote-url-access=true"
              "test-resources/js/polyfill.js"
              "target/cljs/testable.js"]
             :compiler {
                        :output-dir "target/cljs/out"
                        :output-to "target/cljs/testable.js"
                        :optimizations :simple
                        :pretty-print true}}]})
