(ns dsler.core
  (:require [inflections.core :refer [plural]]))

(defn- remove-stars [n] (.replace (str n) "*" ""))

(defn- to-list-keyword [name]
  (keyword (plural (remove-stars name))))

(defmacro defscalarfn
  "Defines an assignment function."
  [name args & body]
  `(defn ~name ~args (~'dsler.core/update-fn 
                      ~(keyword (remove-stars name)) 
                      (fn [attrs#] ~@body) '~(remove-stars name))))


(defmacro defmultiplefn
  "Defines an assignment function which adds values as list"
  [name args & body]
  (let [list-keyword (to-list-keyword name)]
    `(defn ~name ~args (~'dsler.core/update-fn 
                        ~list-keyword
                        (fn [attrs#] 
                          ((fnil conj []) (~list-keyword attrs#) ~@body))
                        '~(remove-stars name)))))

(defmacro defscalar
  "Defines an assignment function which takes a single argument"
  [name]
  `(defscalarfn ~name [arg#] arg#))

(defmacro defmap
  "Defines an assignment function which takes a map as multiple args"
  [name] 
  `(defscalarfn ~name [& {:as args#}] args#))

(defmacro deflist
  "Defines an assignment function which takes a list as multiple args"
  [name]
  `(defscalarfn ~name [& args#] args#))

(defmacro defattr
  "Defines a true attribute function"
  [name]
  `(def ~name 
     (~'dsler.core/assoc-fn ~(keyword (str name "?")) true '~name)))

(defmacro defattri
  "Defines a corresponding false attribute function for a given true attribute 
   function"
  [name attr]
  `(def ~name
     (~'dsler.core/assoc-fn ~(keyword (str attr "?")) false '~name)))

(defmacro defmultiple
  [name]
  `(defmultiplefn ~name [arg#] arg#))

(defmacro defsingularp
  "Defines a singular processor function"
  [name tags & attrs]
  (let [n (keyword (remove-stars name))]
    `(defn ~name [& ~'args]
       (~'dsler.core/update-fn ~n
                               (fn [attrs#]
                                 (~'dsler.core/apply-changers 
                                  {:type ~n}
                                  (concat ~'args (vector ~@attrs))
                                  ~tags))
                               '~name))))

(defmacro defmultiplep*
  "Defines a processor function which can be used multiple times"
  [name list-keyword tags & attrs]
  `(defn ~name [& ~'args]
     (~'dsler.core/update-fn ~list-keyword
                             (fn [attrs#]
                               (let [l# (vec (~list-keyword attrs#))
                                     e# (~'dsler.core/apply-changers 
                                         {:type ~(keyword (remove-stars name))
                                          :index (count l#)}
                                         (concat ~'args (vector ~@attrs))
                                         ~tags)]
                                 (conj l# e#)))
                             '~name)))

(defmacro defmultiplep
  "Defines a processor function which can be used multiple times"
  [name tags & attrs]
  (let [list-keyword (to-list-keyword name)]
    `(defmultiplep* ~name ~list-keyword ~tags ~@attrs)))

(defmacro required-fields [& fields]
  (let [fields (vec fields)
        field-names (->> fields
                         (map clojure.core/name)
                         (interpose ", ")
                         (apply str))]
    `(fn [{:keys ~(vec (concat fields ['name 'type])) :as attrs#}]
       (if-not (not-any? nil? [~@fields])
         (throw (ex-info
                 (~'i18n.core/t 
                  :missing-fields 
                  (clojure.core/name ~'type) 
                  (if (nil? ~'name) "" ~'name) 
                  ~field-names) {}))
         attrs#))))

