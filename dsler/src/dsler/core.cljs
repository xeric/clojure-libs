(ns dsler.core
  (:require-macros [dsler.core])
  (:require [i18n.core :refer [t]]))

(defprotocol ITag
  (^:export tag [this]))

(defprotocol IMapChanger
  (^:export change [this m]))

(defprotocol IValidate)

(defn- comp-changers [changers m]
  (loop [changers changers result m]
      (if (empty? changers)
        result
        (recur (rest changers) 
               (change (first changers) result)))))

(defn fix-keyword-digit-prefix
  [k]
  (let [n (name k)]
    (if (re-seq #"^\d.*" n)
      (keyword (str "_" n))
      k)))

(extend-protocol ITag
  Symbol
  (tag [this] (name this))

  Keyword
  (tag [this]
    (name this))

  string
  (tag [this] this)

  number
  (tag [this] (str this))

  nil
  (tag [this] nil)

  function
  (tag [this] 
    (tag (this))))

(extend-protocol IMapChanger
  cljs.core.LazySeq
  (change [this m]
    (comp-changers this m))

  cljs.core.PersistentVector
  (change [this m]
    (comp-changers this m))

  cljs.core.PersistentHashMap
  (change [this m]
    (merge m this))

  cljs.core.PersistentArrayMap
  (change [this m]
    (merge m this))

  function
  (change [this m] 
    (this m))

  nil
  (change [this m]
    m)

  Keyword
  (change [this m]
    (let [n (:name m)]
      (if (nil? n)
        (assoc m :name (fix-keyword-digit-prefix this))
        m)))

  number
  (change [this m]
    (assoc m :value this))

  string
  (change [this m] 
          (assoc m :text this)))

(defrecord FunctionMapChanger [key func tag-name]
  IValidate
  ITag
  (tag [_] (if tag-name (name tag-name)))
  IMapChanger
  (change [_ m]
    (let [v (func m)
          old-v (get m key)]
      (if (nil? v)
        m
        (if (and old-v (not= v old-v) (not (vector? old-v)))
          (throw (js/Error (t :key-already-set key m)))
          (assoc m key v))))))

(defn ^:export update-fn
  ([key func] (update-fn key func nil))
  ([key func tag-name]
     (FunctionMapChanger. key func tag-name)))

(defn ^:export assoc-fn 
  [key value tag]
  (update-fn key (fn [] value) tag))

(defn- validate-child? [child allowed m]
  (if (and (satisfies? IValidate child)
          (not (nil? (tag child))))
    (let [child-tag (tag child)
          allowed-tags (map #(tag %) allowed)
          current-tag (name (:type m))]
      (if-not (first (filter #(= child-tag %) allowed-tags))
        (throw (ex-info (t :invalid-child-tag child-tag current-tag
                           (apply str (interpose ", " allowed-tags)))
                        {}))))))

(defn apply-changers
  [{:keys [type] :or {type :tag} :as m} 
   changers allowed-children]
  (loop [changers changers result m]
    (if (empty? changers)
      result
      (let [changer (first changers)]
        (validate-child? changer allowed-children m)
        (recur (rest changers) (change changer result))))))
