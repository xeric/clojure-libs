PATH=/usr/local/git/bin:/usr/bin:/bin:$PATH

export PATH

##
# Your previous /Users/tawus/.bash_profile file was backed up as /Users/tawus/.bash_profile.macports-saved_2011-09-02_at_06:55:47
##

# MacPorts Installer addition on 2011-09-02_at_06:55:47: adding an appropriate PATH variable for use with MacPorts.
export PATH=/usr/local/Cellar/ruby/1.9.3-p194/bin:/Users/tawus/bin:/Users/tawus/java/gradle-1.0-milestone-3/bin:$PATH:/usr/local/sbin

alias mysqlstart='sudo /opt/local/bin/mysqld_safe5 &'
alias mysqlstop='/opt/local/bin/mysqladmin5 -u root -p shutdown'

# Finished adapting your PATH environment variable for use with MacPorts.
export MAVEN_OPTS="-Xms512 -Xmx512m -XX:MaxPermSize=258m"
# export JAVA_OPTS="-Xms512m -Xmx512m -XX:MaxPermSize=258m"
# export JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home/
export JAVA_HOME=`/usr/libexec/java_home 1.8`
export PATH=$HOME/phonegap/lib/android/bin:$JAVA_HOME/bin:/usr/local/Cellar/ruby/1.9.3-p194/bin:$PATH:/Users/tawus/andriod/sdk/tools:$HOME/phonegap/lib/ios/bin:/opt/node/bin:
export GROOVY_HOME=/usr/local/Cellar/groovy/1.8.6/libexec
export GRADLE_OPTS="-Xms512m -Xmx512m -XX:MaxPermSize=256m"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

export FIGNORE=.svn


export PATH=/Users/tawus/bin/Sencha/Cmd/3.0.2.288:$PATH

export SENCHA_CMD_3_0_0="/Users/tawus/bin/Sencha/Cmd/3.0.2.288"
export XERIC_REPO_USERNAME=taha.hafeez
export XERIC_REPO_PASSWORD=super*coder!

alias acurl="curl -s --proto '=https' -g -H 'Authorization: Basic dGF3dXM6c3VwZXIxMjNtYW4='"
