(defproject xeric/i18n "0.2.2-SNAPSHOT"
  :description "i18n for clojure"
  :dependencies 
  [[org.clojure/clojurescript "_"]
   [com.cemerick/clojurescript.test "_" :scope "test"]]

  :parent [clojure-libs]
  :plugins [[lein-modules "0.3.9"]]
  :source-paths ["src/clj", "src/cljs"]
  :test-paths ["test/clj"]
  :cljsbuild 
  {:builds [{:id "test"
             :source-paths ["src/cljs" "test/cljs"]
             :notify-command 
             ["phantomjs" :cljs.test/runner
              "--web-security=false"
              "this.literal_js_was_evaluated=true"
              "--local-to-remote-url-access=true"
              "test-resources/polyfill.js"
              "target/cljs/testable.js"]
             :compiler {
                        :output-dir "target/cljs/out"
                        :output-to "target/cljs/testable.js"
                        :optimizations :simple
                        :pretty-print true}}]})
