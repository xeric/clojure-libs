(ns i18n.core)

(defprotocol IMessageSource
  (get-messages [this locale]))

(defprotocol IMessageConfig
  (message-source [this])
  (default-locale [this]))

(defrecord i18nConfig [message-config current-locale])

(def ^:dynamic *i18n-config*)

(def cache (atom {}))

(defn reset-locale! [locale]
  (swap! cache dissoc (keyword locale)))

(declare locale-messages)

(defn- merge-with-default-locale
  [messages config locale]
  (if (not= (default-locale config) locale)
    (merge (locale-messages config (default-locale config))
           messages)
    messages))

(defn locale-messages
  ([] (locale-messages (:message-config *i18n-config*)
                       (:current-locale *i18n-config*)))
  ([config locale]
     (when-not (contains? @cache locale)
       (let [messages (-> (message-source config)
                          (get-messages locale)
                          (merge-with-default-locale config locale))]
         (swap! cache assoc locale messages)))
     (get @cache locale)))

(defn t*
  [config locale key & args]
  (let [messages (locale-messages config locale)]
    (if (contains? messages key)
      (apply format (get messages key) args)
      (str "[missing-key: " key "]"))))

(defn t [key & args]
  (apply t* (:message-config *i18n-config*) (:current-locale *i18n-config*) key args))

