(ns i18n.core
  (:require [cljs.reader :as reader]
            [goog.dom :as dom]
            [goog.dom.dataset :as dataset]
            [goog.string :as gstring]
            [goog.string.format :as gformat]))

(extend-type js/NodeList
  ISeqable
  (-seq [array] (array-seq array 0)))

(def messages (atom {}))

(defn dom-messages 
  [className]
  (reduce (fn [messages element]
            (conj messages {:messages (-> (.getAttribute element "data-messages")
                                          reader/read-string)
                            :order    (js/parseInt (.getAttribute element "data-order"))}))
          [] (dom/getElementsByClass className)))

(defn merge-messages [messages]
  (->> messages
       (sort-by (fnil :order 1) <)
       (map :messages)
       (apply merge)))

(defn init!
  ([] (init! "messages"))
  ([className]
     (reset! messages (-> (dom-messages className)
                          merge-messages))))
         
(defn t
  [key & args]
  (if-let [message (get @messages key)]
    (apply gstring/format message args)
    (let [message (str "[missing-key: " key "]")]
      (do
        (.log js/console message)
        message))))

(defn add!
  ([messages] (add! messages 1))
  ([messages order]
     (let [body  (.-body js/document)
           child (doto (dom/createElement "div")
                   (.setAttribute "class" "messages hide")
                   (.setAttribute "data-messages" (pr-str messages))
                   (.setAttribute "data-order" (str order)))]
       (.appendChild body child))))

(defn clear!
  ([] (clear! "messages"))
  ([className]
     (loop [elements (dom/getElementsByClass className)]
       (when-let [element (first elements)]
         (dom/removeNode element)
         (recur (rest elements))))))

(init!)
