(ns i18n.test-core
  (:require [clojure.test :refer :all]
            [i18n.core :refer :all])
  (:import [i18n.core i18nConfig]))

(defn dummy-source [messages]
  (reify 
    IMessageSource
    (get-messages [this locale] 
      (locale messages))))

(defn dummy-config
  [messages locale]
  (reify
    IMessageConfig
    (default-locale [this] locale)
    (message-source [this] (dummy-source messages))))

(deftest messages-are-cached
  (let [config (dummy-config {:en {:foo "bar"}
                              :fr {:foo "bar-fr"}} :en)]
    (reset-locale! :en)
    (is (= (t* config :en :foo) "bar"))
    (is (= (t* config :en :foo-2) "[missing-key: :foo-2]"))
    (is (= (t* config :fr :foo) "bar-fr"))))

(deftest message-from-default-locale-is-used-if-message-is-missing
  (let [config (dummy-config {:fr {:foo "bar-fr"} 
                              :en {:foo "bar"
                                   :foo-2 "bar-2"}} :en)]
    (reset-locale! :en)
    (reset-locale! :fr)
    (is (= (t* config :fr :foo-2) "bar-2"))
    (is (= (t* config :fr :foo-3) "[missing-key: :foo-3]"))))

(deftest i18n-config-is-used-by-t
  (binding [*i18n-config* (i18nConfig. (dummy-config {:en {:foo "bar"}
                                                      :fr {:foo "bar-fr"}} :en)
                                       :fr)]
    (reset-locale! :fr)
    (reset-locale! :en)
    (is (= (t :foo) "bar-fr"))
    (is (= (t :foo-3) "[missing-key: :foo-3]"))))
