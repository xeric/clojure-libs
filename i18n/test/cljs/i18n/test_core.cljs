(ns i18n.test-core
    (:require-macros [cemerick.cljs.test 
                    :refer (is deftest are done testing use-fixtures)])
    (:require [cemerick.cljs.test :as t]
              [goog.dom :as dom]
              [i18n.core :as i18n]))

(deftest test-missing-messages-return-missing-key-message
  (is (= (i18n/t :bar) "[missing-key: :bar]")))

(deftest test-messages-are-translated-after-init
  (i18n/add! {:foo "bar"})
  (i18n/init!)
  (is (= (i18n/t :foo) "bar")))

(deftest test-messages-can-be-overridden
  (i18n/add! {:foo "bar"})
  (i18n/add! {:foo "override"} 2)
  (i18n/add! {:foo "later"})
  (i18n/init!)
  (is (= (i18n/t :foo) "override")))

(use-fixtures :each (fn [f]
                      (f)
                      (i18n/clear!)))
