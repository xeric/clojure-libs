(defproject xeric/mongos "0.4.0-SNAPSHOT"
  :description "Mongo based CRUD utils"
  :global-vars {*warn-on-reflection* true}
  :dependencies 
  [[xeric/i18n "0.2.2-SNAPSHOT"]
   [com.novemberain/monger "_"]
   [crypto-password "0.1.1"]
   [prismatic/schema "_"]]
  :plugins [[lein-modules "0.3.9"]])
