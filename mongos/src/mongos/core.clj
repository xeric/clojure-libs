(ns mongos.core
  (:refer-clojure :exclude [find remove count distinct list])
  (:import [org.bson.types ObjectId])
  (:require [monger.conversion :as conversion]
            [monger.query :as q]
            [monger.collection :as mc]
            [monger.core :as mg]
            [schema.core :as s]
            [mongos.validation :as v]))

(def ^{:dynamic true
       :tag com.mongodb.DB} *db*)

(defn ->map
  "Convert monger's dbobject to clojure map"
  [obj]
  (conversion/from-db-object obj true))

(defn object-id [id]
  (if (instance? ObjectId id)
    id
    (ObjectId. ^String id)))

(defn connect-via-uri
  [uri-string]
  (mg/connect-via-uri uri-string))

(defn disconnect!
  "Disconnect from mongodb"
  [conn]
  (mg/disconnect conn))

(defn time-now [] (java.util.Date.))

(defn db-schema [& {:as schema}]
  (assoc schema
    (s/optional-key :_id) ObjectId
    :version s/Int
    :created-on s/Inst
    :last-modified-on s/Inst))

(defprotocol IDBObject
  (create! [this obj])
  (update! [this obj] [this obj changes])
  (update-op! [this sel op] [this sel op opts])
  (delete! [this obj])
  (execute [this query])
  (by-id [this id])
  (list [this])
  (by-filter [this args]))

(defrecord DBObject [^String coll schema]
  IDBObject
  (create! [this obj]
    (let [now (time-now)
          obj (->> (assoc obj :version 1 :created-on now :last-modified-on now)
                   (v/validate-schema schema))
          _id (or (:_id obj) (ObjectId.))]
      (if (v/has-errors? obj)
        (throw (v/validation-exception obj))
        (mc/insert-and-return *db* coll (assoc obj :_id _id)))))
  (update! [this obj]
    (let [version (inc (:version obj))
          obj (->> (assoc obj :last-modified-on (time-now) :version version)
                   (v/validate-schema schema))]
      (if (v/has-errors? obj)
        (throw (v/validation-exception obj))
        (mc/save-and-return *db* coll obj))))
  (update! [this obj changes]
    (update! this (merge obj changes)))
  (update-op! [this sel op]
    (update-op! this sel op {}))
  (update-op! [this sel op opts]
    (mc/update *db* coll sel op opts))
  (delete! [this id]
    (mc/remove *db* coll {:_id id}))
  (execute [this f] (f *db* coll))
  (by-id [this id]
    (if-not (nil? id)
      (mc/find-one-as-map *db* coll {:_id (object-id id)})))
  (list [this] (by-filter this {:page-size 0}))
  (by-filter 
    [this {:keys [page-index page-size sort-column asc filter fields]
           :or   {page-index 0 page-size 0 sort-column :created-on
                  filter {}}}]
    (let [page-index (if (nil? page-index) 1 page-index)
          q-fields   (if fields q/fields (fn [m fields] m))
          sort-column (if sort-column sort-column :created-on)]
      (q/with-collection *db* coll
        (q/find filter)
        (q-fields fields)
        (q/sort {sort-column (if asc 1 -1)})
        (q/paginate :page page-index :per-page page-size)))))

(defn db-object
  ([coll] (db-object coll {}))
  ([coll schema]
     (DBObject. coll schema)))

(defmacro assoc-context
  [f]
  `(fn [db-object# & args#]
     (execute db-object#
              (fn [db# coll#]
                (apply ~f db# coll# args#)))))

(def find-one-as-map (assoc-context mc/find-one-as-map))
(def find-maps (assoc-context mc/find-maps))
(def find-map-by-id (assoc-context mc/find-map-by-id))
(def aggregate (assoc-context mc/aggregate))
(def find (assoc-context mc/find))
(def find-one (assoc-context mc/find-one))
(def distinct (assoc-context mc/distinct))
(def count (assoc-context mc/count))
(def update (assoc-context mc/update))
(def upsert (assoc-context mc/upsert))
(def save (assoc-context mc/save))
(def save-and-return (assoc-context mc/save-and-return))
(def remove (assoc-context mc/remove))
(def remove-by-id (assoc-context mc/remove-by-id))
(def insert (assoc-context mc/insert))
