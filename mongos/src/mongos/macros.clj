(ns mongos.macros
  (:require [monger.collection :as mc]
            [monger.query :as query]
            [monger.core :as mg])
  (:import [org.bson.types ObjectId]
           [java.util Date]))

(defn _create!
  [db coll doc]
  (let [now  (Date.)
        _id  (ObjectId.)]
    (mc/insert db coll (merge doc {:created-on now :last-modified-on now :_id _id}))
    _id))

(defn _update!
  [db coll doc-id changes]
  (mc/update-by-id db coll doc-id
                   (assoc changes :last-modified-on (Date.))))

(defn only-crud?
  [^String fn-name]
  (some #(.startsWith fn-name %1)
        ["aggregate" "insert" "update" "remove" "find"]))

(defn fn-map
  [db-fn coll-name]
  (->> (ns-publics 'monger.collection)
       (filter #(only-crud? (name (first %1))))
       (merge {'create!    (var _create!)
               'update!    (var _update!)})
       (map (fn [[fn-name fn-var]]
              [fn-name (list fn-var (list db-fn) coll-name)]))))

(defmacro collection-api
  [db-fn coll-name]
  (apply list
         'do
         `(defmacro ~'with-collection
            [& ~'body#]
            `(monger.query/with-collection ~'(~db-fn) ~'~coll-name ~@~'body#))
         (for [[fn-name prefix] (fn-map db-fn coll-name)]
           `(defn ~fn-name [& args#]
              (apply ~@prefix args#)))))




