(ns mongos.test
  (:require [mongos.core :as mongos]
            [monger.core :as mg]
            [monger.collection :as mc]))

(defn connection-fixture [uri]
  (fn [f]
    (let [{:keys [conn db]} (mongos/connect-via-uri uri)]
      (binding [mongos/*db* db]
        (f)
        (mongos/disconnect! conn)))))

(defn truncate-fixture [& colls]
  (fn [f]
    (doseq [coll colls]
      (mc/remove mongos/*db* coll))
    (f)))
