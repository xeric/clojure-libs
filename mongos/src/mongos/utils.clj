(ns mongos.utils
  (:require [crypto.password.scrypt :as encrypt-password]
            [monger.conversion :as conversion]))

(def VALID-CHARS "abcdefghijlkmnopqrstuvwxyz0123456789")

(defn create-random-key 
  "Create a random key of alphanumeric characters"
  ([] (create-random-key 16))
  ([length]
     (apply str (take length (repeatedly (fn [] 
                                           (rand-nth VALID-CHARS)))))))

(defn check-password [password encrypted]
  "Compares password with an encrypted password and returns true if 
   it is a match"
  (if (and password encrypted)
    (encrypt-password/check password encrypted)
    false))

(defn encrypt-password [password]
  "Encrypts a password"
  (if password
    (encrypt-password/encrypt password)))

(defn rand-long
  "Generate a random long number"
  []
  (long (rand Long/MAX_VALUE)))
