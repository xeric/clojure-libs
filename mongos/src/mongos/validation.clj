(ns mongos.validation
  (:require [schema.core :as s]
            [schema.macros :as s-macros]
            [schema.spec.leaf :as leaf]
            [schema.spec.core :as spec]
            [schema.utils :as s-utils]
            [clojure.string :as str]
            [clojure.walk :refer [prewalk]]))

(defrecord MaxCountSchema [l]
  s/Schema
  (spec [this]
    (leaf/leaf-spec (spec/precondition
                     this
                     #(<= (count %) l)
                     #(list '<= (list 'count %) l))))
  (explain [this] (list '<= 'count l)))

(defn max-count [v]
  (MaxCountSchema. v))

(defrecord MinCountSchema [l]
  s/Schema
  (spec [this]
    (leaf/leaf-spec (spec/precondition
                     this #(>= (count %) l)
                     #(list '>= (list 'count (s-utils/value-name %)) l))))
  (explain [this] (list '>= 'count l)))

(defn min-count [v]
  (MinCountSchema. v))

(defrecord EqCountSchema [l]
  s/Schema
  (spec [this]
    (leaf/leaf-spec (spec/precondition
                     this #(= (count %) l)
                     #(list '= (list 'count (s-utils/value-name %)) l))))
  (explain [this] (list '= 'count l)))

(defn eq-count [v]
  (EqCountSchema. v))

(defrecord MaxSchema [max]
  s/Schema
  (spec [this]
    (leaf/leaf-spec (spec/precondition
                     this #(<= % max)
                     #(list '< (s-utils/value-name %) max))))
  (explain [this] (list '< max)))

(defn max-value [max]
  (MaxSchema. max))

(defrecord MinSchema [min]
  s/Schema
  (spec [this]
    (leaf/leaf-spec (spec/precondition
                     this #(>= % min)
                     #(list '> (s-utils/value-name %) min))))
  (explain [this] (list '> min)))

(defn min-value [min]
  (MinSchema. min))

(defn set-error
  ([obj message] (set-error obj :composite message))
  ([obj field message]
     (update-in obj [:errors field] conj message)))

(defn ->fail-explanation
  [e] e)

(defn remove-blanks
  [obj]
  (if (map? obj)
    (into {} (remove (fn [[k v]] (or (nil? v)
                                     (and (string? v)
                                          (str/blank? v)))) obj))
    obj))

(defn validate-schema [schema obj]
  (if-let [errors (s/check schema (remove-blanks obj))]
    (let [errors (prewalk ->fail-explanation errors)]
      (assoc obj :errors (if (map? errors) errors {:composite [errors]})))
    obj))

(defn has-errors? [obj]
  (contains? obj :errors))

(defn validation-exception [data]
  (ex-info (str "Validation failed: " (pr-str data))
           {:type :validation
            :data data}))

(defrecord Predicate [p pred-name]
  s/Schema
  (spec [this]
    (let [f #(s-macros/try-catchall
              (p %)
              (catch e (.printStackTrace e) 'throws?))]
      (leaf/leaf-spec (spec/precondition
                       this #(boolean (f %))
                       #(list 'pred pred-name)))))
  (explain [this] (list 'pred pred-name)))

(defn pred
  ([p] (pred p p))
  ([p pred-name]
     (Predicate. p pred-name)))

(defn validate
  ([v] (validate v v))
  ([v validator-name]
     (Predicate. v validator-name)))

(defmacro defpred
  [name pred-fn error-message]
  `(def ~name (pred (fn [x#]
                      (if-not (~pred-fn x#)
                        [~error-message x#]))
                    '~pred-fn)))

(defpred Int integer? :not-an-integer)
(defpred Str string? :not-a-string)
(defpred Keyword keyword? :not-a-keyword)
(defpred Symbol symbol? :not-a-symbol)

(defmacro defvalidate
  [name fns & forms]
  `(defn ~name [obj#]
     (let [res# ((comp (reverse fns)) obj#)]
       (if (has-errors? res#)
         (throw (validation-exception res#))
         (do ~@forms)))))
