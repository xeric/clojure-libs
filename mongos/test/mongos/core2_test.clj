(ns mongos.core2-test
  ;; Without authentication.
  (:import [org.bson.types ObjectId]) 
  (:require [clojure.test :refer :all]
            [monger.collection :as mc]
            [mongos.core :as c]
            [mongos.test :refer :all]
            [schema.core :as s]))

(use-fixtures :once (connection-fixture "mongodb://localhost/mongos_test"))
(use-fixtures :each (truncate-fixture "person"))

(def person {:name "john" :full-name "john-doe" :age 25})

(def person-schema
  (c/db-schema
   (s/required-key :name) s/Str
   (s/required-key :age) long
   (s/required-key :full-name) s/Str
   (s/optional-key :emails) s/Str))

(def Person (c/db-object "person" person-schema))

(defn by-name [name]
  (c/execute Person (fn [db coll] (mc/find-one-as-map db coll {:name name}))))

(deftest object-is-created
  (let [p (c/create! Person person)]
    (testing "all data is saved"
      (is (= person (select-keys p [:name :full-name :age]))))
    (testing "creation-time is saved"
      (is (contains? p :created-on)))
    (testing "last-modified-time-is-saved"
      (is (contains? p :last-modified-on)))
    (testing "version is saved"
      (is (= (:version p) 1)))))

(deftest operations-can-be-performed
  (let [p (c/create! Person person)]
    (c/update-op! Person {:_id (:_id p)} {"$push" {:emails "joe@example.com"}})
    (let [p (c/by-id Person (:_id p))]
      (is (= (first (:emails p)) "joe@example.com")))))

(deftest test-assoc-context
  (let [p (c/create! Person person)]
    (is (= (count (c/find-maps Person {:name (:name person)})) 1))
    (is (= (:name (c/find-one-as-map Person
                                     {:name (:name person)})) (:name person)))))
