(ns mongos.core-test
  (:import [org.bson.types ObjectId]) 
  (:require [clojure.test :refer :all]
            [monger.collection :as mc]
            [mongos.core :as c]
            [mongos.test :refer :all]
            [schema.core :as s]))

;; Create a user with the following command
;; db.createUser({user: "test-user", pwd: "secret",
;;                roles: [{role: "readWrite", db : "mongos_test"}]});
;;
(use-fixtures :once (connection-fixture
                     "mongodb://test-user:secret@localhost/mongos_test"))
(use-fixtures :each (truncate-fixture "person"))

(def person {:name "john" :full-name "john-doe" :age 25})

(def person-schema
  (c/db-schema
   (s/required-key :name) s/Str
   (s/required-key :age) long
   (s/required-key :full-name) s/Str
   (s/optional-key :emails) s/Str))

(def Person (c/db-object "person" person-schema))

(defn by-name [name]
  (c/execute Person (fn [db coll] (mc/find-one-as-map db coll {:name name}))))

(deftest object-is-created
  (let [p (c/create! Person person)]
    (testing "all data is saved"
      (is (= person (select-keys p [:name :full-name :age]))))
    (testing "creation-time is saved"
      (is (contains? p :created-on)))
    (testing "last-modified-time-is-saved"
      (is (contains? p :last-modified-on)))
    (testing "version is saved"
      (is (= (:version p) 1)))))

(deftest on-schema-validation-failure-object-is-not-created
  (is (thrown? clojure.lang.ExceptionInfo (c/create! Person {:name "john"}))))

(deftest object-is-updated
  (let [p (c/create! Person person)
        _ (Thread/sleep 100)
        u (c/update! Person p {:age 26 :name "john doe"})]
    (testing "last-modified-date changes"
      (is (not= (:last-modified-on u) (:created-on u))))
    (testing "version changes"
      (is (= (inc (:version p)) (:version u))))
    (testing "changes are persisted"
      (is (= (:age u) 26))
      (is (= (:name u) "john doe")))))

(deftest on-schema-validation-failure-object-is-not-updated
  (let [p (c/create! Person person)]
    (is (thrown? clojure.lang.ExceptionInfo 
                 (c/update! Person (dissoc p :name))))))

(deftest object-is-retrieved-by-id
  (let [p (c/create! Person person)]
    (is (= (:name (c/by-id Person (:_id p))) (:name p)))))

(deftest object-can-be-listed
  (let [ps (doall (repeatedly 5 #(c/create! Person person)))]
    (is (= (count (c/list Person)) 5))))

(deftest operations-can-be-performed
  (let [p (c/create! Person person)]
    (c/update-op! Person {:_id (:_id p)} {"$push" {:emails "joe@example.com"}})
    (let [p (c/by-id Person (:_id p))]
      (is (= (first (:emails p)) "joe@example.com")))))

(deftest test-assoc-context
  (let [p (c/create! Person person)]
    (is (= (count (c/find-maps Person {:name (:name person)})) 1))
    (is (= (:name (c/find-one-as-map Person {:name (:name person)})) (:name person)))))
