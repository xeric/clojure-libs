(ns mongos.macros-test
  (:refer-clojure :exclude [find remove update])
  (:require [mongos.macros :refer :all]
            [monger.core :as mg]
            [clojure.test :refer :all]))

(def mongo (mg/connect-via-uri "mongodb://localhost/mongos_test"))

(def user-db "user")

(collection-api (constantly (:db mongo)) user-db)

(use-fixtures :each (fn [f] (remove) (f)))

(deftest simple-db-access-test
  (let [uid (create! {:a 10 :b 20})]
    (testing "Look up by id"
      (is (= (some? (:_id (find-map-by-id uid))))))
    (testing "Simple lookup"
      (is (some? (find-one-as-map {:a 10})))
      (is (nil? (find-one-as-map {:a 11}))))
    (testing "Document is updated"
      (update-by-id uid {"$set" {:a 200}})
      (is (= (:a (find-one-as-map {:b 20})) 200)))
    (testing "Document is removed"               
      (remove-by-id uid)
      (is (nil? (find-map-by-id uid))))))
