(ns mongos.utils-test
  (:require [clojure.test :refer :all]
            [mongos.utils :refer :all]))

(deftest test-random-key
  (is (not= (create-random-key) (create-random-key)))
  (is (= (count (create-random-key 8)) 8)))

(deftest test-password-encryption
  (let [p (encrypt-password "secret")]
    (is (not= p "secret"))
    (is (check-password "secret" p))))
