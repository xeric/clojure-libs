(ns mongos.validation-test
  (:require [clojure.test :refer :all]
            [schema.core :as s]
            [mongos.validation :as v]))

(deftest error-is-set
  (testing "default error messages are associated with :composite"
    (let [obj (v/set-error {} "missing values")]
      (is (v/has-errors? obj))
      (is (= (get-in obj [:errors :composite]) '("missing values")))))
  (testing "field error messages are set"
    (let [obj (v/set-error {} :username "username already exists")]
      (is (v/has-errors? obj))
      (is (= (get-in obj [:errors :username] "username already exists"))))))

(deftest test-validate-schema
  (let [obj (v/validate-schema {(s/required-key :a) long} {})]
    (is (v/has-errors? obj))
    (is (= (get-in obj [:errors :name] :required)))))

(deftest test-max-count
  (testing "reports errors if count > max"
    (let [obj (v/validate-schema {(s/required-key :a) 
                                  (v/max-count 5)} {:a [1 2 3 4 5 6]})]
      (is (v/has-errors? obj))))
  (testing "reports no error if count <= max"
    (let [obj (v/validate-schema {(s/required-key :a) 
                                  (v/max-count 5)} {:a [1 2 3 4]})]
      (is (not (v/has-errors? obj))))))

(deftest test-min-count
  (testing "reports errors if count < min"
    (let [obj (v/validate-schema {(s/required-key :a) 
                                  (v/min-count 5)} {:a [1 2 3 4]})]
      (is (v/has-errors? obj))))
  (testing "reports no error if count >= min"
    (let [obj (v/validate-schema {(s/required-key :a) 
                                  (v/min-count 5)} {:a [1 2 3 4 5 6]})]
      (is (not (v/has-errors? obj))))))

(deftest test-min-value
  (testing "reports errors if value < min"
    (let [obj (v/validate-schema {(s/required-key :a) 
                                  (v/min-value 5)} {:a 4})]
      (is (v/has-errors? obj))))
  (testing "reports no error if value >= min"
    (let [obj (v/validate-schema {(s/required-key :a) 
                                  (v/min-value 5)} {:a 6})]
      (is (not (v/has-errors? obj))))))

(deftest test-max-value
  (testing "reports errors if value > max"
    (let [obj (v/validate-schema {(s/required-key :a) 
                                  (v/max-value 5)} {:a 10})]
      (is (v/has-errors? obj))))
  (testing "reports no error if value <= max"
    (let [obj (v/validate-schema {(s/required-key :a) 
                                  (v/max-value 5)} {:a 4})]
      (is (not (v/has-errors? obj))))))

#_(deftest test-pred-errors
  (testing "pred for whole objects work"
    (let [obj (v/validate-schema 
               (s/both 
                {(s/required-key :a) long
                 (s/required-key :b) long}
                (v/pred
                 (fn [x] (if-not (= x 10) [:not-eq-to-10 x])))) 9)]
      (is (v/has-errors? obj))
      (is (= (get-in obj [:errors :composite]) 10)))))

#_(deftest test-Int
  (testing "error is recorded if it is not an integer"
    (is (= (-> (v/validate-schema {(s/required-key :a) v/Int} {:a "a"})
               (get-in [:errors :a]))
           [:not-an-integer "a"])))
  (testing "no error is recorded if it is an integer"
    (is (-> (v/validate-schema {(s/required-key :a) v/Int} {:a 10})
            v/has-errors?
            not))))

#_(deftest test-Keyword
  (testing "error is recorded if it is not a keyword"
    (is (= (-> (v/validate-schema {(s/required-key :a) v/Keyword} {:a "a"})
               (get-in [:errors :a]))
           [:not-a-keyword "a"])))
  (testing "no error is recorded if it is a keyword"
    (is (-> (v/validate-schema {(s/required-key :a) v/Keyword} {:a :test})
            v/has-errors?
            not))))

#_(deftest test-Str
  (testing "error is recorded if it is not a keyword"
    (is (= (-> (v/validate-schema {(s/required-key :a) v/Str} {:a 10})
               (get-in [:errors :a]))
           [:not-a-string 10])))
  (testing "no error is recorded if it is a keyword"
    (is (-> (v/validate-schema {(s/required-key :a) v/Str} {:a "a"})
            v/has-errors?
            not))))         

(deftest test-remove-blanks
  (is (= (v/remove-blanks 10) 10))
  (is (= (v/remove-blanks {:a 20 :b nil}) {:a 20}))
  (is (= (v/remove-blanks {:a " "}) {}))
  (is (= (v/remove-blanks {:a "" :b 20 :c nil}) {:b 20})))
