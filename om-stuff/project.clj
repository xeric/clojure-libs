(defproject xeric/om-stuff "0.3.0-SNAPSHOT"
  :description "om based components"
  :dependencies 
  [[org.clojure/clojurescript "_"]
   [org.omcljs/om "_" :exclusions [cljsjs/react]]
   [cljsjs/react-with-addons "0.13.3-0"]
   [kioo "0.4.1-SNAPSHOT" :exclusions [om cljsjs/react]]
   [xeric/i18n :version]
   [org.clojure/core.async "_"]
   [cljs-http "0.1.37"]
   [bidi "1.20.3"]
   [xeric/om-test :version :scope "test" :exclusions [cljsjs/react]]
   [com.cemerick/clojurescript.test "_" :scope "test"]]

  :plugins 
  [[lein-modules "0.3.9"]]
 
  :cljsbuild 
  {:builds [{:id "dev"
             :source-paths ["src" "test" "resources" "test-resources"]
             :compiler {
                        :output-to "target/om_stuff.js"
                        :output-dir "target/out"
                        :optimizations :none
                        :source-map true}}
                       
            {:id "test"
             :source-paths ["src" "test" "resources" "test-resources"]
             :notify-command
             ["phantomjs" :cljs.test/runner
              "--web-security=false"
              "this.literal_js_was_evaluated=true"
              "--local-to-remote-url-access=true"
              "test-resources/js/polyfill.js"
              "test-resources/js/react-with-addons.js"
              "target/cljs/testable.js" ]
             :compiler {
                        :output-to "target/cljs/testable.js"
                        :optimizations :whitespace
                        :pretty-print false}}]})
