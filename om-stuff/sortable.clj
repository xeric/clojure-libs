(ns om-stuff.sortable
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put!]]
            [goog.dom :as gdom]
            [goog.style :as style]
            [goog.dom.classlist :as classlist]
            [goog.events :as events]
            [goog.fx.AbstractDragDrop.EventType :as DDEventType])
  (:import [goog.fx DragDropGroup]))

(defn list-item
  "A simple list-item implementation. Displays the :text value"
  [column owner opts]
  (om/component
   (dom/li nil (:text column))))

(defn- insert-after
  "Inserts element at s-index after t-index in the columns
   vector"
  [columns s-index t-index]
  (prn s-index t-index)
  (let [source (get columns s-index)
        columns (vec (map-indexed
                      (fn [i x] (if (= i s-index) nil x))
                      columns))]
    (->> (concat (take t-index columns) [source]
                (drop t-index columns))
         (remove nil?)
         vec)))

(defn before?
  "Checks if the mouse is positioned before or after the 
   current target"
  [e]
  (let [target (.-dropTargetElement e)
        clientY (.-clientY e)
        offset-top (.-y (style/getClientPosition target))
        rel-y (- clientY offset-top)
        height (/ (.-offsetHeight target) 2)]
    (< rel-y height)))

(defn add-listeners
  "Add listeners to manage sorting"
  [columns target ch]
  (events/listen target DDEventType/DRAGOUT
                 (fn [e]
                   (when (identical? (.-dropTarget e) (.-currentTarget e))
                     (let [target (.-dropTargetElement e)]
                       (classlist/remove target "s-target")))))

  (events/listen target DDEventType/DRAGOVER
                 (fn [e]
                   (when (identical? (.-dropTarget e) (.-currentTarget e))
                     (classlist/add (.-dropTargetElement e) "s-target"))))
  
  (events/listen target DDEventType/DROP
                 (fn [e]
                   (let [s-index (-> e .-dragSourceItem .-data)
                         t-index (-> e .-dropTargetItem .-data)
                         target  (.-dropTargetElement e)
                         t-index (if (before? e) t-index (inc t-index))]
                     (classlist/remove target "s-target")
                     (om/transact! columns #(insert-after % s-index t-index))))))
  
(defn put-dragndrop-events
  [target ch]
  (doseq [event [DDEventType/DRAGOVER
                 DDEventType/DRAGSTART
                 DDEventType/DRAG
                 DDEventType/DROP
                 DDEventType/DRAGEND]]
    (events/listen target event
                   (fn [e]
                     (put! ch {:id (keyword event)
                               :event e
                               :item (-> e .-dragSourceItem .-data)})))))

(defn setup-group [group elements]
  (loop [elements elements i 0]
    (when-let [el (first elements)]
      (.addItem group el i)
      (recur (rest elements) (inc i)))))

(defn setup-dnd [owner] 
  (let [container (om/get-node owner)
        group  (om/get-state owner :group)
        elements (array-seq (gdom/getChildren container))]
    (.removeItems group)
    (setup-group group elements)
    (.addTarget group group)
    (.init group)))

(defn sortable
  [columns owner
   {:keys [parent child ch]
    :or {parent dom/ul child list-item}
    :as opts}]
  (reify
    om/IDidMount
    (did-mount [_]
      (let [group (DragDropGroup.)]
        (om/set-state! owner :group group)
        (if ch
          (add-listeners columns group ch)
          (put-dragndrop-events group ch))
        (setup-dnd owner)))
    om/IDidUpdate
    (did-update [_ _ _]
      (setup-dnd owner))
    om/IRenderState
    (render-state [_ state]
      (apply parent nil
             (om/build-all child columns {:opts opts})))))
