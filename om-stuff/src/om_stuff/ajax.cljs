(ns om-stuff.ajax
  (:refer-clojure :exclude [get])
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:import  [goog History Uri]
            [goog.Uri QueryData])
  (:require [goog.dom.classlist :as classlist]
            [cljs-http.client :as http]
            [cljs.core.async :as async :refer [put! <! chan]]
            [om.core :as om :include-macros true]
            [goog.dom :as gdom]
            [cljs.reader]))

(def routes-mock identity)

(def routes (if-let [routes (aget js/document "Routes")]
              (cljs.reader/read-string routes)
              routes-mock))

(defn get-params []
  (let [qd (.getQueryData (Uri. (.-search js/location)))]
    (into {}
          (for [key (.getKeys qd)]
            [(keyword key) (.get qd key)]))))

(defn parse-url
  [url params]
  {:pre [(some? url)]}
  (let [params (or params {})
        matches (set (map #(-> % second keyword) (re-seq field-matcher url)))
        separator (if (= (.indexOf url "?") -1) "?" "&")
        append-params (fn [url params]
                        (if (empty? params)
                          url
                          (apply str url separator params)))
        replacer (fn [_ match]
                   (str "/" (if-let [r (get params (keyword match))]
                              r (str ":" match))))
        url (string/replace url field-matcher replacer)]
    (->> (apply dissoc params matches)
         (map (fn [[k v]] (str (name k) "=" v)))
         (interpose "&")
         (append-params url))))

(defn path-for
  "Routing function for the server side"
  [route-name & {:as params}]
  {:pre [(some? route-name)]}
  (if-let [route (routes route-name)]
    (parse-url route params)
    (throw (ex-info (str "path not found: " route-name) {}))))

(defn absolute-path-for
  [route-name & {:as params}]
  (if-let [route (routes route-name)]
    (let [host  (.. js/window -location -host)
          proto (.. js/window -location -protocol)]
      (str proto "//" host (parse-url route params)))
    (throw (ex-info (str "path not found: " route-name) {}))))

(defn iframe?
  []
  (try
    (not= (.-self js/window) (.-top js/window))
    (catch js/Error e true)))

(defn redirect
  "Redirect to a given url."
  ([url]
   (try
     (if (iframe?)
       (set! (-> js/window .-top .-location .-href) url)
       (.replace js/location url))))
  ([url params] (redirect url params nil))
  ([url params anchor]
   (let [uri (Uri. url)
         query-data (QueryData.)]
     (doseq [[k v] params]
       (.add query-data (name k) v))
     (.setQueryData uri query-data)
     (redirect (str (.toString uri) anchor)))))

(defn redirect-absolute
  [& args]
  (str (path-for :home) (apply redirect args)))

(defn get-params []
  (let [qd (.getQueryData (Uri. (.-search js/location)))]
    (into {}
          (for [key (.getKeys qd)]
            [(keyword key) (.get qd key)]))))

(defn stop-ajax-wait! []        
  (classlist/remove (gdom/getElementByClass "app-content") "loading"))

(defn start-ajax-wait! []
  (classlist/add (gdom/getElementByClass "app-content") "loading"))

(def default-media-params :edn-params)
(def error-chan (chan))

(defn status? [expected {:keys [status]}]
  (= expected status))

(defn ok?
  [{:keys [status]}]
  (and (>= status 200) (< status 300)))

(def malformed? (partial status? 400))

(defn ajax*
  [method path & [params options]]
  (let [bg? (:bg? options)
        c (method path
                  (-> (assoc options
                             default-media-params (dissoc params :errors))
                      (dissoc :bg?)))
        o (chan)]
    (if-not bg? (start-ajax-wait!))
    (go
      (let [result (<! c)]
        (if-not bg? (stop-ajax-wait!))
        (when-not (ok? result)
          (put! error-chan result))
        (let [response (:body result)]
          (put! o response)
          (if (:redirect? response)
            (redirect (:location response))))))
    o))

(def get    (partial ajax* http/get))
(def put    (partial ajax* http/put))
(def post   (partial ajax* http/post))
(def delete (partial ajax* http/delete))

(defn get-bg [path & [params options]]
  (ajax* http/get path params (assoc options :bg? true)))

