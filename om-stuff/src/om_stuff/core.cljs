(ns om-stuff.core
  (:require-macros [cljs.core.async.macros :refer [go-loop]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [secretary.core :as secretary]
            [clojure.string :as string]
            [cljs.core.async :as async :refer [put! <! chan sub]]
            [om-stuff.ajax :as ajax]
            [i18n.core :as i18n :refer [t]]
            [goog.history.EventType :as EventType]
            [goog.events :as events]
            [cljs-time.core :as cljs-time]
            [cljs-time.format :as time-format]
            [cljs-time.coerce :as time-coerce])
  (:import [goog History]))

(def field-matcher #"/:([^/]+)")

(def history (atom nil))

(extend-type js/NodeList
  ISeqable
  (-seq [array] (array-seq array 0)))

(defn show 
 ([^boolean show?] (show show? "block"))
 ([^boolean show? display]
    (if show?
      #js {:display display}
      #js {:display "none"})))

(defn hide
  ([hide?] (hide hide? "block"))
  ([hide? display]
   (show (not hide?) display)))

(defn value [el]
  (-> el .-target .-value))

(defn checked [el]
  (-> el .-target .-checked))

(defn config [el]
  (-> (cljs.reader/read-string
       (.getAttribute el "data-config"))))

(declare head-cell row cell-renderer)

(defn grid 
  [{:keys [headers data params] :as app} owner opts]
 (reify
   om/IRenderState
   (render-state [_ state]
     (dom/table 
      #js {:className "table table-condensed listing"}
      (dom/thead
       nil
       (apply dom/tr nil
              (om/build-all head-cell headers
                            {:init-state state :opts opts})))
      (apply dom/tbody nil
             (mapv (fn [row-data]
                     (om/build row
                               {:row row-data :headers headers
                                :key key}
                               {:opts opts}))
                   data))))))

(defn sort-list [key rm x])

(defn- head-cell 
  [{:keys [title sortable? key css-class]} owner {:keys [rm]}]
 (reify
   om/IRenderState
   (render-state [_ state]
     (dom/th  #js {:className (or css-class (name key))}
             (if sortable? 
               (dom/a #js {:onClick (partial sort-list key rm)
                           :href "#"} title)
               title)))))

(defn- row [{:keys [headers row link key]} owner opts]
  (reify
    om/IRenderState
    (render-state [_ state]
      (apply dom/tr nil
             (mapv (fn [{:keys [key renderer css-class]
                         :as header
                         :or {renderer cell-renderer}}]
                     (dom/td #js {:className (or css-class (name key))}
                             (om/build renderer
                                       {:header header :row row
                                        :link link :key key}
                                       {:opts opts})))
                   headers)))))

(defn- cell-renderer [{:keys [key row]} _]
 (reify
   om/IRenderState
   (render-state [_ state]
     (dom/span nil (str (clojure.core/get row key))))))

(defn date-renderer-fn [formatter]
  (fn [{:keys [key row]} _]
    (om/component
     (let [v (clojure.core/get row key)]
       (dom/span nil (if v (formatter v)))))))

(def locale-date-renderer (date-renderer-fn (memfn toLocaleDateString)))
(def locale-time-renderer (date-renderer-fn (memfn toLocaleTimeString)))

(declare form-editor render-field form-group input-component)

(defn form-editor
  [{:keys [fields obj] :as editor} owner opts]
  (om/component
   (apply dom/div nil
          (mapv #(render-field obj % opts) fields))))

(defn render-field 
 [obj {:keys [key type value] :as field} opts]
 (if (or (= type :checkbox)
         (= type :radio))
   (om/build input-component [field obj] {:opts opts})
   (om/build form-group [field obj] {:opts opts})))

(defn form-group 
  [[{:keys [title hidden? key] :as field} obj] owner opts]
  (om/component
   (let [error (get-in obj [:errors key])]
     (dom/div #js {:style (show (not hidden?))
                   :className (str "form-group " (name key)
                                   (if error " has-error has-feedback"))}
              (dom/label #js {:labelFor key} title)
              (om/build input-component [field obj] {:opts opts})
              (dom/span #js {:className 
                             "glyphicon glyphicon-remove form-control-feedback"
                             :style (show error "inline")})
              (dom/span #js {:className "help-block"
                             :style (show error)} 
                        error)))))

(defmulti input-component (fn [[field obj] value] (:type field)))

(defn save-value [e field obj]
  (let [translate (:translate @field)
        key       (:key @field)
        target    (.-target e)
        value     (.-value target)
        value'    (if translate (translate value) value)]
    (om/update! obj key value')))

(defn save-checked [e field obj]
 (om/update! obj (:key @field)
             (-> e .-target .-checked)))

(defn- save-selected [e field obj]
 (let [n (.-target e)
       value (if (.getAttribute n "multiple") 
               (->> (.querySelectorAll n "option")
                    (filter #(.-selected %))
                    (mapv #(.-value %)))
               (.-value n))]
   (om/update! obj (:key @field) value)))

(defmethod input-component :password 
 [[field obj]]
 (om/component
  (dom/input #js {:type "password"
                  :name (name (:key field))
                  :className "form-control"
                  :value (get obj (:key field))
                  :onChange #(save-value % field obj)})))

(defmethod input-component :email 
 [[field obj]]
 (om/component
  (dom/input #js {:type "email"
                  :name (name (:key field))
                  :value (get obj (:key field))
                  :className "form-control"
                  :onChange #(save-value % field obj)})))

(defmethod input-component :default
 [[field obj]]
 (om/component
  (dom/input #js {:type "text"
                  :name (name (:key field))
                  :value (get obj (:key field))
                  :className "form-control"
                  :onChange #(save-value % field obj)})))

(defmethod input-component :checkbox
 [[field obj] owner]
 (om/component
  (dom/label #js {:className "checkbox"}
             (dom/input #js {:type "checkbox"
                             :name (name (:key field))
                             :checked (get obj (:key field))
                             :onChange #(save-checked % field obj)})
             (:title field))))

(defmethod input-component :radio
 [[field obj] owner]
 (om/component
  (dom/label #js {:className "radio"}
            (dom/input #js {:type "radio"
                            :name (name (:key field))
                            :checked (get obj (:key field))
                            :onChange #(save-checked % field obj)})
            (:title field))))

(defmethod input-component :select 
 [[{:keys [options multiple? key] :as field} obj]]
 (om/component
  (apply dom/select 
         #js {:value (let [value (get obj key)]
                       (if multiple? (clj->js value) value))
              :multiple multiple?
              :name (name key)
              :ref (name key)
              :className "form-control"
              :onChange #(save-selected % field obj)}
         (mapv (fn [[k v]] 
                (dom/option #js {:value k} v)) options))))

(defmethod input-component :textarea
 [[{:keys [rows value] :as field} obj]]
 (om/component
  (dom/textarea #js {:value (get obj (:key field))
                     :rows rows
                     :name (name (:key field))
                     :className "form-control"
                     :onChange #(save-value % field obj)})))

(defn field-values [{:keys [fields obj]}]
  (if (nil? (:_id obj))
    obj
    (reduce
     (fn [values {:keys [key value]}]
       (assoc values key (get obj key)))
     {} fields)))

