(ns om-stuff.kioo
  (:require [kioo.om :as kioo
             :refer-macros  [defsnippet deftemplate component]
             :refer [do-> content add-class listen set-attr do->]]
            [om.core :as om :include-macros true]
            [i18n.core :refer [t]]
            [cljs.core.async :refer [sub chan pub put! unsub]]
            [clojure.string :as string]
            [cljs.reader])
  (:import [goog Uri]))

(defn translate-messages []
  (fn [node]
    (let [key (-> node :attrs :data-message)]
      (assoc node :content [(t (keyword key))]))))

(defn render-if [^boolean condition?]
  (fn [node]
    (update-in node [:attrs :style :display]
               (fn [display]
                 (if condition? display "none")))))

(deftemplate base-page "templates/om_stuff/base.html"
  [loading? page-content]
  {[:#page] (do->
             (content page-content)
             (add-class (if loading? "loading")))})

(defn translate-messages []
  (fn [node]
    (let [key (-> node :attrs :data-message)]
      (assoc node :content [(t (keyword key))]))))

(defn remove-content
  []
  (fn [node]))

(defn form-el
  [owner key]
  (do-> (set-attr :value (om/get-state owner key))
        (listen :on-change
                #(om/set-state! owner key (value %)))))

(defn form-el-checked
  [owner key]
  (do-> (set-attr :checked (om/get-state owner key))
        (listen :on-change
                #(om/set-state! owner key (checked %)))))

(defn form-el-unchecked
  [owner key]
  (do-> (set-attr :checked (not (om/get-state owner key)))
        (listen :on-change
                #(om/set-state! owner key (not (checked %))))))

(defn kioo-disabled
  [disabled?]
  (if disabled?
    (set-attr :disabled "disabled")
    identity))

(defn kioo-hide
  ([hide?] (kioo-hide hide? "block"))
  ([hide? display]
   (set-attr :style (hide hide? display))))

