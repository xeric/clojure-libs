(ns om-stuff.palette
  (:require-macros [cljs.core.async.macros :refer [go-loop]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [om-stuff.kioo :include-macros true :refer-macros [kioo-component]]
            [kioo.om :as kioo
             :refer [content add-class set-attr set-style do-> substitute listen]]
            [cljs.core.async :refer [put! chan]]
            [clojure.string :as string]
            [om-stuff.sortable :refer [sortable]]
            [om-stuff.core :as om-stuff]
            [i18n.core :as i18n :refer [t]]))

(defn contains-text?
  [text find-text]
  (or (nil? text)
      (nil? find-text)
      (>= (.indexOf (string/lower-case text)
                    (string/lower-case find-text)) 0)))

(defn matches-tag?
  [tags tag]
  (if (and tag (not (string/blank? tag)))
    (some? (first (filter #(= tag %) tags)))
    true))

(defn unselected
  [columns selected {:keys [tag text]}]
  (let [selected  (set selected)
        remove-fn #(or (contains? selected %)
                       (not (matches-tag? (:tags %) tag))
                       (not (contains-text? (:text %) text)))]
    (vec (remove remove-fn columns))))

(defn- event-handler [ch owner columns selected]
  (go-loop [{:keys [action column] :as e} (<! ch)]
    (case action
      :filter     (om/update-state! owner :filter #(merge % (:filter e)))
      :add        (om/transact! selected #(conj % (om/value column)))
      :remove     (om/transact! selected
                                (fn [columns]
                                  (vec (remove #(= (om/value column) %) columns))))
      :add-all    (let [filter (om/get-state owner :filter)
                        unsel (unselected columns @selected filter)]
                    (if (= (count columns) (count unsel))
                      (om/update! selected (om/value columns))
                      (om/transact! selected #(vec (concat % unsel)))))
      :remove-all (om/update! selected []))
    (recur (<! ch))))

(defn- act
  [ch action column]
  (put! ch {:action action :column column}))

(defn- selection-item
  [column owner {:keys [ch]}]
  (om/component
   (kioo-component
    [:.p-right :.list-group-item]
    {[kioo/root]
     (listen :on-double-click #(act ch :remove column))
     [kioo/root :> :span]
     (content (:text column))
     [:a]
     (listen :on-click #(act ch :remove column))})))

(defn- selection-view
  [columns owner {:keys [ch] :as opts}]
  (om/component
   (kioo-component
    [:.p-right]
    {[:.remove-all]
     (listen :on-click #(act ch :remove-all nil))
     [:.panel-body :ul]
     (content
      (om/build sortable columns
                {:opts (assoc opts :child selection-item)}))})))

(defn- source-item
  [column owner {:keys [ch]}]
  (om/component
   (kioo-component
    [:.p-left :ul :.list-group-item]
    {[kioo/root]
     (listen :on-double-click #(act ch :add column))
     [kioo/root :> :span]
     (content (:text column))
     [:a]
     (do->
      (listen :on-click #(act ch :add column)))})))

(defn column-tags
  [columns]
  (distinct (remove string/blank? (mapcat :tags columns))))

(defn filter-by-tag
  [ch e]
  (put! ch {:action :filter :filter {:tag (-> e .-target .-value)}}))

(defn filter-by-text
  [owner ch e]
  (let [value (-> e .-target .-value)]
    (om/set-state! owner :filter value)
    (put! ch {:action :filter :filter {:text value}})))

(defn- source-view
  [columns owner {:keys [ch] :as opts}]
  (reify
    om/IRenderState
    (render-state [_ state]
      (kioo-component
       [:.p-left]
       {[:.add-all]
        (listen :on-click #(act ch :add-all nil))
        [:.tags]
        (set-attr :style (om-stuff/show (not (empty? (:tags state)))
                                        "inline-block"))
        [:input]
        (do->
         (set-attr :value (:filter state))
         (listen :on-change (partial filter-by-text owner ch)))
        [:select]
        (do->
         (kioo/content
          (kioo/html
           (conj
            (for [tag (:tags state)]
              [:option {:value tag} tag])
            [:option {:value ""} (t :choose-tag)])))
         (listen :on-change (partial filter-by-tag ch)))
        [:ul]
        (kioo/content
         (kioo/html
          (om/build-all source-item columns {:opts opts})))}))))

(defn palette
  [{:keys [columns selected]} owner]
  (reify
    om/IInitState
    (init-state [_]
      {:filter {:tag "" :text ""}})
    om/IWillMount
    (will-mount [_]
      (let [ch (chan)]
        (om/set-state! owner :ch ch)
        (om/set-state! owner :tags (column-tags columns))
        (event-handler ch owner columns selected)))
    om/IRenderState
    (render-state [_ {:keys [ch filter tags]}]
      (kioo-component
       [:.p-palette]
       {[:.p-left]
        (substitute (om/build source-view
                              (unselected columns selected filter)
                              {:opts {:ch ch} :state {:tags tags}}))
        [:.p-right]
        (substitute (om/build selection-view
                              selected
                              {:opts {:ch ch}}))}))))
