(ns om-stuff.query-view
  (:require-macros [cljs.core.async.macros :refer [go-loop]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! <! chan]]
            [om-stuff.core :as om-stuff]
            [i18n.core :refer [t]]))

(defn value [e] (-> e .-target .-value))

(defn kvalue [e] (-> e .-target .-value keyword))

(defmulti query-view (fn [app owner] (-> app :predicate :type keyword)))

(defn find-by-key
  [coll key]
  (first (filter #(= (:key %) key) coll)))

(defn find-op-by-type
  [ops type]
  (filter #(contains? (:for-types %) type) ops))

(defn get-ops
  [ops columns column-key]
  (let [type (:type (find-by-key columns column-key))]
    (find-op-by-type ops type)))

(defn new-predicate
  [index]
  {:type :predicate :index index})

(defn new-group
  [index]
  {:type :predicate-group :op :and
   :index index :items [(new-predicate 0)]})

(defn root-group [] (new-group nil))

(defn set-op-group
  [predicate op]
  (if op
    (om/update! predicate :op (name op))))

(defn set-column [predicate config e]
  (let [new-column (value e)
        {:keys [ops columns]} config
        default-op (:key (first (get-ops ops columns new-column)))]
    (om/transact! predicate
                  (fn [p]
                    (assoc p :column new-column :value "" :op default-op)))))

(defn set-op [predicate e]
  (om/update! predicate :op (value e)))

(defn set-value
  [predicate e]
  (om/update! predicate :value (value e)))

(defn add-group
  [predicate]
  (om/transact! predicate :items #(conj % (new-group (count %)))))

(defn add-predicate
  [predicate]
  (om/transact! predicate :items #(conj % (new-predicate (count %)))))

(defn delete-predicate
  [parent item]
  (om/transact! parent [:items]
                (fn [items]
                  (->> items
                       (remove #(= (:index %) (:index item)))
                       (map-indexed (fn [i v] (assoc v :index i)))
                       vec))))

(defn predicate-view
  [{{:keys [op column value] :as predicate} :predicate
    {:keys [columns ops ch] :as config} :config}
   owner]
  (reify
    om/IRenderState
    (render-state [_ {:keys [parent-ch] :as state}]
      (dom/div
       #js {:className "q-item form-inline"}
       (dom/div
        #js {:className "form-group"}
        (apply
         dom/select
         #js {:value column :className "form-control"
              :onChange (partial set-column predicate config)}
         (dom/option #js {:value "no-column"}
                     (t :no-column-selected))
         (for [{:keys [key text]} columns]
           (dom/option #js {:value (name key)} text))))
       (dom/div
        #js {:className "form-group"}
        (apply
         dom/select
         #js {:value op
              :className "form-control"
              :onChange (partial set-op predicate)}
         (for [{:keys [key text]} (get-ops ops columns column)]
           (dom/option #js {:value (name key)} text))))
       (dom/div
        #js {:className "form-group"}
        (dom/input
         #js {:type "text" :value value :className "input-xs form-control"
              :onChange (partial set-value predicate)}))
       (dom/button
        #js {:type "button"
             :className "btn btn-danger pull-right btn-remove-rule"
             :onClick #(put! parent-ch @predicate)}
        (dom/span
         #js {:className "glyphicon glyphicon-minus-sign"})
        (t :delete))))))

(defn add-or-gate
  [{:keys [op index] :as predicate} owner]
  (om/component
   (let [v (rand-int 99999)
         btn-class (fn [type]
                     (str "btn btn-xs btn-primary group-op "
                          (if (= type op) "active")))]
     (dom/div
      #js {:className "btn-group"}
      (dom/label #js {:className (btn-class "and")}
               (dom/input #js {:name (str "and-or-gate-" v)
                               :type "radio"
                               :value "and"
                               :checked (= (keyword (:op predicate)) :and)
                               :onChange #(set-op-group predicate :and)}
                          (t :and)))
      (dom/label #js {:className (btn-class "or")}
                 (dom/input #js {:name (str "and-or-gate-" v)
                                 :type "radio"
                                 :value "or"
                                 :checked (= (keyword (:op predicate)) :or)
                                 :onChange #(set-op-group predicate :or)}
                            (t :or)))))))

(defn rule-buttons
  [{:keys [index] :as predicate} owner]
  (reify
    om/IRenderState
    (render-state [_ {:keys [parent-ch]}]
      (dom/div
       #js {:className "q-buttongroup btn-group pull-right"}
       (dom/button #js {:className "btn btn-xs btn-success btn-add-rule"
                        :type "button"
                        :onClick #(add-predicate predicate)}
                   (dom/span #js {:className "glyphicon glyphicon-plus-sign"})
                   (t :add-predicate))
       (dom/button #js {:className "btn btn-xs btn-add-group btn-success"
                        :type "button"
                        :onClick #(add-group predicate)}
                   (dom/span #js {:className "glyphicon glyphicon-plus-sign"})
                   (t :add-predicate-group))
       (dom/button #js {:className "btn btn-xs btn-danger btn-remove-group"
                        :style (om-stuff/show (some? index))
                        :type "button"
                        :onClick #(if parent-ch (put! parent-ch @predicate))}
                   (dom/span #js {:className "glyphicon glyphicon-minus-sign"})
                   (t :delete))))))

(defn predicate-group-view
  [{{:keys [op items] :as predicate} :predicate
    config :config} owner]
  (reify
    om/IWillMount
    (will-mount [_]
      (let [ch (chan)]
        (go-loop []
          (let [item (<! ch)]
            (delete-predicate predicate item))
          (recur))
        (om/set-state! owner :ch ch)))
    om/IRenderState
    (render-state [_ {:keys [ch parent-ch] :as state}]
      (dom/dl
       #js {:className "rule-group-container"}
       (dom/dt #js {:className "clearfix rules-group-header"}
               (om/build add-or-gate predicate)
               (om/build rule-buttons predicate {:state state}))
       (dom/dd #js {:className "q-predicate-wrapper"}
               (apply dom/ul #js {:className "rules-list"}
                      (for [item items]
                        (dom/li #js {:className "rule-container"}
                                (om/build query-view {:predicate item
                                                      :config config}
                                          {:state {:parent-ch ch}})))))))))

(defmulti query-view (fn [state _ _] (-> state :predicate :type keyword)))

(defmethod query-view :predicate-group
  [state owner opts]
  (predicate-group-view state owner))

(defmethod query-view :predicate
  [state owner opts]
  (predicate-view state owner))

