(ns om-stuff.test
  (:require-macros [cljs.core.async.macros :refer [go-loop]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan sub pub]]
            [om-stuff.core :as om-stuff]))

(def db (atom []))

(defn init-db! [data]
  (reset! db data))

(defn paginaged
  [db page-no page-size]
  (let [start (* page-no page-size)
        end   (+ start page-size)]
    (cond
      (>= start (count db)) []
      (> end (count db)) (subvec db start)
      :default (subvec db start end))))

(defn mock-ajax-req
  [owner method url {:keys [id on-success data params]}]
  (let [id (if (some? id) (js/parseInt id))
        page-no (if-let [page-no (:page-no params)] page-no 0)
        page-size 2
        more? (< (* page-no (inc page-size)) (count @db))
        result (case method
                 :get (if-not id
                        (paginaged @db page-no page-size)
                        (first (filter #(= (get % :_id) id) @db)))
                 :post (let [_id (inc (count @db))]
                         (swap! db #(conj % (assoc data :_id _id)))
                         (assoc data :_id _id))
                 :put  (do
                         (swap! db (fn [db]
                                     (vec (map #(if (= id (:_id %))
                                                  (merge % data) %) db))))
                         (assoc data :_id id))
                 :delete (swap! db (fn [db]
                                     (remove #(= id (:_id %)) db))))]
    {:status (if (:page-no params) :append :success)
     :data result :more? more?}))

(defn mock-ajax-handler
  [{url :url {:keys [method options]} :arg} app owner]
  (let [{:keys [on-success data redirect-url]} options
        refresh #(om-stuff/refresh-data % app)
        options (if (:page-no @app)
                  (assoc-in options [:params :page-no] (:page-no @app))
                  options)
        res (mock-ajax-req owner method url options)]
    (if on-success
      (on-success res refresh)
      (refresh res))
    (if (and (= (:status res) :success) redirect-url)
      (om-stuff/redirect redirect-url))))

(defn event-handler
  [app owner url]
  (let [ch (sub (om/get-shared owner :notify-ch) [:ajax url] (chan))]
    (go-loop [req (<! ch)]
      (cond
       (= (-> req :action) :ajax) (mock-ajax-handler req app owner)
       :default (om-stuff/action-handler req app owner))
      (recur (<! ch)))))
