(ns om-stuff.core-test
  (:require-macros [cemerick.cljs.test :refer 
                    (is deftest are done testing use-fixtures)]
                   [om-test.core :refer [wait-let wait om-root with-timeout]]
                   [cljs.core.async.macros :refer [go]])
  (:require [cemerick.cljs.test :as t]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [chan put! <!]]            
            [om-stuff.ajax :as ajax]
            [goog.history.EventType :as HistoryEventType]
            [om-stuff.core :as om-stuff]
            [om-test.core :as om-test :refer 
             [components-by-tag simulate-change value-state checked-state]]
            [goog.events :as events])
  (:import [goog History]))

(deftest test-parse-url
  (are [url params result] (= (om-stuff/parse-url url params) result)
       "hello" {} "hello"
       "/secure/admin/:id/view" {:id "abc"} "/secure/admin/abc/view"
       "hello/:a" {} "hello/:a"
       "hello/:a" {:a 20} "hello/20"
       "hello" {:a 20} "hello?a=20"
       "hello/:a/:b" {:a 20 :b 30} "hello/20/30"
       "hello/:a" {:a 20 :b 30} "hello/20?b=30"))

(deftest ^:async test-password-field
  (let [field {:key :password :type :password}
        obj   {}]
    (om-root
     om-stuff/input-component [field obj]
     (fn [state root]
       (let [n (om/get-node root)]
         (is (= (.-name n) "password"))
         (is (= (.-tagName n) "INPUT"))
         (is (= (.-type n) "password"))
         (simulate-change n (value-state "password" "password"))
         (wait (= (-> @state second :password) "password")
               (done)))))))

(deftest ^:async test-text-field
  (let [field {:key :address :type :text}
        obj   {}]
    (om-root
     om-stuff/input-component [field obj]
     (fn [state root]
       (let [n (om/get-node root)]
         (is (= (.-name n) "address"))
         (is (= (.-tagName n) "INPUT"))
         (is (= (.-type n) "text"))
         (simulate-change n (value-state "address" "home"))
         (wait (= (-> @state second :address) "home")
               (done)))))))

(deftest ^:async test-checkbox
  (let [field {:key :acknowledge? :type :checkbox}
        obj   {}]
    (om-root
     om-stuff/input-component [field obj]
     (fn [state root]
       (wait-let [n (first (components-by-tag root "input"))]
                 (simulate-change n (checked-state))
                 (wait (-> @state second :acknowledge?)
                       (done)))))))

(deftest ^:async test-radio
  (let [field {:key :yes-or-no? :type :radio}
        obj   {}]
    (om-root
     om-stuff/input-component [field obj]
     (fn [state root]
       (wait-let [n (first (components-by-tag root "input"))]
                 (simulate-change n (checked-state))
                 (wait (-> @state second :yes-or-no?)
                       (done)))))))

(deftest ^:async test-text-area
  (let [field {:key :address :type :textarea}
        obj   {}]
    (om-root
     om-stuff/input-component [field obj]
     (fn [state root]
       (let [n (om/get-node root)]
         (is (= (.-name n) "address"))
         (is (= (.-tagName n) "TEXTAREA"))
         (simulate-change n (value-state "address" "home"))
         (wait (= (-> @state second :address) "home")
               (done)))))))

(deftest ^:async test-email
  (let [field {:key :primary-email :type :email}
        obj   {}]
    (om-root
     om-stuff/input-component [field obj]
     (fn [state root]
       (let [n (om/get-node root)]
         (is (= (.-name n) "primary-email"))
         (is (= (.-type n) "email"))
         (is (= (.-tagName n) "INPUT"))
         (simulate-change n (value-state "primary-email" "john.doe@example.com"))
         (wait (= (-> @state second :primary-email) "john.doe@example.com")
               (done)))))))

(defn test-form
  [app owner]
  (reify
    om/IRenderState
    (render-state [_ state]
      (om/build om-stuff/form-editor (:editor app)))))

(def form-state
  (atom {:editor
         {:fields [{:key :name :title "Name"}
                   {:key :address :type :textarea :title "Address"}
                   {:key :role :type :select :title "Role" :multiple? true
              :options {"admin" "Administrator" "user" "User"}}
                   {:key :agree :type :checkbox
                    :title "Do you agree to the terms and conditions"}]
          :obj {:name "joe" :address "New York" :agree true
                :role #js ["user"]}}}))

(defn select-value
  [el value]
  (set! (.-value (om/get-node el)) value)) 

(deftest form-fields-are-prepopulated
  (let [root (om-test/create-root test-form form-state)]
    (wait-let
     [select     (first (om-test/by-tag root "select"))
      textarea   (first (om-test/by-tag root "textarea"))
      [input cb] (om-test/by-tag root "input")]
     (is (= (-> input .-props .-value) "joe"))
     (is (= (-> textarea .-props .-value) "New York"))
     (is (= (-> cb .-props .-checked) true)))))
