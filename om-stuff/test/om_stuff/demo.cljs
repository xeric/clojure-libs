(ns om-stuff.demo
  (:require-macros [cljs.core.async.macros :refer [go-loop]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [i18n.core :refer [t]]
            [cljs.core.async :refer [chan put! pub]]
;            [om-stuff.sortable-demo :as sortable-demo]
            [om-stuff.palette-demo]
            [om-stuff.query-view-demo]
            [om-stuff.core :as om-stuff]
            [om-stuff.test]
            [om-stuff.ajax :as ajax]
            [goog.events :as events]
            [secretary.core :as secretary :refer-macros [defroute]])
  (:import [goog History]
           [goog.History EventType]))

(enable-console-print!)

(declare new-link get-link list-link)

(defn link-renderer
  [{:keys [row]} owner]
  (om/component
   (dom/a #js {:href (get-link {:id (:_id row)})} (:name row))))

(defn form-state []
  (atom {:headers [{:key :_id :title "ID"}
                   {:key :name :title "Name" :renderer link-renderer}
                   {:key :address :title "Address"}]
         :editor
         {:fields [{:key :name :title "Name"}
                   {:key :address :title "Address" :type :textarea}
                   {:key :agree :type :checkbox
                    :title "Do you agree to these terms and conditions?"}
                   {:key :color :type :select :multiple? false
                    :options {"red" "Red" "blue" "Blue"} :title "Favourite color?"}
                   {:key :roles :title "Roles" :type :select :multiple? true
                    :options {:admin "Administrator" :user "User"}}]}}))

(def mock-db [{:_id 1 :name "John Doe" :address "New York"}
              {:_id 2 :name "Jane Lane" :address "Washington"}
              {:_id 3 :name "Mike" :address "Kansas"}
              {:_id 4 :name "Mike2" :address "Kansas"}
              {:_id 5 :name "Mike3" :address "Kansas"}
              {:_id 6 :name "Mike4" :address "Kansas"}
              {:_id 7 :name "Mike5" :address "Kansas"}])

(defn form
  [{:keys [obj] :as editor} owner]
  (om/component
   (dom/div
    nil
    (om/build om-stuff/form-editor editor)
    (dom/button #js {:className "btn btn-sm btn-primary"
                     :type "button"
                     :onClick #(om-stuff/resource-save owner :url editor)}
                "Save")
    (dom/a #js {:className "btn btn-sm btn-default"
                :href (list-link)} "Back"))))

(defn crud-component
  [app owner]
  (reify
    om/IWillMount
    (will-mount [_]
      (om-stuff.test/event-handler app owner :url))
    om/IRenderState
    (render-state [_ state]
      (dom/div nil
               (dom/a #js {:href (new-link)
                          :className "btn btn-sm btn-primary"} "New")
               (if (-> app :editor :obj)
                (om/build form (:editor app))
                (om/build om-stuff/grid app))
               (dom/a #js {:href "#"
                           :style (om-stuff/show (:more? app))
                           :onClick
                           (fn [e]
                             (.preventDefault e)
                             (om/transact! app [:page-no] (fnil inc 0))
                             (om-stuff/resource-get (om/get-shared owner :pub-ch) :url))
                           :className "btn btn-large btn-success"}
                           "More")))))

(when-let [app (.. js/document (getElementById "app"))]
  (let [app-state (form-state)
        pub-ch (chan)
        notify-ch (pub pub-ch om-stuff/resource-topic)]
    (om-stuff.test/init-db! mock-db)
    (om-stuff/setup-history!)
    
    (defroute new-link "/new" []
      (om-stuff/resource-new pub-ch :url {}))

    (defroute get-link "/:id" [id]
      (om-stuff/resource-get pub-ch :url :id id))

    (defroute list-link "/" []
      (om-stuff/resource-get pub-ch :url))

    (om/root crud-component app-state
             {:target app
              :shared {:pub-ch pub-ch :url "http://example.com"
                       :notify-ch notify-ch}})

    (om-stuff/enable-history)))

