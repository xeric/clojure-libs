(ns om-stuff.kioo-test
  (:require-macros [cemerick.cljs.test :refer 
                    (is deftest are done testing use-fixtures)])
  (:require [cemerick.cljs.test :as t]
            [om-stuff.kioo :include-macros true]
            [om-stuff.core :as om-stuff]
            [om-test.core :as om-test :include-macros true]
            [goog.dom :as gdom]
            [kioo.om :as kioo]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(defn demo-component
  [app owner]
  (om/component
   (om-stuff.kioo/base-page
    (om-stuff/ajax? app)
    (om-stuff.kioo/kioo-component
     [:#demo]
     {[:.name] (kioo/content (:name app))
      [:.counter]
      (kioo/do->
       (kioo/listen :on-click #(om/transact! app [:counter] inc))
       (kioo/content (str (:counter app))))
      [:.address]
      (kioo/content (:address app))}))))

(deftest ^:async demo-test
  (let [state (atom {:name "john.smith"
                     :counter 1
                     :address "New York"})
        root (om-test/create-root demo-component state)]
    (om-test/wait-let
     [counter (om-test/component-by-class root "counter")]
     (om-test/simulate-click (om/get-node counter))
     (om-test/wait
      (= (:counter @state) 2)
      (om-test/re-render! root)
      (is (= (gdom/getTextContent (om/get-node counter)) "2"))
      (done)))))

(defn simple-grid
  [app owner]
  (om/component
   (om-stuff.kioo/kioo-component
    [:#simple-grid]
    {[:table] (kioo/substitute (om/build om-stuff/grid app))})))

(defn simple-grid-component
  [app owner]
  (reify
    om/IRenderState
    (render-state [_ {:keys [selection]}]
      (om-stuff.kioo/base-page
       (om-stuff/ajax? app)
       (om-stuff.kioo/kioo-component
        [:#demo]
        {[:.data-select]
         (kioo/do->
          (kioo/set-attr :value selection)
          (kioo/listen :on-change
                       #(om/set-state! owner :selection (-> % .-target .value)))
          (kioo/content (kioo/html
                         (for [i ["A" "B" "C" "D"]]
                           [:option {:value i} i]))))
          [:#simple-grid]
          (kioo/substitute (om/build simple-grid app))})))))

(deftest ^:async grid-test
  (let [state (atom {:headers [{:key :address :title "Address"}
                               {:key :name :title "Name"}
                               {:key :created-on :title "Created On"}]})
        root (om-test/create-root simple-grid-component state)]
    (om-test/wait-let
     [tbody (om-test/by-tag root "tbody")]
     (is (zero? (count (om-test/by-tag tbody "tr"))))
     (swap! state assoc-in
            [:data]
            [{:name "John" :address "New York" :created-on "01/01/2014"}
             {:name "Mary" :address "L.A" :created-on "04/04/2013"}])
     (om-test/wait (= (count (om-test/by-tag root "tr")) 3)
                   (reset! state {:data nil})
                   (om-test/wait
                    (= (count (om-test/by-tag root "option" true)) 4)
                    (done))))))
