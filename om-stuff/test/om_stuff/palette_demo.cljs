(ns om-stuff.palette-demo
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [i18n.core :as i18n]
            [om-stuff.palette :refer [palette]]))

(enable-console-print!)

(i18n/add! {:add-all "Add All"
            :p-selected "Selected Values"
            :p-values "Values"
            :choose-tag "Choose Tag"
            :remove-all "Remove All"})

(i18n/init!)

(def palette-el (.. js/document (getElementById "palette-demo")))

(def app-state
  (atom {:columns [{:text "orange" :key :orange :tags ["color" "fruit"]}
                   {:text "apple" :key :apple :tags ["fruit"]}
                   {:text "blue" :key :blue :tags ["color" ""]}]
         :selected []}))

(defn palette-demo
  [app owner]
  (reify
    om/IRender
    (render [_]
      (om/build palette {:columns
                          (om/get-shared owner :columns)
                          :selected (:selected app)}))))

(when palette-el
  (om/root palette-demo
           app-state
           {:target palette-el
            :shared {:columns (:columns @app-state)}}))
