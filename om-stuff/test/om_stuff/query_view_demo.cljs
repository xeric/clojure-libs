(ns om-stuff.query-view-demo
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [i18n.core :as i18n]
            [om-stuff.query-view-test :refer [query-component]]
            [om-stuff.query-view :as qv :refer [query-view]]))

(enable-console-print!)

(i18n/add! {:add-predicate "Add Rule"
            :add-predicate-group "Add Group"
            :delete "Delete"
            :no-column-selected "-------"
            :and "And"
            :or "Or"})

(i18n/init!)

(def el (.. js/document (getElementById "query-view")))

(def query-config
  {:columns [{:key "name" :text "Name" :type :string}
             {:key "age" :text "Age" :type :int}
             {:key "address" :text "Address" :type :string}
             {:key "weight" :text "Weight" :type :double}]
   :ops [{:key :lt :text "<" :for-types #{:int :double}}
         {:key :le :text "<=" :for-types #{:int :double}}
         {:key :gt :text ">" :for-types #{:int :double}}
         {:key :ge :text ">=" :for-types #{:int :double}}
         {:key :eq :text "=" :for-types #{:int :string :double}}
         {:key :ne :text "!=" :for-types #{:int :double :string}}]})

(def app-state (atom (qv/root-group)))

(when el
  (om/root (fn [app owner opts]
             (om/component
              (dom/div nil
                       #_(om/build query-component app)
                       (prn app)
                       (om/build query-view
                                 {:predicate app
                                  :config query-config})
                       (dom/div nil (pr-str app)))))
           app-state
           {:target el
            :opts query-config}))
