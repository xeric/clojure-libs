(ns om-stuff.query-view-test
  (:require-macros [cemerick.cljs.test :refer 
                    (is deftest are done testing use-fixtures)]
                   [om-test.core :refer [wait-let wait om-root]])
  (:require [cemerick.cljs.test :as t]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [chan put! <!]]
            [om-stuff.query-view :refer [query-view root-group]]
            [om-test.core :as om-test :refer 
             [components-by-tag simulate-change value-state
              simulate-click checked-state visible? enabled? re-render!
              render render-to-str react-utils components-by-class]]))

(defn by-class
  [p class]
  (first (components-by-class p class)))

(def query-config
  {:columns [{:key "name" :text "Name" :type :string}
             {:key "age" :text "Age" :type :int}
             {:key "address" :text "Address" :type :string}
             {:key "weight" :text "Weight" :type :double}]
   :ops [{:key :lt :text "<" :for-types #{:int :double}}
         {:key :le :text "<=" :for-types #{:int :double}}
         {:key :gt :text ">" :for-types #{:int :double}}
         {:key :ge :text ">=" :for-types #{:int :double}}
         {:key :eq :text "=" :for-types #{:int :string :double}}
         {:key :ne :text "!=" :for-types #{:int :double :string}}]})

(defn query-component
  [app-state owner opts]
  (reify
    om/IRenderState
    (render-state [_ state]
      (om/build query-view {:predicate app-state
                            :config query-config}))))

(deftest ^:async group-ops-change-state
  (om-root
   query-component (atom (root-group))
   (fn [state root]
     (wait-let
      [or-op (second (components-by-class root "group-op"))]
      (simulate-change (first (components-by-tag or-op "input")))
      (wait (= (keyword (:op @state)) :or)
            (done))))))

(deftest ^:async add-group-adds-new-group
  (let [state (atom (root-group))
        root  (om-test/create-root query-component state)]
    (wait-let
     [add-g     (first (om-test/by-class root "btn-add-group" true))
      remove-g  (first (om-test/by-class root "btn-remove-group" true))
      remove-rs (om-test/by-class root "btn-remove-rule" true)]
      (is (= @state {:type :predicate-group
                     :op :and :trigger true
                     :index nil
                     :items [{:type :predicate :index 0}]}))
      (is (not (om-test/visible? remove-g)))
      (is (= (count remove-rs) 1))
      (simulate-click (om/get-node add-g))
      (wait (= @state
               {:type :predicate-group
                :op :and :trigger true
                :index nil
                :items [{:type :predicate :index 0}
                        {:type :predicate-group
                         :op :and :index 1
                         :items [{:type :predicate :index 0}]}]})
            (re-render! root)
            (is (= (count (om-test/by-class root "btn-remove-rule")) 2))
            (let [remove-g (second (om-test/by-class root "btn-remove-group" true))]
              (is (visible? remove-g))
              (simulate-click remove-g)
              (wait (= @state {:type :predicate-group
                               :op :and :trigger true
                               :index nil
                               :items [{:type :predicate :index 0}]})
                    (is (= (count (om-test/by-class root "btn-remove-group" true)) 1))
                    (done)))))))
      
(deftest ^:async add-rule-adds-new-rule
  (om-root
   query-component (atom (root-group))
   (fn [state root]
     (wait-let
      [add-r (first (om-test/by-class root "btn-add-rule"))
       remove-r (first (om-test/by-class root "btn-remove-rule"))]
      (is (= (count (om-test/by-class root "btn-remove-rule" true)) 1))
      (simulate-click (om/get-node add-r))
      (wait (= @state {:type :predicate-group
                       :op :and
                       :index nil
                       :items [{:type :predicate :index 0}
                               {:type :predicate :index 1}]})
            (re-render! root)
            (wait-let [remove-r (second (om-test/by-class root "btn-remove-rule" true))]
              (simulate-click (om/get-node remove-r))
              (wait (= @state {:type :predicate-group
                               :op :and
                               :index nil
                               :items [{:type :predicate :index 0}]})
                    (re-render! root)
                    (wait (= (count (om-test/by-class root "btn-remove-rule" true)) 1))
                    (done))))))))

