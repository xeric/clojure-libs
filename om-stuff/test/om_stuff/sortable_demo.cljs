(ns om-stuff.sortable-demo
  (:require-macros [cljs.core.async.macros :refer [go-loop]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [om-stuff.core :as om-stuff]
            [cljs.core.async :refer [chan <! put!]]
            [om-stuff.sortable :as sortable]))

(enable-console-print!)

(def sortable-el (.. js/document (getElementById "sortable-demo")))

(defn change [a]
  (vec (take 5 (repeatedly (partial rand-int 10)))))

(defn editor
  [app owner {:keys [ch] :as opts}]
  (reify
    om/IWillMount
    (will-mount [_]
      (om/set-state! owner :text "")
      (om/set-state! owner :type "String"))
    om/IRenderState
    (render-state [_ {:keys [text type]}]
      (dom/div
       #js {:className "form-inline"}
       (dom/div
        #js {:className "form-group"}
        (dom/label #js {:className "sr-only"} "Title")
        (dom/input #js {:type "text"
                        :value text
                        :onChange
                        #(om/set-state! owner :text (-> % .-target .-value))
                        :className "form-control"
                        :placeholder "Enter Title"}))
       (dom/div #js {:className "form-group"}
                (dom/label #js {:className "sr-only"} "Type")
                (dom/select #js {:className "form-control"
                                 :value type
                                 :onChange
                                 #(om/set-state! owner :type
                                                 (-> % .-target .-value))}
                            (dom/option #js {:value "String"} "String")
                            (dom/option #js {:value "Integer"} "Integer")
                            (dom/option #js {:value "Double"} "Double")
                            (dom/option #js {:value "Date"} "Date")))
       (dom/button #js {:className "btn btn-sm btn-default"
                        :onClick
                        (fn [e]
                          (let [{:keys [type text]}
                                (om/get-state owner)]
                            (om/transact! app #(conj % {:text text :type type}))
                            (om/set-state! owner :text "")
                            (om/set-state! owner :type "String")))}
                        (dom/span #js {:className "glyphicon glyphicon-plus"}))))))

(defn inline-form
  [row-state owner {:keys [ch] :as opts}]
  (om/component
   (dom/div #js {:className "form-inline"
                 :data-id (:_index row-state)}
            (dom/div #js {:className "form-group"}
                 (dom/label #js {:className "sr-only"} "Title")
                 (dom/input #js {:type "text"
                                 :value (:text row-state)
                                 :onChange #(om/update! row-state [:text]
                                                        (-> % .-target .-value))
                                 :className "form-control"
                                 :placeholder "Enter Title"}))
            (dom/div #js {:className "form-group"}
                 (dom/label #js {:className "sr-only"} "Type")
                 (dom/select #js {:className "form-control"
                                  :onChange #(om/update! row-state [:type]
                                                         (-> % .-target .-value))
                                  :value (:type row-state)}
                             (dom/option #js {:value "String"} "String")
                             (dom/option #js {:value "Integer"} "Integer")
                             (dom/option #js {:value "Double"} "Double")
                             (dom/option #js {:value "Date"} "Date")))
            (dom/button #js {:className "btn btn-sm btn-danger"
                             :onClick (fn [e]
                                        (put! ch {:id :delete
                                                  :event @row-state}))}
                        (dom/span #js {:className "glyphicon glyphicon-trash"})))))

(defn sort-items
  [app sortable]
  (om/transact! app
                (fn [app]
                  (vec (map #(assoc %1 :sort-index (js/parseInt %2))
                            app (.toArray sortable))))))

(defn delete-item
  [app event]
  (om/transact! app
                (fn [app]
                  (vec (remove #(= (:text %) (:text event)) app)))))

(defn sortable-demo
  [app owner opts]
  (reify
    om/IWillMount
    (will-mount [_]
      (let [ch (chan)]
        (om/set-state! owner :ch ch)
        (go-loop []
          (when-let [{:keys [id event item sortable]} (<! ch)]
            (case id
              :update (sort-items app sortable)
              :delete (delete-item app event)
              nil)
            (om/refresh! owner))
          (recur))))
    om/IRenderState
    (render-state [_ {:keys [ch]}]
      (dom/div
       #js {:className "row"}
       (dom/div #js {:className "col-md-2"})
       (dom/div
        #js {:className "col-md-10"}
        (om/build sortable/sortable app
                  {:opts {:child inline-form
                          :ch ch
                          :parent dom/div}})
        (om/build editor app)
        (dom/button #js {:className "btn btn-sm btn-success"
                         :onClick (fn [e] (om/transact! app change))}
                    "Change")
        (dom/div nil
                 (pr-str app)))))))

(def app-state (atom [{:text "hello" :type "String"}
                      {:text "goodbye" :type "Integer"}
                      {:text "when" :type "Date"}
                      {:text "weight" :type "Double"}]))

(if sortable-el
  (om/root sortable-demo
           app-state
           {:target sortable-el}))
