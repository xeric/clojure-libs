(ns om-stuff.sortable-test
  (:require-macros [cemerick.cljs.test :refer 
                    (is deftest are done testing use-fixtures)]
                   [om-test.core :refer [wait-let wait om-root]])
  (:require [cemerick.cljs.test :as t]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [chan put! <!]]
            [om-stuff.sortable :as sortable]
            [om-test.core :refer 
             [components-by-tag simulate-change value-state checked-state
              render render-to-str react-utils]]))

(defn sortable-demo1
  [app owner opts]
  (om/component
   (om/build sortable/sortable app {:opts opts})))

(defn span-item
  [column owner]
  (om/component
   (dom/span nil (:text column))))

(deftest test-list-is-rendered
  (let [state [{:text "a"} {:text "b"} {:text "c"}]
        cstr (render-to-str (om/build sortable-demo1 state))]
    (is (= cstr "<ul class=\"s-sortable\"><li>a</li><li>b</li><li>c</li></ul>"))))

(deftest test-list-parent-child-can-be-customized
  (let [state [{:text "a"} {:text "b"} {:text "c"}]
        cstr (render-to-str (om/build sortable-demo1 state
                                      {:opts {:parent dom/div
                                              :child span-item}}))]
    (is (= cstr "<div class=\"s-sortable\"><span>a</span><span>b</span><span>c</span></div>"))))

