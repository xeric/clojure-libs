(ns om-stuff.test-ajax
  (:require-macros [cemerick.cljs.test :refer (is deftest are done testing)])
  (:require [cemerick.cljs.test :as t]
            [om-stuff.ajax :as ajax]
            [goog.events :as events])
  (:import goog.net.EventType))

(defn set-text!
  [xhr text]
  (set! (.-xhr_ xhr) #js {})
  (set! (-> xhr .-xhr_ .-responseText) text))

(deftest ^:async test-on-success
  (testing "If xhr request returns success, on-success event is called"
    (with-redefs [ajax/send! 
                  (fn [{:keys [xhr]}]
                    (set-text! xhr (pr-str {:status :success}))
                    (.dispatchEvent xhr goog.net.EventType.SUCCESS))]
      (doto (ajax/get "http://www.google.com")
        (ajax/on-success (fn [e] (done)))
        (ajax/send!)))))

(deftest ^:async test-on-error
  (testing "If xhr request returns error, on-error event is called"
    (with-redefs [ajax/send! 
                  (fn [{:keys [xhr]}]
                    (set-text! xhr "error")
                    (.dispatchEvent xhr goog.net.EventType.ERROR))]
      (doto (ajax/get "http://www.google.com")
        (ajax/on-error (fn [e] (done)))
        (ajax/send!)))))

(deftest ^:async test-on-complete
  (testing "If xhr request dispatches COMPLETE event, on-complete event is called"
    (with-redefs [ajax/send! 
                  (fn [{:keys [xhr]}]
                    (set-text! xhr "complete")
                    (.dispatchEvent xhr goog.net.EventType.COMPLETE))]
      (doto (ajax/get "http://www.google.com")
        (ajax/on-complete (fn [e] (done)))
        (ajax/send!)))))
