(ns om-stuff.upload
  (:require-macros [cljs.core.async.macros :refer [go-loop]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [i18n.core :refer [t]]
            [om-stuff.ajax :as ajax]
            [cljs.core.async :refer [put! chan <!]]))

(defn on-file-upload
  [owner data out-chan]
  (let [result (.-result data)
        result (if result (cljs.reader/read-string result))]
    (put! out-chan (or result {}))
    (om/set-state! owner :progress 0)))

(defn on-progress
  [owner data]
  (let [progress (int (* (/ (.-loaded data) (.-total data)) 100))]
    (om/set-state! owner :progress progress)))

(defn progress-bar [_ owner]
  (reify
    om/IRenderState
    (render-state [_ {:keys [progress]}]
      (dom/div #js {:className "progress m-t-xs full process-striped"
                    :style (om-stuff/hide (= progress 0))}
               (dom/div #js {:className "progress-bar progress-bar-warning"
                             :style #js {:width
                                         (str progress "%")}}
                        (dom/span #js {:className "sr-only"}
                                  (str progress (t :percent-complete progress))))))))

(defn init-fileupload
  [el ch url & {:keys [type] :or {type :post}}]
  (.fileupload
   el #js {:send   (fn [e data] (ajax/start-ajax-wait!))
           :always (fn [e data] (ajax/stop-ajax-wait!))
           :done   (fn [e data] (put! ch {:data data :event :done}))
           :fail   (fn [e data] (put! ch {:data
                                         {:error (aget (aget data "jqXHR")
                                                       "responseText")}
                                         :event :fail}))
           :type   (name type)
           :formAcceptCharset "utf-8"
           :progressall (fn [e data] (put! ch {:data data :event :progress}))
           :url url}))

(defn uploader
  [_ owner {:keys [label upload-url multiple? type field-name]}]
  (reify
    om/IWillMount
    (will-mount [_]
      (let [inner-ch (chan)
            out-ch   (om/get-state owner :ch)]
        (om/set-state! owner :inner-ch inner-ch)
        (om/set-state! owner :progress 0)
        (go-loop [{:keys [event data]} (<! inner-ch)]
          (case event
            :progress (on-progress owner data)
            :fail     (do (put! out-ch data)
                          (om/set-state! owner :progress 0))
            :done     (on-file-upload owner data out-ch))
          (recur (<! inner-ch)))))
    om/IDidMount
    (did-mount [_]
     (let [n (js/jQuery (om/get-node owner "upload"))
           c (om/get-state owner :inner-ch)]
     (init-fileupload n c upload-url :type type)))
    om/IRenderState
    (render-state [_ {:keys [progress] :as state}]
      (dom/div
       #js {:className "upload-wrapper"}
       (om/build progress-bar {} {:state state})
       (dom/div #js {:className "file-upload btn btn-xs btn-info"}
                (dom/i #js {:className "fa fa-upload"})
                (dom/input #js
                           {:className "upload" :type "file" :ref "upload"
                            :name (or field-name "upload")
                            :value "" :multiple multiple?})
                (t (or label :upload)))))))
