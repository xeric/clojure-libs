(defproject xeric/om-test "0.2.2-SNAPSHOT"
  :description "Test utilities for om (ClojureScript)"
  :dependencies 
  [[org.clojure/clojurescript "_"]
   [org.omcljs/om "_" :exclusions [cljsjs/react]]
   [cljsjs/react-with-addons "0.13.3-0"]
   [org.clojure/core.async "_"]
   [com.cemerick/clojurescript.test "_" :scope "test"]]

  :plugins 
  [[lein-modules "0.3.9"]]
  
  :cljsbuild 
  {:builds [{:id "test"
             :source-paths ["src" "test"]
             :notify-command
             ["phantomjs" :cljs.test/runner
              "--web-security=false"
              "this.literal_js_was_evaluated=true"
              "--local-to-remote-url-access=true"
              "test-resources/js/polyfill.js"
              "test-resources/js/react-with-addons.js"
              "target/cljs/testable.js"]
             :compiler {
                        :output-dir "target/cljs/out"
                        :output-to "target/cljs/testable.js"
                        :optimizations :whitespace
                        :pretty-print false}}]})

