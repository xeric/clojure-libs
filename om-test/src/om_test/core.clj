(ns om-test.core
  (:require [cemerick.cljs.test :as t :refer [done]]
            [clojure.walk :as walk]))

(defmacro test-app-state
  "Asynchronously test application state using function f. If f returns true, 
  clojurescript.test's done is called."
  [f]
  `(let [done?# (atom false)]
     (fn [state#]
       (when (and (not @done?#) 
                (~f state#))
         (reset! done?# true)
         (done)))))

(def short-delay 200)

(defmacro delayed
  "Introduces a delay. If the first argument is a number then it becomes
   the delay period otherwise *short-delay* period is used as delay" 
  [& forms]
  (let [[period & forms]
        (if (number? (first forms))
          forms
          (cons short-delay forms))]
  `(~'js/setTimeout (fn [] ~@forms) ~period)))

(defmacro wait*
  "Waits till the given condition is met. Wait can be customized
   with options parameter. options include
     frequency frequency after which the condition is checked
     timeout time period upto which the condition is checked."
  ([lets form] `(wait* {} ~lets ~form))
  ([{:keys [frequency timeout]} lets form]
     (assert (and (vector? lets) (= (count lets) 2)))
     (let [condition (second lets)]
       `(~'om-test.core/wait-condition
         :condition-met? (fn [] (if-let ~lets (do ~form true) false))
         :frequency ~frequency
         :timeout ~timeout
         :test-ctx ~'-test-ctx
         :error-fn (partial ~'om-test.core/report '~condition)))))

(defmacro wait-let
  [& forms]
  (let [[options lets & forms] 
        (if (map? (first forms)) forms (conj forms {}))
        {:keys [frequency timeout]} options
        form (list* 'do forms)]
    (assert (and (vector? lets)
                 (>= (count lets) 2)
                 (zero? (mod (count lets) 2))))
    (let [c (vec (take 2 lets))
          r (vec (drop 2 lets))]
      (if-not (empty? r)
        `(wait* ~options ~c 
                (wait-let ~options ~r ~@forms))
        `(wait* ~options ~c ~form)))))

(defmacro wait
  [& forms]
  (let [[options condition & forms]
        (if (map? (first forms)) forms (conj forms {}))
        {:keys [frequency timeout]} options
        form (list* 'do forms)]
    (assert (not (nil? condition)))
    `(wait* ~options [~'_ ~condition] ~form)))

(defn with-wait-config
  [options & forms]
  `(binding [~'om-test.core/*wait-config* ~options]
     ~@forms))

(defmacro timeout
  []
  `(~'js/setTimeout
    (fn []
      (let [{test-name# :test-name 
             test-env#  :test-env} ~'-test-ctx
             running# (:cemerick.cljs.test/running @test-env#)]
        (when-not (and running# (not (contains? running# test-name#)))
          (t/do-report ~'-test-ctx
                       {:type :fail 
                        :message (str test-name# 
                                      " timed out after " 
                                      ~'om-test.core/*delay* " ms")
                        :expected "Async tests should complete" 
                        :actual "Test didn't complete ('done' was not called)"})
          (done))))
    ~'om-test.core/*delay*))

(defmacro with-timeout
  [delay & args]
  `(binding [~'om-test.core/*delay* ~delay]
     ~@args
     (timeout)))

(defmacro with-default-timeout
  [& args]
  `(do ~@args (timeout)))
 
(defmacro om-root
  ([component state f]
     `(om-root ~component ~state ~f {}))
  ([component state f config]
     `(~'om-test.core/om-root* ~component ~state 
                               (fn [state# owner#]
                                 (~f state# owner#))
                               ~config)))

(defn wait-fold
  [forms]
  (loop [l forms result '()]
    (if (empty? l)
      result
      (let [first-el (first l)]
        (if (and (coll? first-el) (= (first first-el) 'wait))
          (concat result [(concat (first l) (wait-fold (rest l)))])
          (recur (rest l) (concat result [(first l)])))))))

(defn add-done-to-inner-most-wait
  [forms]
  (let [inner-wait? (atom false)]
    (clojure.walk/postwalk (fn [x]
                             (if(and (not @inner-wait?)
                                     (coll? x)
                                     (= (first x) 'wait))
                               (do
                                 (reset! inner-wait? true)
                                 (concat x (list (list `done))))
                               x)) forms)))

(defn wait-fold-walk
  [forms]
  (clojure.walk/prewalk (fn [x]
                          (if (seq? x)
                            (wait-fold x)
                            x))
                        forms))

(defmacro test-wrapper
  [& forms]
  (cons 'do (-> (wait-fold-walk forms)
                add-done-to-inner-most-wait)))

(defmacro deftestw
  [name & forms]
  `(t/deftest ~name
     (test-wrapper ~@forms)))
