(ns om-test.core
  (:require-macros [cemerick.cljs.test :refer 
                    (is deftest are done with-test-ctx)])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [goog.style :as gstyle]
            [goog.dom :as gdom]
            [clojure.string :as string]
            [cemerick.cljs.test :as t]))

(def ^:dynamic *delay* 2000)

(def normal-frequency 200)

(def normal-timeout 2000)

(defn default-modes [v] {:normal v :fast (/ v 2) :slow (* v 2)})

(def ^:dynamic *wait-config* 
  {:frequencies (default-modes normal-frequency)
   :timeouts    (default-modes normal-timeout)})

(def react-utils (-> js/React .-addons .-TestUtils))

(extend-protocol om/IRenderQueue
  nil
  (-get-queue [this])
  (-queue-render! [this c])
  (-empty-queue! [this]))

(defn unmount-root 
  ([] (unmount-root #()))
  ([test-fn]
     (try
       (test-fn)
       (.unmountComponentAtNode js/React 
                                (.-body js/document))
       (catch js/Error e (prn e)))))

(defn on-mount [root f]
  (if (.isMounted root)
    (f)
    (js/setTimeout #(on-mount root f) 100)))

(defn to-cursor [val]
  (om/MapCursor. val (atom {}) []))

(defn checked-state
  ([] (checked-state true))
  ([state]
     #js {:target #js {:checked state}}))

(defn value-state
  [name value]
  #js {:target #js {:name name :value value}})

(defn render-to-str
  "Render a react component to static markup"
  [c]
  (js/React.renderToStaticMarkup c))

(defn render 
  "Render an om component to static markup"
  ([component args] (render component args {}))
  ([component args opts]
     (render-to-str (om/build component (to-cursor args) opts))))

(defn render-into-document 
  "Render an om component into a document"
  ([component args] (render-into-document component args {}))
  ([component args opts]
     (.renderIntoDocument react-utils
                          (om/build component (to-cursor args) opts))))

(declare el hidden?)

(defn by-tag
  "Get components by tag name"
  ([parent tag-name]
   (by-tag parent tag-name false))
  ([parent tag-name include-hidden?]
   (let [components
         (.scryRenderedDOMComponentsWithTag react-utils parent tag-name)]
     (if include-hidden?
       components
       (remove hidden? components)))))

(def components-by-tag by-tag)

(defn by-class
  "Get components by class name"
  ([parent css-class]
   (by-class parent css-class false))
  ([parent css-class include-hidden?]
   (let [components
         (.scryRenderedDOMComponentsWithClass react-utils parent css-class)]
     (if include-hidden?
       components
       (remove hidden? components)))))

(def components-by-class by-class)

(defn first-by-tag
  "Get component by tag name" 
  [parent tag-name]
  (.findRenderedDOMComponentWithTag react-utils parent tag-name))

(defn first-by-class 
  "Get component by css class"
  [parent css-class]
  (.findRenderedDOMComponentWithClass react-utils parent css-class))

(defn simulate-change
  "Simulate a change event"
  ([c]
     (simulate-change c nil))
  ([c opts]
     (-> react-utils .-Simulate (.change c opts))))

(defn simulate-click
  "Simulate a click event"
  [c]
  (-> react-utils .-Simulate (.click c)))

(defn re-render!
  [owner]
  (om/render-all))

(defn wait-frequency
  [frequency]
  (if (number? frequency)
    frequency
    (get (:frequencies *wait-config*) frequency normal-frequency)))

(defn wait-timeout
  [timeout]
  (if (number? timeout)
    timeout
    (get (:timeouts *wait-config*) timeout normal-timeout)))

(defn- report 
  [condition test-ctx message]
  (with-test-ctx test-ctx
    (t/do-report 
     test-ctx
     {:type :fail
      :message (str "Expression " condition " failed with message: " message)
      :expected condition
      :actual false})
    (done)))

(defn wait-condition
  [& {:keys [condition-met? frequency timeout test-ctx error-fn]
      :as args}]
  (let [frequency (wait-frequency frequency)
        timeout  (wait-timeout timeout)]
    ((fn timeout? [time-left]
       (if-not (try (condition-met?)
                    (catch js/Error e
                      (error-fn test-ctx e)))                        
         (if (pos? (- time-left frequency))
           (js/setTimeout
            (partial timeout? (- time-left frequency))
            frequency)
           (error-fn test-ctx "time out")))) timeout)))

(defn root-component
  [app-state owner {:keys [f child]}]
  (reify
    om/IDidMount
    (did-mount [_]
      (f app-state owner))
    om/IRenderState
    (render-state [_ state]
      (om/build child app-state {:init-state state}))))

(defn om-root*
  ([component state f] (om-root* component state f {}))
  ([component state f config]
     (let [el (.createElement js/document "div")]
       (-> js/document .-body (.appendChild el))
       (let [config (assoc config
                      :target el 
                      :opts {:child component :f f})]
         (om/root root-component state config)))))

(defn create-root
  ([component state] (create-root component state {}))
  ([component state config]
   (let [el (.. js/document (createElement "div"))
         _  (-> js/document .-body (.appendChild el))
         root (om/root component state (assoc config :target el))]
     root)))

(defn has-class? [el class]
  (contains? (set (string/split (-> el .-props .-className) #"\s+")) class))

(defn style [el]
  (if el
    (if-let [style (aget el "style")]
      style 
      (.-style (om/get-node el)))
    #js {}))

(defn el [e]
  (if (some? (.-nodeName e)) e (om/get-node e)))

(def element-or-node el)

(defn visible?
  [e]
  (let [e (el e)]
    (and (some? (.-offsetParent e))
         (not= (gstyle/getStyle e "display") "none"))))

(defn hidden? [el]
  (not (visible? el)))

(defn enabled? [el]
  (not (.-disabled (element-or-node el))))

(defn text [e] (.-innerText (el e)))

(defn html [e] (.-innerHTML (el e)))

