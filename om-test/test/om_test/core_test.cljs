(ns om-test.core-test
  (:require [om.core :as om :include-macros true]
            [om-test.core :as om-test :include-macros true
             :refer-macros [wait deftestw wait-let]
             :refer [simulate-change simulate-click]]
            [om.dom :as dom :include-macros true]
            [cemerick.cljs.test :as t :refer-macros [is deftest are done]]))

(defn hello-component
  [_ _]
  (om/component (dom/div #js {:className "text"} "hello")))

(defn message-component
  [state _]
  (om/component (dom/div #js {:className "message"}
                         (str "hello " (:name state)))))

(defn form-component
  [state _]
  (om/component
   (dom/div 
    nil
    (dom/input #js {:type "checkbox"
                    :className "checkbox"
                    :checked (:checked state)
                    :onChange #(om/update! state 
                                           :checked 
                                           (-> % .-target .-checked))})
    (dom/input #js {:type "text"
                    :className "name"
                    :value (:name state)
                    :onChange #(om/update! state 
                                           :name 
                                           (-> % .-target .-value))})
    (dom/button #js {:onClick #(om/update! state :clicked true)
                     :className "button"}))))

(defn large-component
  [state _]
  (om/component
   (apply
    dom/ul
    nil
    (dom/button #js {:onClick
                     (fn [_]
                       (js/setTimeout
                        (fn []
                          (om/update! state :clicked true)) 1000))}
                "Click")                                
    (for [i (range 10000)]
      (dom/li nil (str i))))))

(deftest test-render-to-str
  (is (= (om-test/render-to-str 
          (om/build hello-component nil)) 
         "<div class=\"text\">hello</div>")))

(deftest test-render
  (is (= (om-test/render message-component {:name "john"})
         "<div class=\"message\">hello john</div>")))

(deftest test-by-tag
  (let [state (atom {:name "foo"})
        root  (om-test/create-root message-component state)]
    (is (= (count (om-test/by-tag root "div")) 1))
    (is (= (-> (om-test/by-tag root "div")
               first om/get-node .-innerHTML) "hello foo"))))
      
(deftest test-by-class
  (let [state (atom {:name "foo"})
        root (om-test/create-root message-component state)]
    (is (= (count (om-test/by-class root "message")) 1))
    (is (= (-> (om-test/by-class root "message")
               first om/get-node .-innerHTML)
           "hello foo"))))

(deftestw ^:async change-is-simulated-for-checkbox
  (let [state (atom {:checked false})
        root  (om-test/create-root form-component state)]
    (let [checkbox (first (om-test/by-class root "checkbox"))]
      (simulate-change checkbox (om-test/checked-state))
      (wait (:checked @state)))))

(deftestw ^:async change-is-simulated-for-input-text
  (let [state (atom {})
        root (om-test/create-root form-component state)]
    (let [input (first (om-test/by-class root "name"))]
      (simulate-change input (om-test/value-state "name" "john"))
      (wait (= "john" (:name @state))))))

(deftestw ^:async click-is-simulated
  (let [state (atom {})
        root  (om-test/create-root form-component state)]
    (let [button  (first (om-test/by-class root "button"))]
      (simulate-click button)
      (wait (:clicked @state)))))

(deftestw ^:async wait-waits-for-condition
  (let [state (atom {})
        root (om-test/create-root form-component state)
        button (first (om-test/by-class root "button"))]
    (is (not (:clicked @state)))
    (simulate-click button)
    (wait (:clicked @state))
    (is (:clicked @state))))

(deftestw ^:async wait-waits-for-condition-if-delayed
  (let [state (atom {})
        root (om-test/create-root form-component state)]
    (let [button (first (om-test/by-class root "button"))]
      (js/setTimeout (fn []
                       (simulate-click button)) 500)
      (wait (:clicked @state)))))

(deftestw ^:async wait*-waits-for-response-with-custom-options
  (let [state (atom {})
        root  (om-test/create-root form-component state)]
    (wait (first (om-test/by-class root "button")))
    (let [button (first (om-test/by-class root "button"))]
      (js/setTimeout (fn []
                       (simulate-click button)) 3000)
      (wait {:timeout 4000} (:clicked @state)))))

(deftestw ^:async wait-let-waits-for-each-condition
  (let [state (atom {})
        root (om-test/create-root form-component state)]
    (let [button (first (om-test/by-tag root "button"))]
      (js/setTimeout (fn []
                       (simulate-click button)) 1000)
      (wait-let [a 10
                 clicked (:clicked @state)
                 c 20]
                (is (= (+ a c) 30))
                (done)))))

(deftestw ^:async test-large-component
  (let [state (atom {})
        root (om-test/create-root large-component state)]
    (let [button (first (om-test/by-tag root "button"))]
      (simulate-click button)
      (wait (:clicked @state))
      (is (:clicked @state)))))
            
