(ns om-test.core-test
  (:require-macros [cemerick.cljs.test :refer (is deftest are done)])
  (:require [om.core :as om :include-macros true]
            [om-test.core :as om-test :include-macros true]
            [om.dom :as dom :include-macros true]
            [cemerick.cljs.test :as t]))

(defn hello-component
  [_ _]
  (om/component (dom/div #js {:className "text"} "hello")))

(defn message-component
  [state _]
  (om/component (dom/div #js {:className "message"}
                         (str "hello " (:name state)))))

(defn form-component
  [state _]
  (om/component
   (dom/div 
    nil
    (dom/input #js {:type "checkbox"
                    :className "checkbox"
                    :checked (:checked state)
                    :onChange #(om/update! state 
                                           :checked 
                                           (-> % .-target .-checked))})
    (dom/input #js {:type "text"
                    :className "name"
                    :value (:name state)
                    :onChange #(om/update! state 
                                           :name 
                                           (-> % .-target .-value))})
    (dom/button #js {:onClick #(om/update! state :clicked true)
                     :className "button"}))))

(deftest test-render-to-str
  (is (= (om-test/render-to-str 
          (om/build hello-component nil)) 
         "<div class=\"text\">hello</div>")))

(deftest test-render
  (is (= (om-test/render message-component {:name "john"})
         "<div class=\"message\">hello john</div>")))

(deftest test-by-tag
  (let [root  (om-test/render-into-document message-component 
                                            {:name "foo"})]
    (is (= (count (om-test/by-tag root "div" true)) 1))
    (is (= (-> (om-test/by-tag root "div" true)
               first om/get-node .-innerHTML)
           "hello foo"))))
      
(deftest test-by-class
  (let [root (om-test/render-into-document 
              message-component {:name "foo"})]
    (is (= (count (om-test/by-class root "message" true)) 1))
    (is (= (-> (om-test/by-class root "message" true)
               first om/get-node .-innerHTML)
           "hello foo"))))

(deftest ^:async change-is-simulated-for-checkbox
  (let [state (atom {:checked false})]
    (om-test/om-root 
     form-component state
     (fn [state owner] 
       (om-test/wait-let
        [checkbox (first (om-test/by-class owner "checkbox"))]
        (om-test/simulate-change checkbox (om-test/checked-state))
        (om-test/wait (:checked @state)
                      (done)))))))

(deftest ^:async change-is-simulated-for-input-text
  (let [state (atom {})]
    (om-test/om-root
     form-component state 
     (fn [state owner]
       (om-test/wait-let 
        [input (first (om-test/by-class owner "name"))]
        (om-test/simulate-change input (om-test/value-state "name" "john"))
        (om-test/wait (= "john" (:name @state))
                      (done)))))))

(deftest ^:async click-is-simulated
  (let [state (atom {})]
    (om-test/om-root
     form-component state
     (fn [state root]
       (om-test/wait-let
        [button  (first (om-test/by-class root "button"))]
        (om-test/simulate-click button)
        (om-test/wait (:clicked @state)
                      (done)))))))

(deftest ^:async wait-waits-for-condition
  (let [state (atom {})]
    (om-test/om-root
     form-component state
     (fn [state root]
       (om-test/wait-let
        [button (first (om-test/by-class root "button"))]
        (om-test/simulate-click button)
        (om-test/wait (:clicked @state)
                      (done)))))))

(deftest ^:async wait-waits-for-condition-if-delayed
  (let [state (atom {})]
    (om-test/om-root
     form-component state
     (fn [state root]
       (om-test/wait-let
        [button (first (om-test/by-class root "button"))]
        (js/setTimeout (fn []
                         (om-test/simulate-click button)) 500)
        (om-test/wait (:clicked @state)
                      (done)))))))

(deftest ^:async wait*-waits-for-response-with-custom-options
  (let [state (atom {})]
    (om-test/om-root
     form-component state
     (fn [state root]
       (om-test/wait-let
        [button (first (om-test/by-class root "button"))]
        (js/setTimeout (fn []
                         (om-test/simulate-click button)) 3000)
        (om-test/wait {:timeout 4000}
                      (:clicked @state)
                      (done)))))))

(deftest ^:async wait-let-waits-for-each-condition
  (om-test/om-root
   form-component {:state (atom {})}
   (fn [state root]
     (om-test/wait-let 
      [button (first (om-test/by-class root "button"))]
      (js/setTimeout (fn []
                       (om-test/simulate-click button)) 1000)
      (om-test/wait-let [a 10
                         clicked (:clicked @state)
                         c 20]
                        (is (= (+ a c) 30))
                        (done))))))



