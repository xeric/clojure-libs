(defproject xeric/pedestal-shiro "0.2.2-SNAPSHOT"
  :description "Pocheshiro integration utilities for pedestal"
  :url "http://bitbucket.org/xeric/clojure-libs/pedestal-shiro"
  :dependencies [[pocheshiro "0.1.1"]
                 [io.pedestal/pedestal.service "_"]
                 [io.pedestal/pedestal.jetty "_" :scope "dev"]
                 [ch.qos.logback/logback-classic "1.1.2" 
                  :exclusions [org.slf4j/slf4j-api]]
                 [org.slf4j/jul-to-slf4j "1.7.7"]
                 [org.slf4j/jcl-over-slf4j "1.7.7"]
                 [org.slf4j/log4j-over-slf4j "1.7.7"]]
  :min-lein-version "2.0.0"
  :global-vars {*warn-on-reflection* true}
  :resource-paths ["config", "resources"]
  :plugins [[lein-modules "0.3.9"]]
  :profiles {:test 
             {:aliases {"run-dev" 
                        ["trampoline" "run" "-m" "pedestal-shiro.server/run-dev"]}
                   :dependencies [[io.pedestal/pedestal.service-tools "_"]]}}
  :main ^{:skip-aot true} pedestal-shiro.server)

