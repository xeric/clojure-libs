(ns pedestal-shiro.core
  (:import [java.util ArrayList]
           [org.apache.shiro.authc HostAuthenticationToken 
            RememberMeAuthenticationToken UsernamePasswordToken]
           [org.apache.shiro.authc.credential PasswordMatcher]
           [org.apache.shiro.web.util WebUtils]
           [org.apache.shiro.realm AuthorizingRealm]
           [org.eclipse.jetty.servlet ServletContextHandler]
           [org.eclipse.jetty.server.session SessionHandler]
           [org.apache.shiro.subject Subject]
           [org.apache.shiro.subject.support SubjectThreadState]
           [org.apache.shiro.web.subject WebSubject WebSubject$Builder]
           [org.apache.shiro.web.servlet ShiroHttpServletRequest])
  (:require [pocheshiro.core :as shiro]
            [io.pedestal.log :as log]
            [io.pedestal.http.route :as route]
            [io.pedestal.http :as server]
            [ring.util.response :as ring-resp]
            [io.pedestal.interceptor :as interceptor
             :refer [interceptor definterceptorfn]]))

(defn ^Subject current-subject
  "Returns the current shiro subject"
  []
  (shiro/get-bound-subject))

(defn authenticated?
  "Checks if the current user is authenticated"
  []
  (.isAuthenticated (current-subject)))

(defn authorized?
  [& {:keys [roles permissions]}]
  "Checks if the current user has a given permission"
  (let [^Subject subject (current-subject)]
    (and (or (not roles)
             (some true? (.hasRoles subject (map name roles))))
         (or (not permissions)
             (loop [permissions permissions]
               (if-let [p (first permissions)]
                 (if-not (.isPermitted subject ^String (name p))
                   false
                   (recur (rest permissions)))
                 true))))))

(defn throw-forbidden
  "Throws a forbidden exception"
  [args]
  (throw (ex-info "403 forbidden" (merge {::type :unauthenticated} args))))

(defn user-credentials
  "Creates a UsernamePasswordToken from username and password"
  ([username password] (user-credentials username password false))
  ([username password remember-me?]
     (UsernamePasswordToken. ^String username ^String password ^boolean remember-me?)))

(defrecord Token [username id host remember-me?]
  Object
  (toString [this] "AuthenticationToken for Token based authentication")
  HostAuthenticationToken
  (getHost [this] host)
  RememberMeAuthenticationToken
  (getPrincipal [this] username)
  (getCredentials [this] id)
  (isRememberMe [this] remember-me?))

(defn ^Token token
  "Creates a token based on token id"
  [& {:keys [username id host remember-me?]}]
  {:pre (some? id)}
  (Token. username id host remember-me?))
  
(defn token-realm
  "Defines a token based realm"
  [& {:keys [supports? get-authentication get-authorization
             realm-name passwords]
      :or {realm-name "token-realm"
           passwords (shiro/iterated-hashed-passwords {})
           supports? (partial instance? Token)}}]
  (shiro/defrealm-fn realm-name 
    (doto (PasswordMatcher.) (.setPasswordService passwords))
    :supports? supports?
    :get-authentication get-authentication
    :get-authorization get-authorization))

(defn guard-with
  "Interceptor for guarding pedestal routes. Supported keys are :-
   :check             : map containing roles and permissions (as keys) 
                        to check authorization against.
  :unauthenticated-fn : function to be called in case of authentication failure
  :unauthorized-fn    : function to be called in case of authorization failure"
  [check unauthenticated-fn unauthorized-fn]
  (fn [{req :request :as context}]
    (if (authenticated?)
      (if (or (nil? check)
              (and (map? check)
                   (authorized? :roles (:roles check) 
                                :permissions (:permissions check)))
              (and (fn? check) (check)))
        context
        (unauthorized-fn context))
      (unauthenticated-fn context))))

(defn access-forbidden-handler
  "Default access forbidden handler"
  [silent? & {:keys [type reason reason-fn]
              :or {type :unauthenticated 
                   reason "You are not allowed to access this resource"}}]
  (let [reason-fn (or reason-fn (constantly reason))]
    (fn [context]
      (throw-forbidden {:silent? silent? 
                        ::type type
                        :reason (reason-fn type context)}))))  

(definterceptorfn guard
  [& {:keys [check unauthenticated-fn unauthorized-fn silent?]
      :or {silent? true}}]
  (let [unauthenticated-fn (or unauthenticated-fn 
                               (access-forbidden-handler silent?))
        unauthorized-fn    (or unauthorized-fn
                               (access-forbidden-handler silent?))]
    (interceptor :name ::guard
                 :enter (guard-with check unauthenticated-fn unauthorized-fn))))

(defn non-session-encoding-response [res]
  ;; Do not encode JSESSIONID into the url matrix
  (proxy [javax.servlet.http.HttpServletResponseWrapper] [res]
    (encodeURL [url] url)
    (encodeUrl [url] url)
    (encodeRedirectURL [url] url)
    (encodeRedirectUrl [url] url)))

(defn- create-web-subject [manager req resp]
  (.buildWebSubject (WebSubject$Builder. manager req resp)))

(defn associate-subject
  [security-manager-retriever]
  (fn [{req :request :as context}]
    (let [s-req  (:servlet-request req)
          s-ctxt (:servlet-context req)
          shiro-req (ShiroHttpServletRequest. s-req s-ctxt true)
          s-res (non-session-encoding-response (:servlet-response req))
          sec-mgr (security-manager-retriever req)
          subject (create-web-subject sec-mgr shiro-req s-res)
          shiro-state (SubjectThreadState. subject)
          req' (assoc req :servlet-response s-res :shiro-state shiro-state)]
      (.bind shiro-state)
      (assoc context :request req'))))

(defn cleanup-subject
  [{req :request :as context}]
  (if (:shiro-state req)
    (.clear ^SubjectThreadState (:shiro-state req)))
  context)

(defn save-request-and-redirect 
  [{req :request res :response :as context} url]
  (let [{:keys [uri request-method query-string]} req
        url (if (and (= request-method :get) (some? uri))
              (route/url-for url :params {:uri uri :q query-string})
              (route/url-for url))]
    (assoc context :response (ring-resp/redirect url))))

(defn access-forbidden-catcher [url]
  (fn [context error]
    (let [error-data (ex-data error)
          type (::type error-data)]
      (if-not (nil? type)
        (if (true? (:silent? error-data))
          (dissoc context :response)
          (save-request-and-redirect context url))
        (throw error)))))

(definterceptorfn security
  [{:keys [login-page security-manager-retriever]
    :or {login-page "/login"}}]
  (interceptor :name ::security
               :enter (associate-subject security-manager-retriever)
               :leave cleanup-subject
               :error (access-forbidden-catcher login-page)))

(defn session-handler
  [service-map]
  (letfn [(set-handler [context]
            (.setSessionHandler ^ServletContextHandler context (SessionHandler.)))]
    (-> service-map
        (assoc-in [::server/container-options :context-configurator] set-handler)
        (assoc-in [::server/jetty-options :context-configurator] set-handler))))

(defn servlet-session []
  (let [attributes (atom {})
        valid (atom true)]
    (proxy [javax.servlet.http.HttpSession] []
      (setAttribute [k v] (swap! attributes assoc k v))
      (getAttribute [k] (@attributes k))
      (removeAttribute [k] (swap! attributes dissoc k))
      (invalidate [] 
        (reset! attributes {})
        (reset! valid false)))))

(defn request-wrapper [req session]
  (proxy [javax.servlet.http.HttpServletRequestWrapper] [req]
    (getRemoteHost [] "localhost")
    (getCookies [] nil)
    (getSession 
      ([create?] session)
      ([] session))))

(defmacro with-realm 
  [realm & forms]
  `(do
     (SecurityUtils/setSecurityManager (DefaultSecurityManager. ~realm))
     ~@forms
     (SecurityUtils/setSecurityManager nil)
     (ThreadContext/remove)))

