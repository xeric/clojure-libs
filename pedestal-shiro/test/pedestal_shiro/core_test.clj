(ns pedestal-shiro.core-test
  (:import [org.apache.shiro SecurityUtils]
           [org.apache.shiro.util ThreadContext]
           [org.apache.shiro.mgt DefaultSecurityManager]
           [org.apache.shiro.authc.credential DefaultPasswordService]
           [org.apache.shiro.realm AuthorizingRealm]
           [org.apache.shiro.authc AuthenticationException
            IncorrectCredentialsException])
  (:require [pedestal-shiro.core :as ps :refer [with-realm]]
            [io.pedestal.test :refer [response-for]]
            [pedestal-shiro.service :as service]
            [pedestal-shiro.server :as server]
            [io.pedestal.http :as bootstrap]
            [pocheshiro.core :as s]
            [ring.util.response :as ring-resp]
            [clojure.test :refer :all]))

(use-fixtures :each (fn [f]
                      (try (f)
                           (finally 
                             (do (SecurityUtils/setSecurityManager nil)
                                 (ThreadContext/remove))))))

(deftest authentication-check
  (let [passwords (s/iterated-hashed-passwords {:hash-algo "SHA-256"
                                                :hash-iterations 500000})
        token     "unique-token"
        username  "anonymous"
        token-x   (.encryptPassword ^DefaultPasswordService passwords token)
        realm     (ps/token-realm 
                   :get-authentication
                   (fn [token] {:principal "anonymous" :credentials token-x}))]
    (testing "authenticates with correct token"
      (with-realm ^AuthorizingRealm realm
        (s/login! (ps/token :id token :username username))
        (is (ps/authenticated?))))
    (testing "doesn't authenticate with wrong token"
      (with-realm ^AuthorizingRealm realm
        (is (thrown? IncorrectCredentialsException
                    (s/login! (ps/token :id "invalid-token"))))))))

(deftest authorization-check
  (let [passwords (s/iterated-hashed-passwords {:hash-algo "SHA-256"
                                                :hash-iterations 50000})
        token "unique-token"
        token-x (.encryptPassword ^DefaultPasswordService passwords token)
        realm (ps/token-realm 
               :get-authentication
               (fn [token] {:principal "anonymous" :credentials token-x})
               :get-authorization
               (fn [pcs]
                 {:roles [:anonymous :reader]
                  :permissions ["read-all"]}))]
    (with-realm ^AuthorizingRealm realm
      (s/login! (ps/token :id token))
      (is (ps/authenticated?))
      (is (ps/authorized? :roles [:anonymous]))
      (is (ps/authorized? :roles [:anonymous :reader]))
      (is (ps/authorized? :permissions ["read-all"]))
      (is (not (ps/authorized? :roles [:anonymous] :permissions ["write"])))
      (is (not (ps/authorized? :roles [:writer]))))))
