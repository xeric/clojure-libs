(defproject xeric/pedestal-tools "0.2.2-SNAPSHOT"
  :description "Helper library for the pedestal and enlive"
  :global-vars {*warn-on-reflection* true}
  :dependencies [[org.clojure/clojurescript "_"]
                 [io.pedestal/pedestal.service "_"]
                 [enlive "_"]
                 [xeric/i18n :version]
                 [io.pedestal/pedestal.jetty "_" :scope "dev"]
                 [io.aviso/twixt "0.1.13"]
                 [net.tanesha.recaptcha4j/recaptcha4j "0.0.7"]
                 [com.novemberain/pantomime "2.3.0"]]
  :resource-paths ["config", "resources"]
  :plugins [[lein-modules "0.3.9"]]
  :source-paths ["src/clj"]
  :test-paths ["test/clj", "test-resources"])
