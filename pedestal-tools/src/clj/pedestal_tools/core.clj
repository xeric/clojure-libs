(ns pedestal-tools.core
  (:require [io.pedestal.http.route :as route]))

(defn value 
  [{params :params :as req} field-name]
  (get params field-name))

(defn has-errors?
  [{errors ::errors :as req}]
  (not (empty? errors)))

(defn all-errors
  [{errors ::errors :as req}]
  (mapcat (fn [[k v]] v) errors))

(defn errors
  ([req] (errors req :form))
  ([{errors ::errors :as req} field-name]
     (get errors field-name)))

(defn set-error
  ([req error] (set-error req :form error))
  ([req field error]
     (update-in req [::errors field] (fnil conj []) error)))

(defn url-for
  [_ key & args]
  (apply route/url-for key args))

(defn set-errors [req field messages]
  (loop [req req messages messages]
    (if-let [message (first messages)]
      (recur (set-error req field message) (rest messages))
      req)))

(defn set-validation-errors [req [ve]]
  (if-not (nil? ve)
    (loop [req req ve ve]
      (if-let [[field messages] (first ve)]
        (recur (set-errors req field messages) (rest ve))
        req))
    req))

