(ns pedestal-tools.edn
  (:require [ring.util.response :as ring-resp]))

(defn response
  "EDN response. status corresponds to application level
   status and NOT http status."
  ([body] (response :success body))
  ([status body]
     {:status 200
      :headers {"Content-Type" "application/edn"}
      :body (pr-str {:status status :data body})}))

(defn failure
  "Failure response. The http header is still 200 as the
   response is processed for any application exception"
  [body]
  (response :failure body))

(defn redirect
  "Redirect at application level. Useful for edn based
   redirects. The HTTP status send is still 200"
  [url]
  {:status 200
   :headers {"Content-Type" "application/edn"}
   :body (pr-str {:redirect? true :location url})})
