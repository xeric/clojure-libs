(ns pedestal-tools.grid
  (require [pedestal-tools.transforms :as t]
           [pedestal-tools.core :refer [url-for]]
           [net.cgrand.enlive-html :as html :refer :all]))

(defn add-grid-header
  "Draws a grid header." 
  [req {:keys [columns route-name]}]
  (transformation 
   [:thead :tr :th] 
   (clone-for [{:keys [sort-column title key]} columns]
              (do->
               (add-class (name key))
               (content
                (if-not sort-column
                  title
                  (let [url 
                        (url-for 
                         req route-name 
                         :params {:sort-column (name sort-column)})]
                    (html/html [:a {:href url} title]))))))))

(defn add-grid-row
  [columns row]
  (transformation
   [:tr :td] 
   (clone-for [{:keys [key renderer]} columns]
              (do->
               (add-class (name key))
               (content
                (if renderer
                  (renderer row)
                  (get row key)))))))

(defn add-grid-pager
  [req 
   {:keys [columns page-size row-count route-name sort-column] :as page}]
  (transformation
   [:ul :li]
   (let [page-count (quot (- (+ row-count page-size) 1) page-size)
         max-page-no (+ page-count 1)]
     (clone-for [p (range 1 max-page-no)]
                (let [params {:page (str p)}
                      params-with-sorting 
                      (if sort-column
                        (assoc params :sort-column sort-column)
                        params)]
                (content (html/html 
                          [:a {:href (url-for 
                                      req route-name :params params)}
                                     (str p)])))))))

(defsnippet grid "templates/pedestal_tools/grid.html" [:table] 
  [req page]
  [:table] (add-grid-header req page)
  [:tfoot :tr :td] (set-attr :colspan (count (:columns page)))
  [:table :tfoot :tr :td :ul] (add-grid-pager req page)
  [:table :tfoot :tr :td :.page-count] (:render-page-count page)
  [:table :tbody :tr]
  (clone-for [row (:rows page)]
             (add-grid-row (:columns page) row)))
