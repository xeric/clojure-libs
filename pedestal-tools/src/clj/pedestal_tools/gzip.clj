(ns pedestal-tools.gzip
  (:require [clojure.java.io :as io])
  (:import (java.io InputStream Closeable File
                    PipedInputStream PipedOutputStream)
           (java.util.zip GZIPOutputStream)))

(defn has-file-extension? [file-name extensions]
  (let [extension-pattern (clojure.string/join "|" extensions)
        complete-pattern (str "^.+\\.(" extension-pattern ")$")
        extensions-reg-exp (re-pattern complete-pattern)]
    (not (nil? (re-find extensions-reg-exp file-name)))))

(defn- accepts-gzip?
  "Returns true if the request accepts gzip"
  [request]
  (if-let [accepts (get-in request [:headers "accept-encoding"])]
    (re-seq
     #"(gzip\s*,?\s*(gzip|deflate)?|X{4,13}|~{4,13}|\-{4,13})"
     accepts)))

(defn- set-response-headers
  "Set Vary to make sure proxies don't deliver the wrong content"
  [headers]
  (let [vary (get headers "vary")
        headers (-> headers
                    (assoc "Content-Encoding" "gzip")
                    (dissoc "Content-Length"))]
    (if (nil? vary)
      headers
      (-> headers
          (assoc "Vary" (str vary ", Accept-Encoding"))
          (dissoc "vary")))))

(defn- unencoded-type?
  [headers]
  (nil? (headers "content-encoding")))

(defn- compress-body
  [body]
  (let [p-in (PipedInputStream.)
        p-out (PipedOutputStream. p-in)]
    (future
      (with-open [out (GZIPOutputStream. p-out)]
        (if (seq? body)
          (doseq [string body] (io/copy (str string) out))
          (io/copy body out)))
      (when (instance? Closeable body)
        (.close body)))
    p-in))

(defn- gzip-response*
  [resp]
  (-> resp
      (update-in [:headers ] set-response-headers)
      (update-in [:body ] compress-body)))

(def ^:private min-length 859)
(def ^:private supported-status? #{200, 201, 202, 203, 204, 205 403, 404})

(defn- supported-size?
  [resp]
  (let [{body :body} resp]
    (cond
      (string? body) (> (count body) min-length)
      (seq? body) (> (count body) min-length)
      (instance? File body) (> (.length body) min-length)
      :else true)))

(defn- supported-response?
  [resp]
  (let [{:keys [status headers]} resp]
    (and (supported-status? status)
         (unencoded-type? headers)
         (supported-size? resp))))

(defn gzip-response
  [req resp]
  (if (and (accepts-gzip? req)
           (supported-response? resp))
    (gzip-response* resp)
    resp))
