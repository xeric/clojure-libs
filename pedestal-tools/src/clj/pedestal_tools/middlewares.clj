(ns pedestal-tools.middlewares
  (:require [io.pedestal.interceptor :as interceptor
             :refer [defon-request on-request definterceptorfn
                     defon-response interceptor]]
            [io.pedestal.http.route :as route]
            [i18n.core :as i18n]
            [clojure.string :as str]            
            [ring.middleware.session.cookie :as cookie]
            [io.pedestal.http.ring-middlewares :as ring-middlewares])
    (:import [i18n.core i18nConfig]))

(interceptor/definterceptor session-interceptor
  (ring-middlewares/session {:cookie-name "SID"
                             :store (cookie/cookie-store)}))

(defn- keyword-syntax? [s]
  (re-matches #"[A-Za-z*+!_?-][A-Za-z0-9*+!_?-]*" s))

(defn- keyify-params [target]
  (cond
    (map? target)
      (into {}
        (for [[k v] target]
          [(if (and (string? k) (keyword-syntax? k))
             (keyword k)
             k)
           (keyify-params v)]))
    (vector? target)
      (vec (map keyify-params target))
    :else
      target))

(defn- edn-keyword-params-request
  [req]
  (update-in req [:edn-params] keyify-params))

(defon-request edn-keyword-params edn-keyword-params-request)

(defn prefix-match 
  [^String path-info {:keys [lang]}]
  (.startsWith path-info (str "/" lang "/")))

(defn split-locale-prefix
  [locales {:keys [path-info uri] :as req}]
  (if (>= (count path-info) 4)
    (when-let [locale (first (filter (partial prefix-match path-info) locales))]
      [(:lang locale) 
       (str "/" (subs path-info 4))
       (str "/" (subs uri 4))])))

(definterceptorfn locale-aware-interceptor 
  [locales default-locale]
  (interceptor :enter
               (fn [{:keys [request] :as context}]
                 (if-let [[locale path-info uri] 
                          (split-locale-prefix locales request)]
                   (update-in context [:request]
                              assoc :locale locale :path-info path-info
                              :uri uri)
                   (assoc-in context [:request :locale] default-locale)))))
 
(def errors-interceptor
  (interceptor :leave
               (fn [{:keys [request response] :as context}]
                 (assoc-in context [:response :errors] (:errors request)))))

(defn url-for-with-locale
  [url-for {:keys [locale] :as req}]
  (fn [route-name & args]
    (apply url-for route-name
           (conj (vec args) :context locale))))

(definterceptorfn locale-aware-url-for-interceptor [default-locale]
  (interceptor :enter
               (fn [{:keys [request] :as context}]
                 (if (not= (:locale request) default-locale) 
                   (update-in context 
                              [:bindings #'route/*url-for*]
                              url-for-with-locale request)
                     context))))

(definterceptorfn i18n-configuration-interceptor [message-config]
  (interceptor :enter
               (fn [{:keys [request] :as context}]
                 (let [locale (if (:locale request)
                                (:locale request)
                                (i18n/default-locale message-config))
                       i18n-config (i18nConfig. message-config (keyword locale))]
                   (assoc-in context
                              [:bindings #'i18n.core/*i18n-config*]
                              i18n-config)))))

(defmacro rest-routes
  [key namespace path-key]
  (let [key-name    (name key)
        url         (str "/" key-name)
        path-key    (str "/" path-key)
        get-key     (keyword (str "get-" key-name))
        delete-key  (keyword (str "delete-" key-name))
        post-key    (keyword (str "new-" key-name))
        put-key     (keyword (str "save-" key-name))
        fn-name     #(str namespace "/" %)]
    `[~url
      {:get [~key (symbol ~(fn-name "list"))]
       :post [~post-key (symbol ~(fn-name "new"))]}
      [~path-key
       {:get [~get-key (symbol ~(fn-name "get"))]
        :put [~put-key (symbol ~(fn-name "save"))]
        :delete [~delete-key (symbol ~(fn-name "delete"))]}
       ^:interceptors [(symbol ~(fn-name "load-document"))]]]))
