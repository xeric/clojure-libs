(ns pedestal-tools.react
  (:require [pedestal-tools.transforms :as t]
            [io.pedestal.http.route.definition
               :refer [defroutes expand-routes]]
            [net.cgrand.enlive-html :as html :refer :all]))

(def react-div (html-snippet "<div></div>"))

(defn data-config 
  [config]
  (transformation 
   [root]
   (set-attr :data-config (pr-str config))))

(defn messages-config
  [messages]
  (html/transformation
   [root]
   (do->
    (add-class "messages")
    (set-attr :data-messages (pr-str messages)))))

(defmacro defreactview
  [name args & transforms]
  `(defn ~name ~args
     (at react-div 
         [html/root]              
         (do->
          (set-attr :id (str '~name))
          ~@transforms))))
