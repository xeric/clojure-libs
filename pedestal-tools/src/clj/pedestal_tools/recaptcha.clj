(ns pedestal-tools.recaptcha
  (:import [net.tanesha.recaptcha 
            ReCaptcha ReCaptchaFactory ReCaptchaImpl ReCaptchaResponse]))

(defprotocol IReCaptcha
  (recaptcha [_] [_ error options])
  (valid? [_ request]))

(defrecord KeyPair [public-key private-key]
  IReCaptcha
  (recaptcha
    [key-pair]
    (recaptcha  key-pair nil nil))

  (recaptcha
    [{:keys [public-key private-key]} errors options]
    (-> (ReCaptchaFactory/newReCaptcha public-key private-key false)
        (.createRecaptchaHtml errors options)))

  (valid? [key-pair request]
    (let [params (:params request)
          captcha (doto (ReCaptchaImpl.)
                      (.setPrivateKey (:private-key key-pair)))
          response (.checkAnswer captcha (:remote-addr request)
                                 (:recaptcha_challenge_field params)
                                 (:recaptcha_response_field params))]
      (.isValid response))))

