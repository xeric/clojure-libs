(ns pedestal-tools.transforms
  (:require [net.cgrand.enlive-html :as html :refer :all]
            [io.pedestal.http.route :as route]
            [i18n.core :refer [t]]
            [clojure.string :as string]))

(defprotocol IForm
  (has-errors? [this])
  (composite-errors [this])
  (error [this name])
  (value [this name]))

(extend-protocol IForm
  clojure.lang.PersistentArrayMap
  (has-errors? [this] 
    (contains? this :errors))
  (composite-errors [this] 
    (get-in this [:errors :composite]))
  (error [this name]
    (get-in this [:errors name]))
  (value [this name]
    (get this name))
  clojure.lang.PersistentHashMap
  (has-errors? [this] 
    (contains? this :errors))
  (composite-errors [this] 
    (get-in this [:errors :composite]))
  (error [this name]
    (get-in this [:errors name]))
  (value [this name]
    (get-in this name)))

(defn url-for
  [key & args]
  (apply route/url-for key args))

(defmacro maybe-content
  "Predicate to replace content only if expr is true"
  ([expr] `(if-let [x# ~expr] (html/content x#) identity))
  ([expr & exprs] `(maybe-content (or ~expr ~@exprs))))

(defmacro maybe-substitute
  "Predicate to substitute content only if expr is true"
  ([expr] `(if-let [x# ~expr] (html/substitute x#) identity))
  ([expr & exprs] `(maybe-substitute (or ~expr ~@exprs))))

(defn template-path
  "Resolves template-path. The template-path is assumed to be on
   the classpath with the same directory structure as the namespace"
  [template-name]
  (if-let [path (-> (meta *ns*) :template-path)]
    path
    (-> (->> (string/split (str *ns*) #"\.") (string/join "/") (str "templates/"))
        (str "/" template-name ".html")
        (string/replace #"-" "_"))))

(defn include-js
  "Html script tag"
  [src]
  (first (html/html [:script {:src src :type "text/javascript"}])))

(defn include-inline-js
  "Inline script tag"
  [content]
  (first (html/html [:script {:type "text/javascript"} content])))

(defn include-css
  "Include css link tag"
  [href]
  (first (html/html [:link {:rel "stylesheet" :type "text/css" 
                            :href href}])))

(defmacro defview
  "Creates a snippet of a given name assuming the template
   is in the same path"
  [name args & forms]
  `(defsnippet ~name ~(template-path name) [root] ~args ~@forms))

(defmacro deflayout
  "Creates a layout"
  [name args & forms]
  `(deftemplate ~name ~(template-path name) ~args ~@forms))

(defn- field-name [node] 
  (-> node :attrs :name keyword))

(defn- field-value [node] 
  (-> node :attrs :value))

(defn- form-value [form node]
  (value form (keyword (field-name node))))

(defn- equal-or-contains 
  [value current-value]
  (let [value-set (cond 
                   (set? value) value 
                   (and (seq? value) (not (string? value))) (set value)
                   :else (hash-set (str value)))]
    (contains? value-set current-value)))

(defmacro when-apply [condition & forms]
  `(if ~condition ~@forms identity))

(defn- checkbox-select 
  [form]
  (fn [node]
    (let [new-value (form-value form node)
          current-value (field-value node)]
      (at node
          [:input]
          (when-apply (equal-or-contains new-value current-value)
                      (set-attr :checked "checked"))))))

(defn- radio-select 
  [form]
  (fn [node]
    (let [new-value (form-value form node)
          current-value (field-value node)]
      (if (= (str new-value) current-value)
        (assoc-in node [:attrs :selected] "selected")
        node))))

(defn- input-text 
  [form]
  (fn [node]
    (if-let [new-value (form-value form node)]
      (assoc-in node [:attrs :value] new-value)
      node)))

(defn error-message
  [error]
  (if (coll? error)
    (apply t error)
    (t error)))

(defn- form-errors 
  [form]
  (fn [node]
    (at node
        [:.errors] #(if (has-errors? form) %)
        [:.errors :.error] (clone-for [error (composite-errors form)]
                                      (content (error-message error))))))

(defn- form-group-errors 
  [form]
  (fn [node#]
    (let [field (-> node# :attrs :data-field keyword)]
      (at node#
          [:.form-group]
          (do->
           (remove-attr :data-field)
           (if (and field (error form field))
             (do->
              (append (html [:span {:class "help-block"} (error-message (error form field))]))
              (add-class "has-error"))
             identity))))))

(defn form
  "updates a form with the input values in the state"
  [form post-back]
  (transformation
   [:.errors] (form-errors form)
   [[:input (attr= :type "text")]] (input-text form)
   [[:input (attr= :type "hidden")]] (input-text form)
   [[:input (attr= :type "email")]] (input-text form)
   [[:input (attr= :type "checkbox")]] (checkbox-select form)
   [[:input (attr= :type "radio")]] (radio-select form)
   [:.form-group] (form-group-errors form)
   [:form] (set-attr :action (url-for post-back))))

(defn select-options 
  [form values]
  (fn [node]
    (at node
        [:option]
        (clone-for [{val :value text :text} values]
                   (do->
                    (content text)
                    (when-apply (= val (form-value form node)) 
                                (set-attr :selected "selected"))
                    (set-attr :value val))))))

(defn checkbox-group
  "Transforms to checkbox to a checkbox group with the given values 
   as choices"
  [form values]
  (fn [node]
    (at node
        [[:input (attr= :type "checkbox")]]
        (clone-for [{val :value text :text} values]
                   (do->
                    (when-apply (equal-or-contains 
                                 (form-value form node) val)
                                (set-attr :checked "checked"))
                    (set-attr :value val)
                    (wrap :label {:class "checkbox"})
                    (append text))))))

(defn url-transform 
  "Transforms all urls for a given selector. Default selector is 
   data-type='link'"
  ([form] (url-transform form [[:a (attr= :data-type "link")]]))
  ([form selector]
     (transformation
      selector 
      (fn [node]
        (update-in node [:attrs :href] 
                   #(url-for (keyword %1)))))))

(defn url-transform-absolute
  "Transforms all urls for a given selector. Default selector is 
   data-type='link-absolute'"
  ([form] (url-transform form [[:a (attr= :data-type "link-absolute")]]))
  ([form condition]
     (transformation
      condition 
      (fn [node]
        (update-in node [:attrs :href] 
                   #(url-for (keyword %1) 
                             :params {:absolute? true}))))))

(defn- roles [form node]
  (let [roles-str (get-in node [:attrs :data-authorized])]
    (map keyword (clojure.string/split roles-str #"\s*,\s*"))))

(defn secure-nodes
  [form authenticated? authorized?]
  (transformation
   [(attr? :data-authenticated)] 
   (fn[n]
     (if (= (= (-> n :attrs :data-authenticated) "true")
            (authenticated? form)) n))
   [(attr? :data-authorized)] 
   #(if (authorized? form (roles form %)) %)))

(defn navigation-active-url
  [form]
  (transformation
   [:ul.nav [:li (has [[:a (attr= :href (:uri form))]])]] 
   (do->
    (add-class "active"))))

(defn translate-messages
  []
  (transformation
   [(attr? :data-message)]
   (fn [node]
     (if-let [message (t (-> node :attrs :data-message keyword))]
       (assoc node :content [message])
       node))))

