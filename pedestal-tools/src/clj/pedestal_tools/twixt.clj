(ns pedestal-tools.twixt
  (:require [io.pedestal.interceptor :as interceptor
             :refer [defon-request on-request definterceptorfn
                     defon-response interceptor]]
            [pedestal-tools.transforms :refer [include-js include-css]]
            [io.pedestal.http.route :as route]
            [clojure.string :as str]
            [io.aviso.twixt.ring :as twixt-ring]
            [io.aviso.twixt.compress :as twixt-compress]
            [io.aviso.twixt :as twixt]
            [io.aviso.twixt.stacks :as twixt-stacks]
            [io.aviso.twixt.exceptions :as twixt-exceptions]
            [ring.util.response :as ring-resp]))

(defn- include-bundles
  [req include-fn & bundles]
  (mapv #(include-fn %)
        (apply concat
               (map
                #(twixt/get-asset-uris (:twixt req) %)
                bundles))))

(defn include-js-bundles
  [req & bundles]
  (apply include-bundles req include-js bundles))

(defn include-css-bundles
  [req & bundles]
  (apply include-bundles req include-css bundles))

(interceptor/definterceptorfn twixt-handler
  []
  (interceptor/before
   (fn [context]
     (if-let [resp (twixt-ring/twixt-handler (:request context))]
       (assoc context :response resp)
       context))))

(defn- is-gzip-supported?
  [req]
  (if-let [encodings (-> req :headers (get "accept-encoding"))]
    (some #(.equalsIgnoreCase ^String % "gzip")
          (str/split encodings #","))))

(interceptor/defon-request compress
  [req]
  (assoc-in req [:twixt :gzip-enabled] (is-gzip-supported? req)))

(interceptor/definterceptorfn twixt-setup
  [& {:keys [twixt-options development-mode]
      :or   {twixt-options twixt/default-options}}]
  (let [twixt-options  (-> twixt-options
                           (assoc :development-mode development-mode)
                           twixt-exceptions/register-exception-reporting
                           twixt-stacks/register-stacks)
        asset-pipeline (twixt/default-asset-pipeline twixt-options)
        twixt          (-> twixt-options
                           (select-keys [:path-prefix :stack-frame-filter 
                                         :development-mode])
                           (assoc :asset-pipeline asset-pipeline)
                           (merge (:twixt-template twixt-options)))]
    (interceptor/before 
     (fn [context]
       (assoc-in context [:request :twixt] twixt)))))

(defn dev-exception-report
  [request ex]
  (-> (twixt-exceptions/build-report request ex)
      ring-resp/response
      (ring-resp/content-type "text/html")
      (ring-resp/status 500)))

(defn production-exception-report
  [request ex]
  (-> (ring-resp/response (.getMessage ex))
      (ring-resp/content-type "text/plain")
      (ring-resp/status 500)))

(def exception-interceptor
  (interceptor :error 
               (fn [{:keys [request] :as context} ex]
                 (let [dev? (-> request :twixt :development-mode)
                       report (if dev? 
                                dev-exception-report
                                production-exception-report)]
                   (assoc context :response
                          (report request ex))))))

