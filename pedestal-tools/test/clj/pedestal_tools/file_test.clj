(ns pedestal-tools.file-test
  (:require [clojure.test :refer :all]
            [ring.util.io]
            [pedestal-tools.file :refer :all]
            [clojure.java.io :as io]
            [pedestal-tools.utils :refer [refer-private]])
  (:import [java.io RandomAccessFile ByteArrayOutputStream]))

(refer-private 'pedestal-tools.file)

(def test-file "target/test_file.txt")

(def file-size 1000)

(defn create-test-file []
  (spit test-file (apply str (take file-size (cycle (range 10))))))

(defn delete-test-file []
  (io/delete-file test-file true))

(deftest test-byte-range-for-file
  (let [file (io/file test-file)]
    (let [{:keys [start end total] :as r} (byte-range file)]
      (is (= start 0))
      (is (= end (dec file-size)))
      (is (= total file-size)))))

(deftest test-byte-range-for-string
  (let [{:keys [start end total]} (byte-range "10-100/1000")]
    (is (= start 10))
    (is (= end 100))
    (is (= total 1000))))

(deftest test-content-range
  (is (= (content-range (byte-range {:start 10 :end 20 :total 100}))
         "bytes 10-20/100")))

(deftest test-load-ranges
  (is (= (load-ranges "bytes 10-20,40-90" 100)
         [{:start 10 :end 20 :length 11 :total 100}
          {:start 40 :end 90 :length 51 :total 100}])))

(deftest test-accepts?
  (are [x y] (accepts? x y)
       "gzip" "gzip"
       "gzip,z2" "gzip"
       "z2,gzip" "gzip"
       "gzip/com,abc" "gzip/*"
       "gzip/com,abc" "*/*"))

(deftest test-write-all-output
  (with-open [file (RandomAccessFile. (io/file test-file) "r")
              output (ByteArrayOutputStream.)]
    (write-all-output output file)
    (is (= (.toString output) (slurp test-file)))))

(deftest test-write-partial-output
  (with-open [file   (RandomAccessFile. (io/file test-file) "r")
              output (ByteArrayOutputStream.)]
    (write-partial-output output file 3 3)
    (is (= (.toString output) "345"))))

(deftest test-file-output
  (let [output (ByteArrayOutputStream.)]
    ((file-output (io/file test-file) {:start 3 :end 7 :total 100 :length 5})
     output)
    (is (= (.toString output) "34567"))))

(deftest test-multipart-file-output
  (let [output (ByteArrayOutputStream.)]
    ((multipart-file-output (io/file test-file) 
                            [{:start 3 :end 7 :total 100 :length 5}
                             {:start 10 :end 13 :total 100 :length 4}])
     output)
      (is (= (.toString output)
             (str "\n--MULTIPART_BYTERANGES\n"
                  "Content-Type: text/plain\n"
                  "Content-Range: bytes 3-7/100\n"
                  "34567\n"
                  "--MULTIPART_BYTERANGES\n"
                  "Content-Type: text/plain\n"
                  "Content-Range: bytes 10-13/100\n"
                  "0123\n"
                  "--MULTIPART_BYTERANGES--\n")))))

(deftest test-find-range
  (let [req {:headers
             {"Accept" "*/*"
              "If-Unmodified-Since" "Tue, 02 Jun 2015 16:21:10 GMT"
              "If-Match" "F1_Slimdulge3.mp4_973968_1433262070000"
              "Range" "bytes=24576-973967"
              "User-Agent" "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko"}}
        res (first (find-range req (io/file test-file) nil))]
    (is (= (:length res) 949392))
    (is (= (:start res) 24576))
    (is (= (:end res) 973967))
    (is (= (:total res) 1000))))

(use-fixtures :each (fn [f]
                      (create-test-file)
                      (f)
                      (delete-test-file)))
