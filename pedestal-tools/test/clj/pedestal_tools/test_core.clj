(ns pedestal-tools.test-core
  (:require [pedestal-tools.core :as tools]
            [clojure.test :refer :all]))

(deftest test-errors-are-set
  (let [req (-> {}
                (tools/set-error :foo "bar"))]
    (is (tools/has-errors? req))
    (is (= (tools/errors req :foo) ["bar"]))
    (is (= (tools/all-errors req) ["bar"]))))

