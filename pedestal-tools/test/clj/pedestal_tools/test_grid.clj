(ns pedestal-tools.test-grid
  (:require [pedestal-tools.grid :as grid]
            [pedestal-tools.transforms :as t]
            [clojure.test :refer :all]
            [net.cgrand.enlive-html :as html :refer [sniptest]]))

(defn details-link [row]
  (html/html
   [:a {:href "something"} (:name row)]))

(def page {:columns [{:key :name :title "Name"}
                     {:key :address :title "Address"}]
           :rows [{:name "John" :address "John's address"}
                  {:name "Mary" :address "Mary's address"}]
           :row-count 6
           :page-size 2
           :route-name :test-route
           :current-page 0})

(deftest test-grid-header
  (testing "header is without links if not sortable"
    (is (= (html/sniptest 
            "<table><thead><tr><th></th></tr></thead></table>" 
            [:table] (grid/add-grid-header 
                      {} 
                      {:route :sort-link
                       :columns [{:key :name :title "Name"}]}))
           (str "<table><thead>"
                "<tr><th class=\"name\">Name</th></tr>"
                "</thead></table>")))
    (is (= (html/sniptest 
            "<table><thead><tr><th></th></tr></thead></table>"
            [:table] (grid/add-grid-header {} page))
           (str "<table><thead><tr><th class=\"name\">Name</th>"
                "<th class=\"address\">Address</th></tr>"
                "</thead></table>"))))
  (testing "header has links if sortable"
    (with-redefs [pedestal-tools.core/url-for
                  (fn [_ key & args]
                    (name key))]
      (is (= (html/sniptest 
              "<table><thead><tr><th></th></tr></thead></table>"
              [:table] (grid/add-grid-header 
                        {}
                        {:route-name :sort-link 
                         :columns [{:key :name :title "Name" 
                                    :sort-column "name"}]}))
             (str "<table><thead><tr><th class=\"name\">"
                  "<a href=\"sort-link\">Name</a></th>"
                  "</tr></thead></table>"))))))

(deftest test-grid-row
  (testing "grid row is displayed"
    (is (=(html/sniptest 
           "<table><tbody><tr><td></td></tr></tbody></table>"
           [:tr] (grid/add-grid-row (:columns page) 
                                    (first (:rows page))))
          (str "<table><tbody><tr><td class=\"name\">John</td>"
               "<td class=\"address\">John's address</td></tr>"
               "</tbody></table>"))))
  (testing "custom rendering is possible"
    (let [page-r (assoc-in page [:columns 0 :renderer] details-link)]
      (is (= (html/sniptest 
              "<table><tbody><tr><td></td></tr></tbody></table>"
              [:tr] (grid/add-grid-row (:columns page-r) 
                                       (first (:rows page-r))))
             (str "<table><tbody><tr><td class=\"name\">"
                  "<a href=\"something\">John</a></td>"
                  "<td class=\"address\">John's address</td>"
                  "</tr></tbody></table>"))))))

(deftest test-pagination
  (testing "pages are displayed"
    (with-redefs [pedestal-tools.core/url-for 
                  (fn [_ key & {:keys [params]}]
                    (str "/?page=" (:page params)))]
      (is (= (html/sniptest "<ul><li></li></ul>"
                            (grid/add-grid-pager {} page))
             (str "<ul><li><a href=\"/?page=1\">1</a></li>"
                  "<li><a href=\"/?page=2\">2</a></li>"
                  "<li><a href=\"/?page=3\">3</a></li>"
                  "</ul>"))))))
