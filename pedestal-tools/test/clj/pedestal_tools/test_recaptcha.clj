(ns pedestal-tools.test-recaptcha
  (:require [net.cgrand.enlive-html :as html]
            [clojure.test :refer :all]
            [pedestal-tools.recaptcha :refer :all])
  (:import [pedestal_tools.recaptcha KeyPair]))

(def key-pair (KeyPair. "6LeZGMkSAAAAAJ2v9r3sKK6FIwuwIIKwqV1Lt1hl"
                        "6LeZGMkSAAAAAGrwa4NVS9cfJtuyhySqOu5M2a7u"))

(deftest html-for-recaptcha-is-generated
  (let [recaptcha-html (-> (recaptcha key-pair)
                           (html/html-snippet))]
    (testing "recaptcha contains challenge"
      (is (html/select 
           recaptcha-html 
           [(html/attr= :name :ecaptcha_challenge_field)])))))
