(ns pedestal-tools.test-transforms
  (:import [i18n.core i18nConfig])
  (:require [pedestal-tools.transforms :as t]
            [net.cgrand.enlive-html :as html]
            [pedestal-tools.utils :refer [refer-private]]
            [i18n.core :as i18n]
            [clojure.test :refer :all]))

(refer-private 'pedestal-tools.transforms)

(def source
  (reify
    i18n/IMessageSource
    (get-messages [this locale]
      {:username-is-too-short "username is too short"})))

(def message-config
  (reify
    i18n/IMessageConfig
    (default-locale [this] :en)
    (message-source [this] source)))

(deftest test-include-js
  (testing "script tag is added"
    (is (= (t/include-js "http://example.com/foo.js")
           {:tag :script :attrs {:src "http://example.com/foo.js"
                                 :type "text/javascript"}
            :content '()}))))

(deftest test-include-css
  (testing "css tag is added"
    (is (= (t/include-css "http://example.com/foo.css")
           {:tag :link
            :attrs {:href "http://example.com/foo.css"
                    :rel  "stylesheet"
                    :type "text/css"}
            :content '()}))))

(deftest test-include-inline-css
  (testing "inline script tag adds inline script"
    (is (= (t/include-inline-js "alert('hello');")
           {:tag :script
            :attrs {:type "text/javascript"}
            :content ["alert('hello');"]}))))

(deftest test-template-path
  (is (or (= (t/template-path "testing-something")
             "templates/user/testing_something.html")
        (= (t/template-path "testing-something")
         "templates/pedestal_tools/test_transforms/testing_something.html"))))

(t/defview foo [])

(t/defview foo-bar [bar]
  [:div] (html/substitute (html/html [:span bar])))

(defn- view-content
  [view & args]
  (-> (apply view args)
      first :content first :content first))

(deftest test-view
  (testing "correct template is used"
    (is (= (view-content foo)
           {:tag :div :attrs {:class "test-class"}
            :content ["hello"]})))
  (testing "parameters are passed"
    (is (= (view-content foo-bar "foo")
           {:tag :span :attrs {}
            :content ["foo"]}))))

(deftest test-values-are-populated-in-checkbox-group
  (with-redefs [t/form-value (fn [_ v] v)]
    (= (html/sniptest
        "<input type=\"checkbox\" name=\"role\" />"
        (t/checkbox-group {} [{:value "admin" :text "Administrator"}
                              {:value "user" :text "User"}]))
       (str "<label class=\"checkbox\">"
            "<input value=\"admin\" name=\"role\" type=\"checkbox\" />"
            "Administrator</label>"
            "<label class=\"checkbox\">"
            "<input value=\"user\" name=\"role\" type=\"checkbox\" />"
            "User</label>"))))

(deftest test-multiple-values-are-selected-in-checkbox-group
  (is (= (html/sniptest
          "<input type=\"checkbox\" name=\"role\" />"
          (t/checkbox-group {:role '("guest" "admin")}
                            [{:value "admin" :text "Administrator"}
                             {:value "user" :text "User"}
                             {:value "guest" :text "Guest"}]))
         (str "<label class=\"checkbox\">"
              "<input value=\"admin\" checked=\"checked\" "
              "name=\"role\" type=\"checkbox\" />"
              "Administrator</label>"
              "<label class=\"checkbox\">"
              "<input value=\"user\" name=\"role\" type=\"checkbox\" />"
              "User</label>"
              "<label class=\"checkbox\">"
              "<input value=\"guest\" checked=\"checked\" "
              "name=\"role\" type=\"checkbox\" />Guest</label>"))))

(deftest test-single-value-is-selected-in-checkbox-group
  (is (= (html/sniptest
          "<input type=\"checkbox\" name=\"role\" />"
          (t/checkbox-group {:role "admin"}
                            [{:value "admin" :text "Administrator"}
                             {:value "user" :text "User"}]))
         (str "<label class=\"checkbox\">"
              "<input value=\"admin\" checked=\"checked\" "
              "name=\"role\" type=\"checkbox\" />"
              "Administrator</label>"
              "<label class=\"checkbox\">"
              "<input value=\"user\" name=\"role\" "
              "type=\"checkbox\" />User</label>"))))
  
(deftest select-is-populated
  (is (= (html/sniptest
          "<select name=\"role\"><option></option></select>"
          (t/select-options {}
                            [{:value "admin" :text "Administrator"}
                             {:value "user" :text "User"}]))
         (str "<select name=\"role\">"
              "<option value=\"admin\">Administrator</option>"
              "<option value=\"user\">User</option>"
              "</select>"))))

(deftest value-is-selected-in-select
  (is (= (html/sniptest
                "<select name=\"role\"><option></option></select>"
                (t/select-options {:role "admin"}
                                  [{:value "admin" :text "Administrator"}
                                   {:value "user" :text "User"}]))
         (str "<select name=\"role\">"
              "<option value=\"admin\" selected=\"selected\">"
              "Administrator</option>"
              "<option value=\"user\">User</option>"
              "</select>"))))

(deftest test-form
  (testing "error class is set on a form-group if present"
    (is (= (html/sniptest
            (str "<form><div class=\"form-group\" "
                 "data-field=\"username\"></div></form>")
            [:form] (t/form {:errors 
                             {:username :username-is-too-short}}
                            :foo))
           (str "<form action=\"foo\">"
                "<div class=\"has-error form-group\">"
                "<span class=\"help-block\">username is too short</span>"
                "</div></form>"))))
  (testing "no class is added in case of no error"
    (is (= (html/sniptest
            (str "<form><div class=\"form-group\" "
                 "data-field=\"username\"></div></form>")
            [:form] (t/form {} :foo))
           (str "<form action=\"foo\">"
                "<div class=\"form-group\"></div></form>")))))

(deftest input-tags-are-populated
  (is (= (html/sniptest
          "<form><input type='text' name='username'/></form>"
          [:form] (t/form {:username "john"}
                          :action))
         (str "<form action=\"action\"><input value=\"john\" "
              "name=\"username\" type=\"text\" /></form>"))))

(deftest test-checkbox
  (testing "checkbox is not set when value is not present"
    (is (= (html/sniptest
            "<form><input type=\"checkbox\" name=\"agree\"/></form>"
            [:form] (t/form {} :action))
           (str "<form action=\"action\">"
                "<input name=\"agree\" type=\"checkbox\" />"
                "</form>"))))
  (testing "checkbox is not set when value is not set"
    (is (= (html/sniptest
            "<form><input type=\"checkbox\" name=\"agree\"/></form>"
            [:form] (t/form {:agree false} :action))
           (str "<form action=\"action\">"
                "<input name=\"agree\" type=\"checkbox\" />"
                "</form>"))))
  (testing "checkbox is set when value is set"
    (is (= (html/sniptest
            (str "<form><input type=\"checkbox\" name=\"agree\" "
                 "value=\"yes\"/></form>")
            [:form] (t/form {:agree "yes"} :action))
           (str "<form action=\"action\">"
                "<input checked=\"checked\" value=\"yes\" "
                "name=\"agree\" type=\"checkbox\" />"
                "</form>")))))

(deftest test-radio
  (is (= (html/sniptest
          (str "<form><input value=\"male\" name=\"gender\" "
               "type=\"radio\" />Male<br/>"
               "<input value=\"female\" name=\"gender\" "
               "type=\"radio\" />Female<br/></form>")
          [:form] (t/form {:gender "male"} :action))
         (str "<form action=\"action\">"
              "<input selected=\"selected\" type=\"radio\" "
              "name=\"gender\" value=\"male\" />Male<br />"
              "<input type=\"radio\" name=\"gender\" "
              "value=\"female\" />Female<br />"
              "</form>"))))

(deftest test-form-errors
  (testing "errors are displayed in .errors tag if present"
    (is (= (html/sniptest 
            (str "<form><div class=\"errors\"><div class=\"error\">"
                 "</div></div></form>")
            [:form] (form-errors 
                     {:errors 
                      {:composite '(:username-is-too-short)}}))
           (str "<form><div class=\"errors\">"
                "<div class=\"error\">username is too short"
                "</div></div></form>"))))
  (testing "errors tag is not displayed if there are no errors"
    (is (= (html/sniptest
            (str "<form><div class=\"errors\"><div class=\"error\">"
                 "</div></div></form>")
            [:form] (form-errors {}))
           (str "<form></form>")))))

(use-fixtures :once 
  (fn [f]
    (binding [i18n/*i18n-config* (i18nConfig. message-config :en)]
      (with-redefs [pedestal-tools.transforms/url-for 
                    (fn [key & args]
                      (name key))]
        (f)))))
