(defproject xeric/clojure-libs "0.2.1-SNAPSHOT"
  :description "Xeric Clojure[Script] libraries"
  :packaging "pom"
  :plugins [[lein-modules "0.3.9"]]
  :profiles {:provided {:dependencies [[org.clojure/clojure "_"]]}}
  :modules {:versions {org.clojure/clojure "1.7.0-RC1"
                       org.clojure/core.async "0.1.346.0-17112a-alpha"
                       com.cemerick/clojurescript.test "0.3.3"
                       org.omcljs/om "0.8.8"
                       cljsjs/react-with-addons "0.13.3-0"
                       enlive "1.1.5"
                       io.pedestal "0.3.1"
                       com.novemberain/monger "3.0.0-rc2"
                       prismatic/schema "1.0.3"
                       org.clojure/clojurescript "0.0-3308"}
            :inherited 
            {:url "http://bitbucket.org/xeric/clojure-libs"
             :license "Xeric Corporation, all rights reserved"
             :plugins [[lein-cljsbuild "1.0.5"]
                       [com.cemerick/clojurescript.test "0.3.3"]]
             
             :repositories
             [["releases" 
               {:url "http://apps.xeric.net/nexus/content/repositories/releases"
                :username :env/xeric_repo_username
                :password :env/xeric_repo_password}]
              ["snapshots" 
               {:url "http://apps.xeric.net/nexus/content/repositories/snapshots"
                :username :env/xeric_repo_username :checksum :ignore
                :password :env/xeric_repo_password}]]}})


