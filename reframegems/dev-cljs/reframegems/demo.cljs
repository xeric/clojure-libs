(ns reframegems.demo
  (:require [reagent.core :as reagent]
            [re-frame.core :refer [dispatch subscribe]
             :as re-frame]
            [reframegems.form :as form]
            [reframegems.events :as events]
            [reframegems.router :as r]
            [reframegems.subs]
            [reframegems.translations :refer [t]]
            [cljs-react-material-ui.reagent :as rui]
            [cljs-react-material-ui.core :as ui]
            [cljs-react-material-ui.icons :as icons]))

(def form-fields
  [{:key :name :type :text :label "Name"}
   {:key :address :type :textarea :label (t :address) :rows 3}
   {:key :gender :type :radio-group :label (t :gender) :options ["Yes" "No"]}
   {:key :angular? :type :toggle :label "Have you ever used angular ?"}
   {:key :colors :type :select :label "Your favourite color ?"
    :options ["Red" "Blue" "Green"]}
   {:key :games :type :multi-select :label "Your favourite game ?"
    :options ["Cricket" "Hockey" "Football"]}
   {:key :languages :type :checkbox-group :label "Languages you know"
    :options ["English" "French" "German"]}
   {:key :movies :type :multi-text :label "Favourite Movies"}    
   {:key :dob :type :date-field :label "Date Of Birth"}])

(enable-console-print!)

(def mui-theme {})

(defn home
  []
  (let [form-data (subscribe [:form/state])]
    (fn []
      [:div {:class "container"}
       [rui/raised-button
        {:label "Go To Page #2"
         :primary true
         :on-touch-tap #(r/go :routes/page2)}]
       [form/form form-fields :demo]
       [:pre (pr-str @form-data)]])))

(defn page2
  []
  [:div
   [rui/raised-button
    {:label "Go back to home page"
     :secondary true
     :on-touch-tap #(r/go :routes/home)}]])

(def panels
  {:routes/home home
   :routes/page2 page2})

(defn app
  []
  (let [active-panel (subscribe [:app/active-panel])
        domain       (subscribe [:app/domain])]
    (fn []
      (when @active-panel
        (prn @domain)
        [rui/mui-theme-provider
         {:mui-theme (ui/get-mui-theme mui-theme)}
         [:div
          [(panels @active-panel)]]]))))

(def routes
  ["" [["/" :routes/home]
       ["/page2" :routes/page2]
       [true :routes/home]]])
  
(re-frame/reg-event-db
 :routes/home
 (fn [db [handler]]
   (events/set-active-panel db handler)))

(re-frame/reg-event-db
 :routes/page2
 (fn [db [handler]]
   (events/set-active-panel db handler)))

(defn main
  []
  (when-let [el (.. js/document (getElementById "app"))]
    (re-frame/dispatch [:app/fetch-routes nil routes])
    (reagent/render-component [app] el)))

(main)
