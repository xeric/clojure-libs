(ns reframegems.events
  (:require [clojure.walk]
            [clojure.string :as str]
            [bidi.bidi :as bidi]
            [ajax.edn :as ajax-edn]
            [ajax.core :as ajax]
            [ajax.protocols :as ajax-protocols]
            [cljs.core.async :refer [<! put!]]
            [reframegems.router :as r]
            [reframegems.storage :as storage]
            [reframegems.translations :refer [t]]
            [goog.net.cookies :as cookies]
            [day8.re-frame.http-fx]
            [re-frame.db :as re-frame-db]
            [re-frame.core :as re-frame
             :refer [subscribe dispatch reg-sub-raw reg-event-db
                     reg-event-fx]]
            [cljs-react-material-ui.reagent :as rui]))

(reg-event-db
 :form/set-value
 (fn [db [handler form-key key value]]
   (assoc-in db [:form/state form-key key] value)))

(reg-event-db
 :form/set-error
 (fn [db [handler form-key key value]]
   (assoc-in db [:form/state form-key :errors key] value)))

(defn remove-cookie
  [& keys]
  (doseq [key keys]
    (.remove goog.net.cookies (name key) "/")))

(defn set-cookie
  [key value]
  (.set goog.net.cookies (name key) value -1 "/"))

(defn transform-reg-exp
  [routes]
  (clojure.walk/prewalk (fn [x]
                          (if (and (string? x)
                                   (= (.indexOf x "__reg_") 0))
                            (re-pattern (.substring x 6))
                            x)) routes))

(def reset-form
  (re-frame/->interceptor
   :id :reset-form
   :before (fn [context]
             (assoc-in context [:coeffects :db :form/state] {}))))

(re-frame/reg-fx
 :route
 (fn [params]
   (apply r/go params)))

(re-frame/reg-fx
 :start-history
 (fn [routes]
   (r/app-routes routes)))

(re-frame/reg-fx
 :storage
 (fn [m]
   (doseq [[key value] m]
     (cond
       (= key :clear) (storage/clear!)
       (nil? value)   (storage/remove! key)
       (fn? value)    (storage/update key value)
       :else          (storage/set key value)))))

(defn set-db
  [db [_ & key-values]]
  (let [db (->> (apply hash-map key-values)
                (reduce (fn [db [key value]]
                          (-> (assoc-in db (if-not (vector? key)
                                             [key] key) value))) db))]
    db))

(reg-event-db :app/assoc set-db)

(def stop-loading-interceptor
  (ajax/to-interceptor
   {:name "Stop loading interceptor"
    :request
    (fn [req] (dispatch [:app/assoc :app/loading? true]) req)
    :response
    (fn [res] (dispatch [:app/assoc :app/loading? false]) res)}))

(defn xhrio
  [method uri params]
  (if (vector? params)
    {:dispatch params}
    {:http-xhrio
     (merge {:method          method
             :uri             uri
             :interceptors    [stop-loading-interceptor]
             :format          (ajax-edn/edn-request-format)
             :response-format (ajax-edn/edn-response-format)}
            params)}))

(defn current-domain
  []
  (let [location js/window.location]
    (str (aget location "protocol") "//"
         (aget location "host"))))

(reg-event-fx
 :app/fetch-routes
 (fn [{:keys [db]} [handler route-path client-routes]]
   (if (nil? route-path)
     {:start-history client-routes
      :db (assoc db :app/domain (current-domain))}
     (xhrio :get (str route-path)
            {:on-success [:app/set-routes client-routes]
             :on-failure [:ajax/general-failure]}))))

(reg-event-fx
 :app/set-routes
 (fn [{:keys [db]} [handler client-routes result]]
   (let [routes (transform-reg-exp (:routes result))
         domain (current-domain)]
     {:db (assoc db :app/routes routes :app/domain domain)
      :start-history client-routes})))

(defn set-active-panel
  [db handler]
  (assoc db :app/active-panel handler))

(defn set-form-errors
  [db form-key errors]
  (assoc-in db [:form/state form-key :errors] errors))

(defn set-form-state
  [db form-key state]
  (assoc-in db [:form/state form-key] state))

(defn translate-messages [errors]
  (into {}
        (map (fn [[k v]]
               (cond
                 (and (list? v) (= (nth (second v) 2) 'nil))
                 [k (t :missing-required-key)]
                 (or (keyword? v) (vector? v)) [k (t v)]
                 (symbol? v) [k (t (name v))]
                 :else [k v]))
             errors)))

(defn path-for
  [db & args]
  (if (:app/routes db)
    (str (:app/domain db) (apply bidi/path-for (:app/routes db) args))
    (throw (ex-info (str "Routes not loaded for " (pr-str args)
                         " in " (pr-str (:app/routes db))) {}))))

(reg-event-fx
 :ajax/general-failure
 (fn [{:keys [db]} [_ form-key result]]
   (let [{:keys [status response original-text]} (or result form-key)
         form-key (if result form-key)
         response (or (:errors response) response)]
     (case status
       401 {:route [:routes/login]}
       403 {:route [:routes/login]}
       400 (if form-key
             {:db (set-form-errors db form-key
                                   (translate-messages response))}
             {:db db})
       (do
         (prn original-text)
         {:db db})))))

(defn form-state
  [db key]
  (dissoc (get-in db [:form/state key]) :errors))
