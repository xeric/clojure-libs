(ns reframegems.form
  (:require [reagent.core :as reagent]
            [re-frame.core :refer [subscribe dispatch]]
            [cljs.core.match :refer-macros [match]]
            [cljs-react-material-ui.reagent :as rui]
            [cljs-react-material-ui.core :as ui]
            [cljs-react-material-ui.icons :as icons]
            [cljsjs.material-ui-chip-input]
            [clojure.string :as string]
            [cljsjs.react-select]
            [reframegems.utils :as utils]
            [reframegems.translations :refer [t]]))

(defn disable-field
  [field]
  (assoc field :disabled? true :help nil :full-width true))

(defn material-ui-field
  [{:keys [key rows args type disabled? label] :as field}]
  (let [type (keyword type)]
    (-> (assoc field
               :type  type
               :multi? (or (some? rows) (= type :textarea))
               :disabled disabled?
               :label  label)
        (update :key keyword)
        (update :args #(merge %1 (select-keys field [:rows])))
        (assoc-in [:args :full-width] true))))

(defn component->form
  [component & {:keys [disabled?]}]
  (let [field-modifier (if disabled?
                         (comp material-ui-field disable-field)
                         material-ui-field)]
    (->> component
         :fields
         (map field-modifier)
         (remove #(= (:type %1) :none)))))

(defmulti transform-field (fn [f] (prn f) (keyword (:type f))))

(defmethod transform-field :default [f] f)

(defn to-date [d]
  (letfn [(days [ms]
            (* 24 60 60 1000 ms))]    
    (let [now (js/Date.)
          today (js/Date. (.getFullYear now)
                          (.getMonth now)
                          (.getDate now))]
      (match d
             [a b c] (js/Date. a b c)
             [:today num] (js/Date. (+ (.getTime today) (days num)))
             [:today] today))))

(defmethod transform-field :date
  [{:keys [date-min date-max] :as field}]
  (cond-> field
      date-min (update :date-min to-date)
      date-max (update :date-max to-date)))

(defn transform-fields
  [app]
  (if (:fields app)
    (update app :fields (partial map transform-fields))
    (transform-field app)))

(defn raised-button
  [attrs]
  (let [loading? (subscribe [:app/loading?])]
    (fn [attrs]
      [rui/raised-button
       (-> (assoc attrs :disabled @loading?)
           (update :class str " btn"))])))

(defmulti render-field (fn [field value error] (:type field)))

(defn floating-label [{:keys [label title]}]
  (when (string/blank? title)
    label))

(defn field-value
  ([v] (field-value v identity))
  ([v value-fn]
   (cond
     (string? v) v
     (map? v)  (value-fn v)
     (coll? v) (mapv #(or (value-fn %1) %1) v)
     :else     v)))

(defn find-field
  [fields fkey]
  (let [field (first fields)]
    (if (or (nil? field) (= (keyword (:key field)) fkey))
      field
      (recur (concat (:fields field) (rest fields)) fkey))))

(defn visible?
  [fields form-state visible-if]
  (if (nil? visible-if)
    true
    (let [[fkey fval] (if (vector? visible-if) visible-if [visible-if])
          fkey        (keyword fkey)
          value       (get form-state fkey)]
      (and value
           (or (nil? fval) (= fval value))
           (visible? fields form-state (:visible-if (find-field fields fkey)))))))

(defn year-since
  [d]
  (range d (+ 1900 (.getYear (js/Date.)))))

(defn match-builtin-lists
  [options]
  (match options
         ["year-since" d] (year-since d)
         ["year-since-desc" d] (vec (reverse (year-since d)))
         ["range" begin end] (vec (range begin end))
         ["range" begin end inc] (vec (range begin end inc))
         :else nil))

(defn make-option [option]
  (cond
    (string? option)
    {:value option :primary-text option}

    (int? option)
    {:value option :primary-text (str option)}
    
    (vector? option)
    {:value (first option) :primary-text (second option)}
    
    :else
    (let [{:keys [id text]} option]
      (if (and id text)
        (assoc (dissoc option :id :text)
               :value id :primary-text text)
        option))))

(defn compatibility-options-modifier
  [options]
  (if (satisfies? IDeref options)
    options
    (atom
     (mapv make-option
           (if-let [l (match-builtin-lists options)]
             l
             options)))))

(defn get-options
  [options form-lists]
  (compatibility-options-modifier
   (if (or (keyword? options) (string? options))
     (get form-lists (keyword options))
     options)))

(defn form
  [fields form-key & {:keys [disabled?]}]
  (let [form-state (subscribe [:form/state])
        form-lists (subscribe [:form/lists])
        loading?   (subscribe [:app/loading?])]
    (fn [fields form-key & {:keys [disabled?]}]
      [:div {:class "form"}
       (when (seq (:errors (get @form-state form-key)))
         [:p {:class "error form-error"} (t :errors-in-form)])
       (doall
        (for [{:keys [options visible-if key help title] :as field} fields
              :let [val     (field-value
                             (get-in @form-state [form-key key])
                             (or (:value-fn field) identity))
                    error   (get-in @form-state [form-key :errors key])
                    show?   (visible? fields (get @form-state form-key) visible-if)
                    change  #(dispatch [:app/assoc [:form/state form-key key] %1])]]
          ^{:key (name (str (:key field) "-wrapper"))}
          [:div {:class (str "form-element-wrapper" (if error " error"))
                 :style (utils/show show?)}
           (if (= (:type field) :container)
             [:div {:class "container"}
              [form (component->form field :disabled? disabled?)
               form-key :full-width? true]]
             [:div
              (when title
                [:div {:class "form-element-title"} title])
              [render-field
               (cond-> (assoc field :val val :error error :change change)
                 (or disabled? @loading? (:disabled? field))
                 (assoc :disabled? true)
                 
                 (some? options) (assoc :options (get-options options @form-lists)))]
              (when help
                [:div {:class "form-element-help"} help])])]))])))

(defn read-only-form
  [fields form-state]
  [:div {:class "form"} 
   (doall
    (for [field fields]
      ^{:key (name (:key field))}
      [render-field
       (assoc field :disabled? true
              :val (field-value
                    (get form-state (:key field))
                    (or (:value-fn field) identity))
              :change (fn []))]))])

(defn el-value [x]  (.-value (.-target x)))

(def full-width? true)

(defn field-defaults
  [{:keys [label value-fn multi? val error change
           disabled? read-only? type help icon key args] :as field}]
  (-> (dissoc args :key :dynamic? :disabled? :multi? :val :full-width?)
      (assoc
       :name (name key)
       :full-width (and full-width? (not (:auto-width? field)))
       :class (str "form-element " (name key))
       :multiLine (boolean (or multi? (:rows args)))
       :disabled (boolean disabled?)
       :default-value val
       :type (name type)
       :hint-text label
       :floating-label-text (floating-label field)
       :error-text error
       :on-change #(change %2))))

(defmethod render-field :default
  [field]
  (let [editing-myself (reagent/atom nil)]
    (fn [{:keys [key rows val dynamic? args] :as field}]
      (let [field (cond-> (field-defaults field)
                     dynamic?
                     (assoc :on-blur #(reset! editing-myself nil)
                            :on-focus #(reset! editing-myself "true")))
            key (if dynamic? (or @editing-myself val) "")]
        [:div ^{:key key} [rui/text-field field]]))))

(defmethod render-field :zipcode
  [{:keys [change keys key] :as field}]
  [rui/text-field (assoc (field-defaults (dissoc field :keys))
                         :on-change
                         (fn [e v]
                           (change v)
                           (when (= (count v) 5)
                             (dispatch [:zipcode/get-city v
                                        (assoc keys :key key)]))))])

(defmethod render-field :toggle
  [{:keys [label help key disabled? read-only?
           args val change]}]
  [rui/toggle
   (assoc (dissoc args :full-width)
          :name (name key)
          :class "toggle"
          :label label
          :disabled (boolean disabled?)
          :default-toggled  val
          :on-toggle #(change %2))])

(defn html-label
  [label]
  (reagent/as-element
   [:span {:dangerouslySetInnerHTML {:__html label}}]))

(defmethod render-field :section
  [{:keys [text class tag]}]
  [(or tag :div)
   {:class (str "section " class)
    :dangerouslySetInnerHTML {:__html text}}])

(defmethod render-field :checkbox
  [{:keys [label help key disabled? read-only?
           args val change error]}]
  [:div
   [rui/checkbox
    (assoc (dissoc args :full-width)
           :name (name key)
           :class "toggle"
           :label (html-label label)
           :disabled (boolean disabled?)
           :checked  val
           :on-check #(change %2))]
   (when error
     [:div {:class "error"} error])])

(defmethod render-field :agreement
  [field]
  (render-field (assoc field :type :checkbox)))

(defmethod render-field :select
  [{:keys [val options disabled? title multi? renderer key value-fn change]
    :or {value-fn :value}
    :as field}]
  [:div {:style {:position "relative"}}
   (when-not disabled?
     [rui/icon-button {:style (utils/show (some? val))
                       :class (str "select-clear "
                                   (if (nil? title) "select-clear-offset"))
                       :on-touch-tap #(change nil)}
      (icons/content-backspace)])
   [rui/select-field
    (-> (field-defaults field)
        (dissoc :default-value)
        (assoc :value val :on-change #(change %3)))
    (doall
     (for [option (if (satisfies? IDeref options) @options options)]
       (if renderer
         ^{:key (value-fn option)}
         (renderer option)
         ^{:key (value-fn option)}
         [rui/menu-item option])))]])

(defmethod render-field :radio-group
  [{:keys [val disabled? label options
           key value-fn error change]
    :or {value-fn :value}
    :as field}]
  [:div {:class "radio-group-wrapper"}
   [:label {:class "radio-group-label"} label]
   [rui/radio-button-group
    {:name (name key)
     :class (str "radio-group form-element " (name key))
     :on-change #(change %2)
     :default-selected val}
    (doall
     (for [option (if (satisfies? IDeref options) @options options)
           :let [v (value-fn option)]]
       ^{:key v}
       [rui/radio-button
        {:key v
         :value v
         :label (:primary-text option)
         :disabled disabled?}
        (:label option)]))]
   (when error
     [:div {:class "error"} error])])

(defmethod render-field :checkbox-group
  [{:keys [val disabled? label options
           key value-fn change error]
    :or {value-fn :value}
    :as field}]
  [:div {:class "checkbox-group-wrapper"}
   [:label {:class "checkbox-group-label"} label]
   [:div
    (doall
     (for [option (if (satisfies? IDeref options) @options options)
           :let [v (value-fn option)]]
       ^{:key v}
       [rui/checkbox
        {:value v
         :key v
         :label (:primary-text option)
         :default-checked (contains? (set val) v)
         :on-check #(change ((if %2 conj disj) (set val) v))
         :disabled disabled?}
        (:label option)]))
    (when error
     [:div {:class "error help"} error])]])

(defmethod render-field :auto-complete
  [{:keys [options label error disabled? label
           multi? renderer key val change value-fn] :as field}]
  (let [change (fn [i] (change (if i (:value (nth @options i)))))
        data-source (mapv (fn [option]
                            {:text  (or (:text option) (:title option))
                             :value (reagent/as-element (partial renderer option))})
                          @options)]
    [:div {:style {:position "relative"}}
     [rui/icon-button {:style (utils/show (some? val))
                      :class "select-clear" :on-touch-tap #(change nil)}
      (icons/navigation-close)]
     [rui/auto-complete
      {:full-width (and full-width? (not (:auto-width? field)))
       :class "form-element"
       :disabled (boolean disabled?)
       :label label
       :name (name key)
       :filter  (fn [search-key val]
                  (>= (.indexOf val search-key) 0))
       :floating-label-text (floating-label field)
       :hint-text label
       :error-text error
       :open-on-focus true
       :on-new-request #(change %2)
       :dataSource (clj->js data-source)}]]))

(defmethod render-field :time-field
  [{:keys [label value-fn disabled? type help icon key
           args val error change] :as field}]
  [rui/time-picker
   (assoc args
          :format "24hr"
          :locale "en-US"
          :label label
          :type type
          :name (name key)
          :full-width full-width?
          :default-time val
          :disabled (boolean disabled?)
          :hint-text label
          :floating-label-text (floating-label field)
          :error-text error
          :on-change #(change %2))])

(defmethod render-field :date-field
  [{:keys [label value-fn disabled? val error change
           type icon key help args] :as field}]
  [rui/date-picker
   (assoc args
          :locale "en-US"
          :class (str "form-element")
          :floating-label-text (floating-label field)
          :hint-text label
          :type type
          :name (name key)
          :full-width full-width?
          :value val
          :auto-ok true
          :disabled (boolean disabled?)
          :error-text error
          :on-change #(change %2))])

(def select2 (reagent/adapt-react-class js/Select))

(defn select2-data
  [l]
  (mapv (fn [{:keys [id label text title] :as e}]
          (if (:id e)
            {:value id :type label :label (or title text)}
            {:value (:value e) :label (:primary-text e)})) l))

(defmethod render-field :multi-select
  [{:keys [label value-fn options args val
           change error id key disabled?] :as field
    :or {value-fn identity}} state form-key]
  [:div {:class (str "select2 " (name key))}
   [:label label]
   [select2 (merge
             args
             {:options (clj->js (select2-data @options))
              :value   (clj->js val)
              :multi true
              :width "100%"
              :disabled (boolean disabled?)
              :onChange
              (fn [e]
                (let [v (js->clj e :keywordize-keys true)
                      r (mapv :value v)]
                  (change r)))})]
   (if error [:div {:class "error-text"} error])])

(def chip-input (reagent/adapt-react-class js/MaterialUIChipInput))

(defmethod render-field :multi-text
  [{:keys [label value-fn options args val
           change error id key disabled?] :as field
    :or {value-fn identity}} state form-key]
  [:div {:class (str "multi-text " (name key))}
   [:label label]
   [chip-input (merge
                args
                {:default-value (clj->js val)
                 :on-change #(change (js->clj %1))
                 :full-width full-width?
                 :full-width-input true
                 :error-text error
                 :floating-label-text (floating-label field)
                 :disabled (boolean disabled?)})]])

(defn date-filter
  [state]
  [:div {:class "column"}
   [rui/date-picker
    {:value  (:start-date @state)
     :label  (t :start-date)
     :floating-label-text (t :start-date)
     :full-width full-width?
     :auto-ok true
     :onChange  #(swap! state assoc :start-date %2)}]
   [rui/date-picker
    {:value  (:end-date @state)
     :label  (t :end-date)
     :floating-label-text (t :end-date)
     :full-width full-width?
     :auto-ok true
     :onChange  #(swap! state assoc :end-date %2)}]])
