(ns reframegems.router
  (:import [goog History]
           [goog.Uri QueryData])
  (:require [clojure.string :as string]
            [bidi.bidi :as bidi]
            [re-frame.core :as re-frame]
            [pushy.core :as pushy]
            [goog.dom :as gdom]
            [goog.dom.classlist :as classlist]
            [goog.events :as events]
            [goog.history.EventType :as EventType]))

(def history (atom nil))
(def routes (atom nil))

(defn set-routes! [rs]
  (reset! routes rs))

(defn get-routes []
  (if (nil? @routes)
    (throw (ex-info "Client routes not set." {}))
    @routes))

(defn dispatch-route
  [{:keys [handler route-params] :as matched-route}]
  (re-frame/dispatch [:app/assoc :params route-params])
  (re-frame/dispatch [handler route-params]))

(defn match-route
  [route]
  (bidi/match-route (get-routes) route))

(defn app-routes
  [rs]
  (reset! routes rs)
  (reset! history (pushy/pushy dispatch-route match-route))
  (pushy/start! @history))

(defn path-for
  [& params]
  (if-let [path (apply bidi/path-for (get-routes) params)]
    path
    (throw (ex-info (str "Route not found " params) {}))))

(defn go
  [& params]
  (let [p (apply path-for params)
        path (.-pathname js/location)
        path (if (nil? path) "/" path)]
    (if (= path p)
      (re-frame/dispatch (vec params))
      (pushy/set-token! @history p))))
