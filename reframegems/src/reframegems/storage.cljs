(ns reframegems.storage
  (:refer-clojure :exclude [get set update])
  (:import [goog.storage Storage]
           [goog.storage.mechanism HTML5LocalStorage])
  (:require [cljs.reader :as reader]))

(def mechanism (HTML5LocalStorage.))

(def storage mechanism)

(defn get [key]
  (if-let [n (.get storage (name key))]
    (reader/read-string n)))

(defn find-one [key id]
  (first (filter #(= (str (:id %)) (str id)) (get key))))

(defn set
  ([key-values]
   (doseq [[key value] key-values]
     (set key value)))
  ([key value]
   (.set storage (name key) (pr-str value))))

(defn remove! [key]
  (.remove storage (name key)))

(defn clear! []
  (.clear mechanism))

(defn valid? []
  (some? (get :principal)))

(defn update
  [key update-fn]
  (set key (update-fn (get key))))

