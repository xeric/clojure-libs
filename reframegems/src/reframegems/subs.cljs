(ns reframegems.subs
  (:require [reagent.ratom :refer-macros [reaction]]
            [re-frame.core :as re-frame :refer [reg-sub-raw]]))

(defn create-reaction [key]
  (fn [db]
    (reaction (get @db key))))

(defn register-key-sub [& keys]
  (doseq [key keys]
    (reg-sub-raw key (create-reaction key))))

(reg-sub-raw
 :form/state
 (fn [db [_ key]]
   (reaction (if (some? key)
               (get-in @db [:form/state key])
               (:form/state @db)))))

(register-key-sub
 :app/loading?
 :form/lists
 :app/active-panel
 :app/domain)
