(ns reframegems.translations
  (:require [taoensso.tower :as tower :include-macros true]
            [goog.string :as gstring]
            [goog.string.format :as gformat]))

(def ^:private tconfig
  {:fallback-locale :en
   :dev-mode? true
   :compiled-dictionary (tower/dict-compile "translations/dict.clj")})

(def t* (partial (tower/make-t tconfig) :en))

(defn t [format]
  (let [[format & args] (if (coll? format) format [format])]
    (if-let [s (t* (keyword format))]
      (if (nil? args)
        s
        (apply gstring/format s args)))))
