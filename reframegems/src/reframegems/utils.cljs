(ns reframegems.utils
  (:require [cljs.core.async :refer [sub chan pub put! unsub]]
            [clojure.string :as string]
            [cljs.reader]
            [cljs-react-material-ui.reagent :as rui]
            [cljs.core.match :refer-macros [match]]
            [re-frame.core :refer [subscribe]])
  (:import [goog Uri]))

(def space " ")

(def icon-menu-style
  {:display "block" :position "absolute" :top 0 :right 4})

(def cell-1-by-1
  "mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone")

(def cell-1-by-2
  "mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone")

(def cell-1-by-3
  "mdl-cell mdl-cell--4-col mdl-cell--2-col-tablet mdl-cell--1-col-phone")

(def cell-1-by-4
  "mdl-cell mdl-cell--3-col mdl-cell--2-col-tablet mdl-cell--1-col-phone")

(def cell-2-by-3
  "mdl-cell mdl-cell--8-col mdl-cell--6-col-tablet mdl-cell--3-phone")

(defn disable-field
  [field]
  (assoc field :disabled? true :help nil :full-width true))

(defn ->date-string
  [date]
  (when date
    (.toLocaleDateString date)))

(defn icon-menu-btn
  [icon]
  (js/React.createElement
   (aget js/MaterialUI "IconButton")
   #js {:key 1 :iconStyle {:fill "white"}} icon))


(defn show
  ([show?] (show show? "block"))
  ([show? display]
   (if show?
     {:display display}
     {:display "none"})))

(defn scroll-to-top []
  (js/window.scroll 0 0))

(defn as-html
  [tag text]
  [tag {:dangerouslySetInnerHTML {:__html text}}])
  
