(ns servergems.client
  (:require  [servergems.server :as server]
             [bidi.bidi :as bidi]
             [clj-http.client :as client])
  (:import [java.util Date]))

(def state (atom {:routes nil :cookies nil}))

(defn reset-cookies []
  (swap! state dissoc :cookies))

(defn path-for [& args]
  (let [path (if (string? (first args))
               (first args)
               (apply bidi/path-for (:routes @state) args))]
    (if (nil? path)
      (throw (ex-info (str "No path found for route: " args) {}))
      (str "http://localhost:9999/" path))))

(defn not-found? [res]
  (= (:status res) 404))

(defn ok? [res]
  (= (:status res) 200))

(defn created? [res]
  (= (:status res) 201))

(defn no-content? [res]
  (= (:status res) 204))

(defn malformed? [res]
  (= (:status res) 400))

(defn forbidden? [res]
  (= (:status res) 403))

(defn unauthorized? [res]
  (= (:status res) 401))

(defn public-request
  [] 
  {:throw-exceptions false
   :as :clojure
   :content-type "application/edn"})

(defn resource-request
  []
  {:throw-exceptions false
   :content-type "text/html"})

(defn auth-request
  []
  (-> (public-request)
      (assoc :cookies (:cookies @state))))

(defn edn
  [req params]
  (assoc req :body (pr-str params)))

(defn edn-with-multipart
  [req params multipart]
  (-> (edn req params)
      (assoc :multipart multipart)
      (dissoc :content-type :as)))

(defn transform-reg-exp
  [routes]
  (clojure.walk/prewalk (fn [x]
                          (if (and (string? x)
                                   (= (.indexOf x "__reg_") 0))
                            (re-pattern (.substring x 6))
                            x)) (:routes routes)))

(defn response-body
  [res]
  (if (or (ok? res) (created? res))
    (:body res)
    (throw (ex-info (str "Unsuccessful response " (pr-str res)) {}))))

(defn set-routes!
  []
  (swap! state
         assoc :routes
         (transform-reg-exp
          (response-body
           (client/get (path-for "/rest/routes")
                       (public-request))))))

(defn routes-fixture
  [f]
  (try
    (set-routes!)
    (f)
    (catch Exception _))
  (reset! state {}))

(defn login!
  ([username] (login! (name username) (name username)))
  ([username password] (login! username password nil))
  ([username password kid]
   (reset-cookies)
   (let [req (-> (public-request)
                 (edn {:user/username username
                       :user/password password}))
         res (client/post (path-for :login) req)]
     (when (created? res)
       (if (nil? (res :cookies))
         (throw (ex-info "login cookie not found" {}))
         (swap! state assoc :cookies (res :cookies))))
     res)))

(defn login-as-admin!
  [& [kid]]
  (login! "admin" "adminpassword" kid))

(defn logout! []
  (try
    (reset-cookies)
    (client/delete (path-for :logout) (auth-request))
    (catch Exception _)))

(defn logout []
  (reset-cookies))
