(ns servergems.liberator
    (:require [liberator.core :refer [resource]]
              [clojure.edn :as edn]
              [clojure.data.json :as json]
              [clojure.walk :as walk]
              [clojure.spec.alpha :as s]
              [bidi.bidi :as bidi]
              [liberator.representation :as representation]
              [buddy.hashers :as hashers]
              [servergems.security :as security]
              [servergems.spec-utils :as spec-utils]))

(def supported-media-types
  ["application/edn"
   "application/transit+json"
   "application/transit+msgpack"
   "application/json"])

(defn put? [ctx]
  (= (get-in ctx [:request :request-method]) :put))

(defn put-request-not-new [ctx]
  (not (put? ctx)))

(defn handle-malformed
  [ctx]
  (condp = (get-in ctx [:request :content-type])
    "application/json"    (json/write (:errors ctx))
    "application/edn"     (pr-str (:errors ctx))
    "application/clojure" (pr-str (:errors ctx))
    (:message ctx)))

(def default-options
  {:new?                  put-request-not-new
   :authorized?           security/authenticated?
   :known-content-types   supported-media-types
   :available-media-types supported-media-types
   :handle-malformed      handle-malformed
   :respond-with-entity?  true})

(defmacro defresource
  [name & args]
  `(def ~name (resource default-options ~@args)))

(defn param
  [ctx key]
  (get-in ctx [:request :params key]))

(defn read-edn
  [body]
  (try
    (-> body slurp edn/read-string)
    (catch Exception _
      false)))

(defn read-json
  [body]
  (try
    (-> body slurp json/read)
    (catch Exception _
      false)))

(defn params
  [ctx]
  (let [{:keys [params content-type body]} (:request ctx)]
    (condp = content-type
      "application/edn"     (read-edn body)
      "application/clojure" (read-edn body)
      "application/json"    (read-json body)
      params)))

(defn malformed-errors
  [errors]
  [true {:errors errors}])

(defn remove-nils
  [m]
  (into {} (remove (fn [[k v]] (and (string? v) (empty? v))) m)))

(defn ->malformed
  [spec & [defaults]]
  (fn [ctx]
    (let [params (remove-nils (merge defaults (params ctx)))
          values (spec-utils/explain spec params)]
      (if (some? values)
        (malformed-errors values)
        [false {:params params}]))))

(defn get-identity
  [context]
  (get-in context [:request :identity]))

(defn db [ctx]
  (-> ctx :request :mongo :db))

(defn anonymous [ctx] true)

(defn- ns->prefixed
    "Add namespace as prefix to the route name. e.g.
   `:namespace/xyz` will become `:namespace-xyz`"
  [x]
  (let [ns (namespace x)
        n  (name x)]
    (if (empty? ns)
      x
      (keyword
       (str
        (if-not (neg? (.indexOf ns "."))
          (last (clojure.string/split ns #"\."))
          ns) "-" n)))))

(defn- readable-route-tags
  [x]
  (cond
    (and (map? x) (keyword? (:tag x))) (ns->prefixed (:tag x))
    (instance? java.util.regex.Pattern x) (str "__reg_" (.pattern x))
    (or (fn? x) (instance? clojure.lang.IRecord x)) (keyword (gensym))
    :else x))

(defn pr-routes
  [routes]
  (let [tagged (walk/prewalk readable-route-tags routes)]
    (walk/postwalk
     (fn [x]
       (if (vector? x)
         (vec (remove nil? x))
         x))
     tagged)))

(defn wrap-routes
  [handler routes]
  (let [routes-export (pr-routes routes)]
    (fn [req]
      (handler (assoc req
                      :routes routes
                      :routes-export routes-export)))))

(defresource routes
  :allowed-methods [:get]
  :authorized?     anonymous
  :handle-ok       (fn [ctx]
                     {:routes (-> ctx :request :routes-export)}))

(s/def :user/username string?)
(s/def :user/password string?)

(s/def ::login-spec (s/keys :req [:user/username :user/password]))

(defn check-password
  [new orig]
  (and new orig (hashers/check new orig)))

(defn authenticate-user
  [ctx]
  (let [[malformed? result :as error] ((->malformed ::login-spec) ctx)]
    (if-not malformed?
      (let [{:user/keys [username password]} (:params result)
            authenticate (-> ctx :request :authentication-fn)]
        (if (nil? authenticate)
          (throw (ex-info "request must have an :authentication-fn set" {}))
          (let [{:keys [user secret]} (authenticate username)]
            (if (check-password password (:password user))
              [false
               {:token (security/create-token (dissoc user :password) secret)
                :user user}]
              (malformed-errors {:username :invalid-login})))))              
      error)))

(defn login-data
  [{:keys [user token] :as ctx}]
  (-> {:principal user}
      (representation/as-response ctx)
      (assoc-in [:cookies :token] {:value token, :path "/"})
      (representation/ring-response)))

(defresource login
  :allowed-methods [:post]
  :malformed?      authenticate-user
  :authorized?     (constantly true)
  :handle-created  login-data)
