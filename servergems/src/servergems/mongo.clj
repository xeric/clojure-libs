(ns servergems.mongo
  (:require [monger.core :as mg]
            [monger.collection :as mc]
            [buddy.hashers :as hashers]
            [integrant.core :as ig])
  (:import [org.bson.types ObjectId]))

(defmethod ig/init-key :db/mongo [_ {:keys [db-uri cleanup?]}]
  (let [mongo (mg/connect-via-uri db-uri)]
    (when cleanup?
      (mg/drop-db (:conn mongo) (.getName (:db mongo))))
    mongo))

(defmethod ig/halt-key! :db/mongo [_ {:keys [conn]}]
  (mg/disconnect conn))

(def User "user")

(defn id
  [arg]
  (cond
    (string? arg) (ObjectId. arg)
    (map? arg)    (id (:_id arg))
    :else arg))

(defn find-by-id
  [db collection db-id]
  (mc/find-one-as-map db collection {:_id (id db-id)}))

(defn encrypt-password [user]
  (if (:password user)
    (update user :password hashers/encrypt)
    user))

(defn create-user!
  [db user]
  (mc/insert-and-return db User (encrypt-password user)))

(defn list-users
  [db]
  (mc/find-maps db User))

(defn update-user!
  [db user] 
  (mc/update db User (id user) (encrypt-password user)))

(defn user-by-id
  [db id]
  (find-by-id db User id))

(defn user-by-username
  [db username]
  (mc/find-one-as-map db User {:username username}))

(defn user-by-email
  [db email]
  (mc/find-one-as-map db User {:email email}))

(defmethod print-method ObjectId [o ^java.io.Writer w]
  (.write w "#bson/object ")
  (print-method (.toString o) w))

(defn object-reader [form]
  (str form))
  
(defn doc-exists?
  ([coll ctx]
   (doc-exists? coll ctx :id :doc))
  ([coll ctx id-key doc-key]
   (when-let [doc (find-by-id (-> ctx :request :mongo :db)
                              coll
                              (-> ctx :request :params id-key))]
     {doc-key doc})))

