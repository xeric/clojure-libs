(ns servergems.security
  (:import [java.util Calendar TimeZone Date])
  (:require [buddy.auth :as buddy-auth]
            [buddy.auth.backends.token :refer [jws-backend]]
            [buddy.auth.middleware :as buddy-middleware]
            [buddy.auth.protocols :as proto]            
            [buddy.sign.jws :as jws]
            [buddy.sign.jwt :as jwt]
            [buddy.hashers :as hashers]
            [clj-time.core :as t]
            [ring.middleware.session :refer [wrap-session]]
            [ring.middleware.session.cookie :as cookie]
            [clojure.java.io :as io]))

(def sign-options {:alg :hs512})

(defn expiry-date []
  (let [c (doto (Calendar/getInstance)
            (.add Calendar/HOUR 24))]
    (.getTime c)))

(defn create-token [data secret]
  (jwt/sign data secret sign-options))

(defn- handle-unauthorized-default
  "A default response constructor for an unauthorized request."
  [request]
  (if (buddy-auth/authenticated? request)
    {:status 403 :headers {} :body "Permission denied"}
    {:status 401 :headers {} :body "Unauthorized"}))

(defn jws-backend-with-cookies
  [{:keys [secret unauthorized-handler options token-name on-error]}]
  (reify
    proto/IAuthentication
    (-parse [_ request]
      (get-in request [:cookies "token" :value]))
    (-authenticate [_ request data]
      (try
        (jwt/unsign data secret options)
        (catch clojure.lang.ExceptionInfo e
          (let [data (ex-data e)]
            (when (fn? on-error)
              (on-error request e))
            nil))))
    proto/IAuthorization
    (-handle-unauthorized [_ request metadata]
      (if unauthorized-handler
        (unauthorized-handler request metadata)
        (handle-unauthorized-default request)))))

(defn auth-backend
  [secret]
  (jws-backend-with-cookies {:secret secret :options sign-options}))

(defn wrap-security
  [handler secret]
  (let [backend (auth-backend secret)]
    (-> handler
        (buddy-middleware/wrap-authentication backend)
        (buddy-middleware/wrap-authorization backend))))      

(defn authenticated?
  [request]
  (let [req (if-let [req (:request request)] req request)]
    (buddy-auth/authenticated? req)))

(defn role?
  [request role]
  (let [req (if-let [req (:request request)] req request)]
    (and (authenticated? req)
         (contains? (set (get-in req [:identity :roles])) role))))

(def user? #(role? % "user"))

(def admin? #(role? % "admin"))
