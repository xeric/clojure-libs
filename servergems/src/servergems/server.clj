(ns servergems.server
  (:require [bidi.bidi :refer [tag]]
            [bidi.ring :refer [make-handler resources-maybe]]
            [integrant.core :as ig]
            [ring.util.response :as response]
            [ring.middleware.gzip :refer [wrap-gzip]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.multipart-params
             :refer [wrap-multipart-params]]
            [environ.core :refer [env]]
            [org.httpkit.server :refer [run-server]]
            [ring.middleware.cookies :refer [wrap-cookies]]
            [servergems.security :as security]
            [servergems.liberator :refer [wrap-routes]]
            [servergems.static :as static]
            [clojure.string :as string]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]))

(defn routes [rest-routes]
  ["" [["/static" (resources-maybe {:prefix "public"})]
       ["/rest/" [rest-routes]]
       [true static/app-page-resource]]])

(defn wrap-db-connection
  [handler mongo]
  (fn [req]
    (handler (assoc req :mongo mongo))))

(defn wrap-stacktrace
  [handler]
  (fn [req]
    (try
      (handler req)
      (catch Exception e
        (prn e)
        (throw e)))))

(defn wrap-authentication-fn
  [handler f]
  (fn [req]
    (handler (assoc req :authentication-fn f))))

(defmethod ig/init-key :server/handler
  [_ {:keys [mongo setup]}]
  {:pre [(:routes setup)
         (string? (:secret setup))
         (fn? (:authentication-fn setup))
         (fn? (:get-assets setup))]}
  (-> (make-handler (:routes setup))
      (wrap-db-connection mongo)
      (wrap-routes (:routes setup))
      (security/wrap-security (:secret setup))
      (wrap-stacktrace)
      (wrap-authentication-fn (:authentication-fn setup))
      (static/wrap-static (:get-assets setup) (not (env :release)))
      (wrap-cookies)
      (wrap-multipart-params)
      (wrap-params)
      (wrap-content-type)
      (ring.middleware.not-modified/wrap-not-modified)
      (wrap-gzip)))

(defmethod ig/init-key :server/httpkit [_ {:keys [options handler]}]
  (run-server handler options))

(defmethod ig/halt-key! :server/httpkit [_ server]
  (server))

(defonce system nil)

(defn start [config]
  (let [config (if (map? config) config (ig/read-string (slurp config)))]
    (alter-var-root #'system (constantly (ig/init config)))))

(defn stop []
  (alter-var-root #'system (fn [system]
                             (when system
                               (ig/halt! system)))))

(defn db []
  (-> system :db/mongo :db))

(defn create-system-fixture
  [& {:keys [config]}]
  (fn [test]
    (start (or config (io/resource "testconfig.edn")))
    (test)
    (stop)))

(defn -main
  [& args]
  (let [config (or (env :config) "config.edn")]
    (if-not (.exists (io/file config))
      (throw (ex-info (str "configuration file not found: " config) {}))    
      (start config))))



