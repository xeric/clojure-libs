(ns servergems.spec-utils
  (:require [clojure.spec.alpha :as s]))

(defmulti user-message
  (fn [{:keys [pred]}]
    (if (seq? pred) (first pred) pred)))

(defmethod user-message 'clojure.core/contains?
  [{:keys [pred]}]
  [(nth pred 2) :missing-value])

(defmethod user-message :default
  [{:keys [pred in] :as result}]
  [(if (= (count in) 1) (first in) in) (keyword (name pred))])

(defn simplify-pred
  [e]
  (update-in e [:pred] (fn [pred] (if (seq? pred) (nth pred 2) pred))))

(defn explain
  "A simple explain for converting errors into user friendly
   messages"
  [spec data]
  (when-let [p (:clojure.spec.alpha/problems (s/explain-data spec data))]
    (into {} (mapv (comp user-message simplify-pred) p))))
