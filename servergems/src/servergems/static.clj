(ns servergems.static
  (:require [optimus.prime :as optimus]
            [optimus.assets :as assets]
            [optimus.optimizations :as optimizations]
            [optimus.strategies :as strategies]
            [optimus.html :as optimus-html]            
            [liberator.core :refer [resource]]
            [hiccup.page :as page]
            [hiccup.core :refer [html]]
            [clojure.tools.logging :as log]
            [ring.util.response :as response]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.not-modified :refer [wrap-not-modified]]
            [ring.middleware.file :refer [wrap-file]]))

(defn get-assets
  "Get static assets configured as bundles"
  [css-files js-files]
  (assets/load-bundles
   "public"
   {"aggregate.css" css-files "aggregate.js" js-files}))

(defn app-page
  "Application home page."
  [{req :request :as ctx} & [app-attributes]]
  (page/html5
   [:head
    [:meta {:charset "utf-8"}]
    [:meta {:http-equiv "X-UA-Compatible" :content "IE=edge"}]
    [:meta {:name "viewport"
            :content (str "width=device-width,initial-scale=1,"
                          "user-scalable=0, maximum-scale=1, minimum-scale=1")}]
    (optimus-html/link-to-css-bundles req ["aggregate.css"])]
   [:body
    [:div#container
     [:div#content
      [:div#app app-attributes]]]
    (optimus-html/link-to-js-bundles req ["aggregate.js"])]))

(def
  ^{:doc "Application page. An empty page which will be filled on the client side"}
  app-page-resource
  (resource
   :available-media-types ["text/html"]
   :allowed-methods [:get]
   :handle-ok app-page))

(defn wrap-static
  "Middleware for static resources"
  [handler get-assets dev?]
  (-> handler
      (wrap-file "resources")
      (optimus/wrap
       get-assets
       (if dev? optimizations/none optimizations/all)
       (if dev?
         strategies/serve-live-assets
         strategies/serve-frozen-assets))
      (wrap-content-type)
      (wrap-not-modified)))
