(ns servergems.liberator-test
  (:require [servergems.liberator :refer :all]
            [servergems.server :as server]
            [servergems.mongo :as mongo]
            [servergems.test-utils]
            [servergems.client :as client-utils :refer :all]
            [clj-http.client :as client]
            [bidi.bidi :as bidi]
            [clojure.test :refer :all]))

(use-fixtures :each (server/create-system-fixture))

(deftest test-routes
  (is (ok? (client/get (path-for "/rest/routes") (public-request)))))

(deftest routes-are-loaded-for-testing-purposes
  (set-routes!)
  (is (.endsWith (path-for :routes) "/rest/routes")))

(deftest test-static-resources
  (is (ok? (client/get (path-for "/css/mdl.css") (resource-request)))))

(deftest test-get-request
  (set-routes!)
  (let [res (client/get (path-for :get-request) (public-request))]
    (is (ok? res))
    (is (= (:message (:body res)) "Hello"))))

(deftest test-post-request
  (set-routes!)
  (let [res (client/post (path-for :post-request)
                        (-> (public-request)
                            (edn {:user/username "john"
                                  :user/password "smith"})))]
    (is (created? res))
    (is (= (:message (:body res)) "created"))))

(deftest test-malformed
  (set-routes!)
  (let [res (client/post (path-for :post-request)
                         (-> (public-request)
                             (edn {:user/username "john"})))]
    (is (malformed? res))
    (is (= (:body res) #:user{:password :missing-value}))))

(deftest test-wrong-format
  (set-routes!)
  (let [res (client/post (path-for :post-request)
                         (-> (public-request)
                             (edn {:user/username 123
                                   :user/password "abc"})))]
    (is (malformed? res))
    (is (= (:body res) #:user{:username :string?}))))

(deftest test-mongo
  (set-routes!)
  (let [res (client/get (path-for :db-request) (public-request))]
    (is (ok? res))
    (is (= (-> res :body count) 1))
    (is (= (-> res :body first :username) "john"))))

(deftest test-login
  (set-routes!)
  (mongo/create-user! (server/db) {:username "john" :password "password"})
  (let [res (client/post (path-for :login)
                         (-> (public-request)
                             (edn {:user/username "john"
                                   :user/password "password"})))]
    (is (created? res))
    (is (= (-> res :body :principal :username) "john"))))

(deftest test-authentication
  (set-routes!)
  (mongo/create-user! (server/db) {:username "john" :password "password"})
  (let [res (login! "john" "password")]
    (is (unauthorized? (client/get (path-for :secure-link)
                                   (public-request))))
    (is (ok? (client/get (path-for :secure-link) (auth-request))))))
                             
  
