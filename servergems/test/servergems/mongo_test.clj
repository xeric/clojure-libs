(ns servergems.mongo-test
  (:require [servergems.mongo :as db]
            [servergems.server :as server :refer [db]]
            [servergems.test-utils]
            [clojure.test :refer :all]))

(use-fixtures :each (server/create-system-fixture))

(defn new-user
  [username]
  (db/user-by-id (db) (:_id (db/create-user! (db) {:username username}))))

(deftest test-create-user
  (is (new-user "abc")))

(deftest test-list-users
  (let [user-count (count (db/list-users (db)))]
    (new-user "bcd")
    (is (= user-count (dec (count (db/list-users (db))))))))



