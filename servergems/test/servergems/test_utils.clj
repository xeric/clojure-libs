(ns servergems.test-utils
  (:require  [clojure.test :as t]
             [servergems.server :as server]
             [bidi.bidi :as bidi]
             [servergems.liberator :as liberator-utils :refer :all]
             [servergems.client :refer :all]
             [servergems.static :as static]
             [servergems.mongo :as mongo]
             [clojure.spec.alpha :as s]
             [integrant.core :as ig]))

(defresource get-request
  :allowed-methods [:get]
  :authorized?     anonymous
  :handle-ok       (fn [_]
                     {:message "Hello"}))

(s/def :user/username string?)
(s/def :user/password string?)
(s/def :user/age int?)

(s/def ::simple-form (s/keys :req [:user/username :user/password]
                             :opt [:user/age]))

(defresource post-request
  :allowed-methods [:post]
  :malformed?      (->malformed ::simple-form)
  :authorized?     anonymous
  :handle-created  (fn [_]
                     {:message "created"}))

(defresource db-request
  :allowed-methods [:get]
  :authorized?     anonymous
  :handle-ok       (fn [ctx]
                     (mongo/create-user! (db ctx) {:user/username "john"})
                     (mongo/list-users (db ctx))))

(defresource secure-link
  :allowed-methods [:get]
  :handle-ok       (fn [ctx]
                     {:message "secured"}))

(defn get-assets
  []
  (static/get-assets ["/css/mdl.css" "/css/mdl-icons.css"] ["/js/gems.js"]))

(defn get-routes
  []
  (server/routes
   ["" [["routes" (bidi/tag liberator-utils/routes :routes)]
        ["test" {:get  (bidi/tag get-request :get-request)
                 :post (bidi/tag post-request :post-request)}]
        ["db" {:get (bidi/tag db-request :db-request)}]
        ["secure-link" (bidi/tag secure-link :secure-link)]
        ["login" {:post (bidi/tag liberator-utils/login :login)}]]]))

(defn authentication-fn
  [mongo secret]
  (fn [username]
    (when-let [user (mongo/user-by-username (:db mongo) username)]
      {:user (update user :_id str) :secret secret})))

(defmethod ig/init-key :server/setup [_ {:keys [secret mongo]}]
  {:routes (get-routes)
   :authentication-fn (authentication-fn mongo secret)
   :get-assets get-assets
   :secret secret})
